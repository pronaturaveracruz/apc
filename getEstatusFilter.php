<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$sql = pg_query($link, "SELECT descripcion_estatus FROM estatus GROUP BY descripcion_estatus ORDER BY descripcion_estatus ASC");
$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
