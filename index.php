<?php
error_reporting(0);
date_default_timezone_set("Mexico/General");
session_start();
$userLogged = $_SESSION['userLogged'];
include "../includes/conexion.php";
$linkMySQL = ConectarseMySQLPronaBD();

#VERIFICAMOS SI EXISTE ALGUN USUARIO LOGGEADO
if ($userLogged) {
    #VERIFICAMOS SI EL USUARIO DE LA SESIÓN ESTA AUTORIZADO PARA USAR ALGUN SISTEMA
    $sqlAuthUserCount = "SELECT COUNT(*) ";
    $sqlAuthUserSelect = "SELECT ca.id_control, sis.nombre_corto, sis.nombre_completo ";
    $sqlAuthUserBase = "FROM control_acceso AS ca
           INNER JOIN usuario AS u ON ca.id_usuario = u.id_usuario
           INNER JOIN sistema AS sis ON ca.id_sistema = sis.id_sistema
    WHERE u.nombre_usuario ='$userLogged' AND u.activo = 1 AND ca.autorizado = 1";

    $sqlAuthUserQueryCount = mysqli_query($linkMySQL, $sqlAuthUserCount . $sqlAuthUserBase);
    while ($rowAuthUserQueryCount = mysqli_fetch_row($sqlAuthUserQueryCount)) {
        $validSystems = $rowAuthUserQueryCount[0];
    }
    if ($validSystems == 0) {
        #NINGUN SISTEMA AUTORIZADO - DESTRUIMOS LA SESIÓN
        session_destroy();
        #REDIRECCIONAR A LA PAGINA DE LOGIN
        echo "<script>window.location.replace('../login/');</script>";
        exit;
    } elseif ($validSystems > 0) {
        #MOSTAR TARJETAS DE SISTEMAS AUTORIZADOS
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>APC · Pronatura Veracruz AC</title>
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.4/css/mdb.min.css" rel="stylesheet">
    <!-- EasyUI -->
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/material/easyui.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/color.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" type="text/css" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

    <?php
        include '../globalNavbar.php';
        include "modals.php";
    ?>

    <div class="container-fluid">

        <div class="container-fluid mt-4">
            <div id="alertOk" class="alert alert-success alert-dismissible fade show d-none" role="alert">
                <i class="fas fa-arrow-circle-down"></i>&nbsp;&nbsp;
                <span id="alertOkText">...</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="alertError" class="alert alert-danger alert-dismissible fade show d-none" role="alert">
                <span id="alertErrorText">...</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <!-- DATAGRID -->
        <div class="card card-cascade narrower mt-4 animated fadeIn dtSolicitudesWrapper">
            <div class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div>

                </div>
                <a href="" class="white-text mx-3">Consultas</a>
                <div>

                </div>
            </div>
            <div class="px-4">
                <div class="table-wrapper">
                    <table id="dg" style="width: 100%;">
                        <thead frozen="true">
                            <tr>
                                <!-- <th data-options="field:'figura_legal', align:'center', width:110">Figura legal</th> -->
                                <th data-options="field:'estatus',align:'center', width:60" formatter="formatStatus">Estatus</th>
                                <th data-options="field:'geomcoordscclpoly',align:'center', width:60" formatter="formatShapefileIcon">Shapefile</th>
                                <th data-options="field:'folio', align:'left', width:80">Folio</th>
                                <th data-options="field:'nombre_apc',align:'left', width:150">Nombre del APC</th>
                                <th data-options="field:'nombre_completo',align:'left', width:250">Propietario(s)</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th data-options="field:'hectareas',align:'left', width:100" formatter="formatHectareas">Superficie</th>
                                <th data-options="field:'estado',align:'left', width:100">Estado</th>
                                <th data-options="field:'municipio',align:'left', width:150">Municipio</th>
                                <th data-options="field:'localidad',align:'left', width:150">Localidad</th>
                                <th data-options="field:'fecha_ingreso',align:'left', width:150" formatter="formatDate">Fecha
                                    de ingreso</th>
                                <th data-options="field:'fecha_certificacion',align:'center', width:185" formatter="formatDate">Fecha
                                    de certificación o decreto</th>
                                <th data-options="field:'numero_sedema',align:'center', width:200" formatter="formatNSedema">Numero
                                    SEDEMA</th>
                                <th data-options="field:'motivo_no_procede',align:'center', width:250" formatter="formatMotivo">Motivo
                                    (No procede/regresado SEDEMA)</th>
                                <th data-options="field:'vegetacion',align:'center', width:200" formatter="formatVegetacion">Vegetación</th>
                                <!-- <th data-options="field:'observaciones_vegetacion',align:'center'" formatter="formatOVegetacion">Observaciones
                                    de vegetación</th> -->
                                <th data-options="field:'especies',align:'center'" formatter="formatEspecies">Especies
                                    (Objetivo certificación)</th>
                                <th data-options="field:'tecnico',align:'left', width:200">Técnico</th>
                                <th data-options="field:'descripcion_zona',align:'left', width:400">Zona</th>
                                <th data-options="field:'nombre_proyecto',align:'left', width:500">Proyecto</th>
                                <th data-options="field:'fase',align:'center', width:60">Fase</th>
                                <th data-options="field:'proteccion_legal',align:'center', width:200">Proteccion legal</th>
                                <th data-options="field:'cuenta_plan_manejo',align:'center', width:200">¿Cuenta con
                                    plan de manejo?</th>
                                <th data-options="field:'ruta_plan_de_manejo',align:'center'">Plan de manejo</th>
                                <th data-options="field:'tenencia',align:'left', width:100">Tenencia</th>
                                <th data-options="field:'observaciones',align:'center'" formatter="formatObservaciones">Observaciones</th>

                                <th data-options="field:'organizacion',align:'center', width:130" formatter='formatSello'>Sello
                                    de biodiversidad</th>
                                <th data-options="field:'org_marca',align:'center', width:250">Marca</th>

                                <th data-options="field:'ruta_doc_ingresados',align:'center'">Documentos</th>
                                <th data-options="field:'ruta_fotografias',align:'center'">Fotografías</th>
                                <th data-options="field:'ruta_certificado',align:'center'">Certificado</th>
                                <!-- <th data-options="field:'geomcoordscclpoly',align:'left', width:300">Coordenada central
                                    (Cónica Conforme de Lambert)</th> -->

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <!-- TOOLBAR -->
        <div id="toolbar" class="px-2 d-flex">
            <div id="spanRows" class="mr-auto d-flex flex-row">
                <a class="rows-text">
                    <i class="far fa-eye"></i>
                    <span id="rowsMessage" style="line-height:28px;">Cargando...</span>
                </a>
            </div>

            <div id="toolButtons" class="ml-auto d-flex flex-row-reverse">
                <a id="btnDownload" style="color:SkyBlue;">
                    <i class="fas fa-cloud-download-alt"></i>
                </a>

                <div id="menuDownload">
                    <div data-options="name:'exportToExcel', iconCls:'fas fa-file-csv SeaGreen'">Archivo CSV</div>
                    <div data-options="iconCls:'fas fa-map Brown'">
                        <span>Shapefile</span>
                        <div>
                            <div data-options="name:'exportShapefileCCL', iconCls:'fas fa-file-archive GoldenRod'">Cónica
                                conforme de Lambert (WGS84)</div>
                            <div data-options="name:'exportShapefileUTM14', iconCls:'fas fa-file-archive GoldenRod'">UTM
                                Zona 14N</div>
                            <div data-options="name:'exportShapefileUTM15', iconCls:'fas fa-file-archive GoldenRod'">UTM
                                Zona 15N</div>
                        </div>
                    </div>
                    <div data-options="name:'exportDocumentacion', iconCls:'fas fa-file-archive GoldenRod'">Documentación</div>
                    <div data-options="name:'exportFotos', iconCls:'fas fa-file-archive GoldenRod'">Fotos</div>
                    <div data-options="name:'exportCertificados', iconCls:'fas fa-file-archive GoldenRod'">Certificados</div>
                    <div data-options="name:'exportDFC', iconCls:'fas fa-file-archive GoldenRod'">Documentación, fotos
                        y certificados</div>
                </div>

                <span class="button-sep"></span>

                <a id="btnReload" style="color:DarkSlateBlue;">
                    <i class="fas fa-redo"></i>
                </a>

                <!-- <a id="btnEdit" style="color:DarkOrange;" href="#" data-toggle="modal" data-target="#modalEdit"> -->
                <a id="btnEdit" style="color:DarkOrange;">
                    <i class="fas fa-pencil-alt"></i>
                </a>

                <a id="btnAdd" style="color:SeaGreen;" href="#" data-toggle="modal" data-target="#modalAdd">
                    <i class="fas fa-plus-circle"></i>
                </a>
            </div>

        </div>


        <div id="menuFilterOptionsHectareas" class="easyui-menu">
            <div id="m-equal_h" data-options="iconCls:'far fa-square'">Igual</div>
            <!-- far fa-square far fa-check-square -->
            <div id="m-greater_h" data-options="iconCls:'far fa-square'">Mayor que</div>
            <div id="m-less_h" data-options="iconCls:'far fa-square'">Menor que</div>
        </div>

        <div id="menuFilterOptionsFechaIngreso" class="easyui-menu">
            <div id="m-equal_fi" data-options="iconCls:'far fa-square'">Igual</div>
            <div id="m-greater_fi" data-options="iconCls:'far fa-square'">Mayor que</div>
            <div id="m-less_fi" data-options="iconCls:'far fa-square'">Menor que</div>
        </div>

        <div id="menuFilterOptionsFechaCertificacion" class="easyui-menu">
            <div id="m-equal_fc" data-options="iconCls:'far fa-square'">Igual</div>
            <div id="m-greater_fc" data-options="iconCls:'far fa-square'">Mayor que</div>
            <div id="m-less_fc" data-options="iconCls:'far fa-square'">Menor que</div>
        </div>


    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.4/js/mdb.min.js"></script>
    <!-- EasyUI -->
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/locale/easyui-lang-es.js"></script>
    <!-- <script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-filter.js"></script> -->
    <!-- NOT WORKING CUSTOM JSON OBJECTS -->
    <script type="text/javascript" src="js/datagrid-filter.js"></script>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-scrollview.js"></script>
    <!-- Custom scripts -->
    <script src="js/scripts.js"></script>

</body>

</html>

<?php
    }
} else {
    #REDIRECCIONAR A LA PAGINA DE LOGIN
    echo "<script>window.location.replace('../login/');</script>";
}