# Sistema de control de Áreas Privadas de Conservación
![](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/os_badge.svg)
![](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/server_apache.svg)
![](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/database_mysql.svg)
![](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/database_postgresql.svg)
![](https://gitlab.com/pronaturaveracruz/apc/raw/master/images/badges/backend_php.svg)



Sistema de captura y seguimiento para las áreas privadas de conservación.