<?php
error_reporting(0);
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 100;
$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id_principal';
$order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
$offset = ($page - 1) * $rows;

#REQUEST
$filterArray = isset($_REQUEST['filterRules']) ? json_decode($_REQUEST['filterRules'], true) : '';
$num_filter = count($filterArray);
//print_r($filterArray);
//print_r($num_filter);

if ($num_filter === 0) {
    //echo "reset query";
    $where = "";
} else {
    //echo "apply filter";
    $where = "";
    for ($i = 0; $i < $num_filter; $i++) {
        $filterField = $filterArray[$i]['field'];
        $filterOperator = $filterArray[$i]['op'];
        $filterValue = $filterArray[$i]['value'];

        #EXCEPCION PARA EL SELLO DE BIODIVERSIDAD
        if ($filterField == 'organizacion') {
            if ($filterValue == 'Si') {
                $filterValue = '';
                $where .= " AND ok3." . $filterField . " <> '" . $filterValue . "'";
            } else if ($filterValue == 'No') {
                $filterValue = '';
                $where .= " AND ok3." . $filterField . " IS NULL";
            }
        } else {

            $mValues = explode(",", $filterValue);
            $countmValues = count($mValues);
            for ($j = 0; $j < $countmValues; $j++) {
                $filterValue = $mValues[$j];
                if ($countmValues > 1) {
                    if ($j === 0) {
                        $where .= " AND(  (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))";
                    } else if ($j !== 0 && $j !== ($countmValues - 1)) {
                        $where .= " OR (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))";
                    } else if ($j === ($countmValues - 1)) {
                        $where .= " OR (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))  )";
                    }

                } else {
                    switch ($filterOperator) {
                        case 'contains':
                            $where .= " AND ( lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*') )";
                            break;
                        case 'equal';
                            $where .= " AND ok3." . $filterField . " = '" . $filterValue . "'";
                            break;
                        case 'notequal';
                            $where .= " AND ok3." . $filterField . " <> '" . $filterValue . "'";
                            break;
                        case 'beginwith';
                            $where .= " AND ok3." . $filterField . " LIKE '" . $filterValue . "%'";
                            break;
                        case 'endwith';
                            $where .= " AND ok3." . $filterField . " LIKE '%" . $filterValue . "'";
                            break;
                        case 'less';
                            $where .= " AND ok3." . $filterField . " < '" . $filterValue . "'";
                            break;
                        case 'lessorequal';
                            $where .= " AND ok3." . $filterField . " <= '" . $filterValue . "'";
                            break;
                        case 'greater';
                            $where .= " AND ok3." . $filterField . " > '" . $filterValue . "'";
                            break;
                        case 'greaterorequal';
                            $where .= " AND ok3." . $filterField . " >= '" . $filterValue . "'";
                            break;
                    }
                }
            }
        }

    }
}

#NEW ORDER IN QUERY (2018)
$select = "SELECT ok3.id_principal, ok3.figura_legal, ok3.nombre_completo, ok3.folio, ok3.nombre_apc, ok3.hectareas,ok3.hectareas_certificadas,ok3.estado,ok3.municipio,ok3.localidad,ok3.fecha_ingreso,ok3.estatus,ok3.fecha_certificacion, ok3.numero_sedema, ok3.motivo_no_procede, ok3.vegetacion,ok3.observaciones_vegetacion, ok3.especies, ok3.tecnico, ok3.descripcion_zona, ok3.nombre_proyecto,ok3.fase, ok3.vigencia, ok3.proteccion_legal, ok3.cuenta_plan_manejo, ok3.vigencia_plan_manejo, ok3.tenencia, ok3.observaciones, ok3.ruta_doc_ingresados, ok3.ruta_fotografias, ok3.ruta_certificado, ok3.ruta_plan_de_manejo, shape_puntos_ccl.geom AS geom_point, shape_poligonos_ccl_merge_final.geom AS geom_poly, ST_AsText(ST_Centroid(shape_poligonos_ccl_merge_final.geom)) AS geomcoordscclpoly, ST_AsText(shape_puntos_ccl.geom ) AS geomcoordscclpoint , ok3.organizacion, ok3.org_marca ";
$selectCount = "SELECT COUNT(*) ";
$consultaBase = "FROM
(
SELECT ok2.id_principal, ok2.figura_legal, trim(replace(ok2.nombre_completo,'  ', ' ')) AS nombre_completo, ok2.folio, ok2.nombre_apc, ok2.hectareas,ok2.hectareas_certificadas,ok2.estado,ok2.municipio,ok2.localidad,ok2.fecha_ingreso,ok2.estatus,ok2.fecha_certificacion, ok2.numero_sedema, ok2.motivo_no_procede, ok2.vegetacion,ok2.observaciones_vegetacion, ok2.especies, ok2.tecnico, ok2.descripcion_zona, ok2.nombre_proyecto,ok2.fase, ok2.vigencia, ok2.proteccion_legal, ok2.cuenta_plan_manejo, ok2.vigencia_plan_manejo, ok2.tenencia, ok2.observaciones, ok2.ruta_doc_ingresados, ok2.ruta_fotografias, ok2.ruta_certificado, ok2.ruta_plan_de_manejo, ok2.organizacion, ok2.org_marca
FROM
(
SELECT ok.id_principal, ok.figura_legal, array_to_string(array_agg(ok.nombre_completo), ',') AS nombre_completo, ok.folio, ok.nombre_apc, ok.hectareas,ok.hectareas_certificadas,ok.estado,ok.municipio,ok.localidad,ok.fecha_ingreso,ok.estatus,ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion,ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.tenencia, ok.observaciones, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca
FROM
(
SELECT apc_principal.id_principal, figura_legal.figura_legal, trim((propietario.nombre || ' ' || propietario.apat || ' ' || propietario.amat)) AS nombre_completo,apc_principal.folio,apc_principal.nombre_apc,apc_principal.hectareas,apc_principal.hectareas_certificadas,apc_principal.estado,apc_principal.municipio,apc_principal.localidad,apc_principal.fecha_ingreso,apc_principal.estatus,apc_principal.fecha_certificacion, apc_principal.numero_sedema, apc_principal.motivo_no_procede, resultado.vegetacion, apc_principal.observaciones_vegetacion, especies, (tecnico.nombre || ' ' || tecnico.apat || ' ' || tecnico.amat) AS tecnico,descripcion_zona,nombre_proyecto, fase, vigencia,proteccion_legal,cuenta_plan_manejo,vigencia_plan_manejo,tenencia, observaciones, apc_principal.ruta_doc_ingresados, apc_principal.ruta_fotografias, apc_principal.ruta_certificado, apc_principal.ruta_plan_de_manejo, marca_sello.organizacion, (marca_sello.organizacion || ' - ' || marca_sello.marca) AS org_marca
FROM
(
SELECT resultante.id_apc, resultante.vegetacion, array_to_string(array_agg(categoria || ':' || nombre_cientifico), ',') AS especies
FROM
(
SELECT tv_apcs.id_apc, array_to_string(array_agg(categoria || ':' || tipo_vegetacion), ',') AS vegetacion
FROM tv_apcs
JOIN tipo_vegetacion ON tv_apcs.id_tipo_vegetacion=tipo_vegetacion.id_tipo_vegetacion
GROUP BY tv_apcs.id_apc
ORDER BY tv_apcs.id_apc
) AS resultante
LEFT JOIN objetivo_certificacion ON resultante.id_apc=objetivo_certificacion.id_apc
GROUP BY resultante.id_apc,resultante.vegetacion
ORDER BY resultante.id_apc
) AS resultado
JOIN apc_principal ON resultado.id_apc=apc_principal.id_principal
JOIN prop_apcs ON apc_principal.id_principal=prop_apcs.id_apc
JOIN public.propietario ON prop_apcs.id_propietario=propietario.id_propietario
LEFT JOIN public.tecnico ON apc_principal.tecnico=tecnico.id_tecnico
LEFT JOIN concentrado_zona_proyecto ON apc_principal.id_czp=concentrado_zona_proyecto.id_czp
LEFT JOIN zonas ON concentrado_zona_proyecto.id_zona=zonas.id_zona
LEFT JOIN proyecto ON concentrado_zona_proyecto.id_proyecto=proyecto.id_proyecto
LEFT JOIN marca_sello ON apc_principal.id_marca = marca_sello.id_marca
LEFT JOIN figura_legal ON apc_principal.id_figura_legal = figura_legal.id_figura_legal
ORDER BY propietario.nombre ASC
) AS ok
GROUP BY ok.folio, ok.figura_legal, ok.id_principal, ok.nombre_apc, ok.hectareas,ok.hectareas_certificadas,ok.estado,ok.municipio,ok.localidad,ok.fecha_ingreso,ok.estatus,ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion,ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.observaciones, ok.tenencia, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca
) AS ok2
) AS ok3
--JOINS
LEFT JOIN shape_puntos_ccl ON ok3.nombre_apc=shape_puntos_ccl.nombre_apc AND ok3.folio=shape_puntos_ccl.folio AND ok3.nombre_completo=shape_puntos_ccl.propietari
LEFT JOIN shape_poligonos_ccl_merge_final ON ok3.nombre_apc=shape_poligonos_ccl_merge_final.nombre_apc AND ok3.folio=shape_poligonos_ccl_merge_final.folio AND ok3.nombre_completo=shape_poligonos_ccl_merge_final.propietari
--END JOINS
WHERE ok3.id_principal>0";

#OBTENEMOS LOS TOTALES DE LA CONSULTA ##########################################
$result = array();
$countAllRows = pg_query($link, $selectCount . $consultaBase);
$countFiltered = pg_query($link, $selectCount . $consultaBase . $where);

$resultAllRows = pg_fetch_row($countAllRows);
$resultFiltered = pg_fetch_row($countFiltered);

$result["total"] = $resultFiltered[0];
$result["all"] = $resultAllRows[0];

#################################################################################

#EJECUTAMOS LA CONSULTA Y LA PASAMOS A UN ARREGLO PARA AL FINAL CONVERTIRLO EN UN JSON
$items = array();
$rs = pg_query($link, $select . $consultaBase . $where . " ORDER BY $sort $order LIMIT $rows OFFSET $offset");

##FOR DEBUG ####################################
//$myQuery = $select . $consultaBase . $where;
//print_r($myQuery);
###########################################

while ($row = pg_fetch_object($rs)) {
    #DAMOS FORMATO A LAS RUTAS PARA PODER VERLAS DESDE EL GRID
    ##PLAN DE MANEJO
    if ($row->ruta_plan_de_manejo != '') {
        $row->ruta_plan_de_manejo = "<a href='" . $row->ruta_plan_de_manejo . "' target='_blank'><i class='fas fa-file-pdf Maroon'></i></a>";
    } else {
        $row->ruta_plan_de_manejo = '<i class="fas fa-exclamation-circle IndianRed"></i>';
    }

    #TIENE SELLO DE BIODIVERSIDAD
    if ($row->organizacion != '') {
        $row->organizacion = "Si";
    } else {
        $row->organizacion = "No";
    }

    ##DOCUMENTOS
    if ($row->ruta_doc_ingresados != '') {
        $row->ruta_doc_ingresados = "<a href='" . $row->ruta_doc_ingresados . "' target='_blank'><i class='fas fa-file-pdf Maroon'></a>";
    } else {
        $row->ruta_doc_ingresados = '<i class="fas fa-exclamation-circle IndianRed">';
    }

    ##CERTIFICADO
    if ($row->ruta_certificado != '') {
        $row->ruta_certificado = "<a href='" . $row->ruta_certificado . "' target='_blank'><i class='fas fa-file-pdf Maroon'></a>";
    } else {
        $row->ruta_certificado = '<i class="fas fa-exclamation-circle IndianRed">';
    }

    ##FOTOGRAFIAS
    if ($row->ruta_fotografias != '') {
        ###VERIFICAR SU EXTENSION
        $ruta_completa = $row->ruta_fotografias;
        $ext_fotos = substr($ruta_completa, -3);
        if ($ext_fotos == 'doc' || $ext_fotos == 'ocx') {
            $row->ruta_fotografias = "<a href='" . $row->ruta_fotografias . "' target='_blank'><i class='fas fa-file-word RoyalBlue'></i></a>";
        } else {
            //
        }
    } else {
        $row->ruta_fotografias = '<i class="fas fa-exclamation-circle IndianRed">';
    }

    #COORDENDAS CCL (RETORNAR [X,Y] EN PUNTO Y CENTROIDE EN POLIGONO)
    /*if ($row->geomcoordscclpoly != '') {
    //POINT(3155039.17314675 756004.521623938)
    $coordPoligono = $row->geomcoordscclpoly;
    $coordPoligono = substr($coordPoligono, 6);
    $coordPoligono = substr($coordPoligono, 0, -1);
    $coordPoligono = explode(" ", $coordPoligono);
    $x = $coordPoligono[0];
    $y = $coordPoligono[1];
    $row->geomcoordscclpoly = "<table><tr><td><div style='width:150px;'>X: " . $x . "</div></td><td><div style='width:150px;'>Y: " . $y . "</div></td></tr></table>";
    } else {
    $coordPoint = $row->geomcoordscclpoint;
    $coordPoint = substr($coordPoint, 6);
    $coordPoint = substr($coordPoint, 0, -1);
    $coordPoint = explode(" ", $coordPoint);
    $x = $coordPoint[0];
    $y = $coordPoint[1];
    $row->geomcoordscclpoly = "<table><tr><td><div style='width:150px;'>X: " . $x . "</div></td><td><div style='width:150px;'>Y: " . $y . "</div></td></tr></table>";
    }*/

    #HACEMOS UNA VALIDACION DE SI TIENE SHAPEFILE CARGADO LEYENDO LA GEOMETRIA TANTO DE PUNTOS COMO DE POLIGONOS, ESTO LO REGRESAREMOS A JS PARA PONER UN ICONO.
    if ($row->geomcoordscclpoint == '' && $row->geomcoordscclpoly == '') {
        $row->geomcoordscclpoly = "Sin shapefile";
    } else {
        $row->geomcoordscclpoly = "Shapefile guardado";
    }

    array_push($items, $row);
}
$result["rows"] = $items;
pg_free_result($resultAllRows);
pg_free_result($resultFiltered);
pg_close($link);
echo json_encode($result);
