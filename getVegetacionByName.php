<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$cargadosDatagrid = isset($_POST['cargadosDatagrid']) ? $_POST['cargadosDatagrid'] : '';

if ($cargadosDatagrid == '') {
    $myQuery = "SELECT *
        FROM (
            SELECT
            id_tipo_vegetacion                      AS id,
            (tipo_vegetacion || ' - ' || categoria) AS name
            FROM public.tipo_vegetacion
            WHERE categoria != 'No especificada'
        ) AS final
        ORDER BY name ASC";

} else {
    $where = "";
    $arrayCargadosDatagrid = explode(",", $cargadosDatagrid);
    for ($i = 0; $i < count($arrayCargadosDatagrid); $i++) {
        if ($i == 0) {
            $where .= "WHERE name !='" . $arrayCargadosDatagrid[$i] . "'";
        } else {
            $where .= " AND name !='" . $arrayCargadosDatagrid[$i] . "'";
        }
    }
    $myQuery = "SELECT *
        FROM (
            SELECT
            id_tipo_vegetacion                      AS id,
            (tipo_vegetacion || ' - ' || categoria) AS name
            FROM public.tipo_vegetacion
            WHERE categoria != 'No especificada'
        ) AS final " . $where . "
		ORDER BY name ASC";
}

$sql = pg_query($link, $myQuery);
$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
