<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$sql = pg_query($link, "SELECT tipo_vegetacion FROM tipo_vegetacion GROUP BY tipo_vegetacion ORDER BY tipo_vegetacion ASC");
$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
