<?php

## PHP ShapeFile - Gaspare Sganga #####################################################
require_once 'includes/php-shapefile-2.4.3/src/ShapeFileAutoloader.php';
\ShapeFile\ShapeFileAutoloader::register();
use \ShapeFile\ShapeFile;
#######################################################################################

## Date configurations #################################################################
date_default_timezone_set("Mexico/General");
$fechaActual = date('Y-m-d');
#######################################################################################

## Session ############################################################################
session_start();
$userLogged = isset($_SESSION['userLogged']) ? $_SESSION['userLogged'] : '';
#######################################################################################

## Database connection ################################################################
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();
#######################################################################################

## Functions ################################################################
function isExtensionValid($file, $extensionsExpected)
{
    $valid = 0;
    if ($file['error'] === 0) {
        if (preg_match("/\.\w+$/", $file['name'], $matches)) {
            $arrayExploded = explode(",", $extensionsExpected);
            foreach ($arrayExploded as $currentExtension) {
                if ($matches[0] === '.' . $currentExtension) {
                    $valid++;
                }
            }
            if ($valid > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}

function recursiveRemoveDirectory($path)
{
    $files = glob($path . '/*');
    foreach ($files as $file) {
        if (is_dir($file)) {
            recursiveRemoveDirectory($file);
        } else {
            unlink($file);
        }
    }
    rmdir($path);
    return;
}

function haveShapefile($file)
{
    if ($file["error"] === 0) {
        return true;
    }
    return false;
}

function convertToValidPath($input)
{
    $input = str_replace('.', '_', $input);
    $input = str_replace('/', '_', $input);
    $input = str_replace('-', '_', $input);
    $input = str_replace('?', '_', $input);
    $input = str_replace('*', '_', $input);
    $input = str_replace('+', '_', $input);
    $input = str_replace('%', '_', $input);
    $input = str_replace(':', '_', $input);
    $input = str_replace('<', '_', $input);
    $input = str_replace('>', '_', $input);
    $input = str_replace('|', '_', $input);
    $input = str_replace(',', '_', $input);
    $input = str_replace('ñ', 'n', $input);
    $input = str_replace('á', 'a', $input);
    $input = str_replace('é', 'e', $input);
    $input = str_replace('í', 'i', $input);
    $input = str_replace('ó', 'o', $input);
    $input = str_replace('ú', 'u', $input);
    $input = str_replace('Ñ', 'n', $input);
    $input = str_replace('Á', 'a', $input);
    $input = str_replace('É', 'e', $input);
    $input = str_replace('Í', 'i', $input);
    $input = str_replace('Ó', 'o', $input);
    $input = str_replace('Ú', 'u', $input);
    $input = strtolower($input);
    $input = ucwords($input);
    $input = str_replace(' ', '', $input);
    return $input;
}

function sendToConsole($type, $message)
{
    switch ($type) {
        case 'log':
            echo "<script>console.log('" . $message . "');</script>";
            break;
        case 'warn':
            echo "<script>console.warn('" . $message . "');</script>";
            break;
        case 'error':
            echo "<script>console.error('" . $message . "');</script>";
            break;
    }
}

function insertGeomToPostgis($link, $arrayPropietariosNamesSortedASC, $nombreAPC, $folio, $geom)
{
    $stringPropietariosNamesSortedASC = '';
    $stringPropietariosNamesSortedASC = join(',', $arrayPropietariosNamesSortedASC);
    //sendToConsole("log", "Intentando guardar en PostGIS");
    $sqlInsertGeom = "INSERT INTO shape_poligonos_ccl_merge_final(propietari, nombre_apc, folio, geom) VALUES ('$stringPropietariosNamesSortedASC', '$nombreAPC', '$folio', ST_Multi(ST_GeomFromText('$geom', 48402))     )";
    //echo "\n\n" . $sqlInsertGeom;
    $resultInsertGeom = pg_query($link, $sqlInsertGeom);
    if ($resultInsertGeom) {
        return true;
    }
    return false;
}
#######################################################################################

if ($userLogged != '') {
    #RECUPERAMOS VARIABLES NECESARIAS PARA DEFINIR LA RUTA DE LOS ARCHIVOS
    $idAPC = trim($_POST['tbIdAPC']);
    $folio = trim($_POST['tbFolioTempModalEdit']);
    $nombreAPC = trim($_POST['tbNombreAPCTempModalEdit']);
    $selectedAction = trim($_POST['cbOpcionModalEdit']);
    $selectedStatus = trim($_POST['cbEstatusModalEdit']);
    $motivoNoProcede = trim($_POST['tbMotivoNoProcedeRegresadoModalEdit']);
    $fechaCertificacion = trim($_POST['dbFechaCertificacionModalEdit']);
    $numeroSEDEMA = trim($_POST['tbNumeroSEDEMAModalEdit']);

    #VERIFICAMOS QUE LOS DATOS ESTEN DEFINIDOS Y QUE NO ESTEN VACIÓS
    if (
        isset($idAPC) && $idAPC != '' &&
        isset($selectedAction) && $selectedAction != ''
    ) {
        //echo "idAPC: " . $idAPC . "\n";
        //echo "selectedAction: " . $selectedAction . "\n";
        //echo "selectedStatus: " . $selectedStatus . "\n";

        #DEPENDIENDO DE LA ACCION SELECCIONADA EN EL FORM DE JS EJECUTAMOS LOS CAMBIOS
        if ($selectedAction == 'Cambiar estatus' && isset($selectedStatus) && $selectedStatus != '') {
            switch ($selectedStatus) {
                case 'Certificada':
                    #VERIFICAMOS QUE LA EXTENSION DEL CERTIFICADO SEA VALIDA
                    $certificateValid = isExtensionValid($_FILES['fbCertificadoModalEdit'], 'pdf');
                    if ($certificateValid) {
                        #GENERAMOS UN NOMBRE DE CARPETA VALIDO PARA LINUX
                        $folderNameAPC = convertToValidPath($nombreAPC);
                        $generalPath = "documentacion/F" . $folio . "_" . $folderNameAPC . "/";
                        $certificatePath = $generalPath . "Certificado/";
                        $certificateName = "F" . $folio . "_" . $folderNameAPC . "_cer.pdf";
                        #SUBIMOS LOS ARCHIVOS ADJUNTOS
                        $contadorArchivosUpload = 0;
                        if (file_exists($generalPath) || @mkdir($generalPath)) {
                            mkdir($certificatePath, 0755, true);
                            $origenCertificado = $_FILES["fbCertificadoModalEdit"]["tmp_name"];
                            $destinoCertificado = $certificatePath . $certificateName;
                            if (@move_uploaded_file($origenCertificado, $destinoCertificado)) {
                                $contadorArchivosUpload++;
                                //sendToConsole("log", "Archivo de certificado: UPLOAD OK");
                            } else {
                                echo "ERROR, No se pudo subir al servidor el archivo de certificado.";
                                //sendToConsole("error", "No se pudo subir al servidor el archivo de certificado.");
                            }
                        } else {
                            echo "ERROR, No se pudo crear en el servidor la carpeta de certificado.";
                            //sendToConsole("error", "No se pudo crear en el servidor la carpeta de documentación.");
                        }
                        if ($contadorArchivosUpload === 1) {
                            $queryUpdateStatus = pg_query($link, "UPDATE apc_principal SET estatus='$selectedStatus',fecha_certificacion='$fechaCertificacion',fecha_ult_modif='$fechaActual',usuario_ult_modif='$userLogged',numero_sedema='$numeroSEDEMA', motivo_no_procede=null, ruta_certificado='$destinoCertificado' WHERE id_principal='$idAPC'");
                            if ($queryUpdateStatus) {
                                echo "OK, Se actualizó correctamente el APC.";
                            } else {
                                echo "ERROR, No se pudo actualizar la información del APC en la BD.";
                            }
                        } else {
                            echo "ERROR, No se pudieron subir los archivos al servidor.";
                            //sendToConsole("error", "No se pudieron subir los archivos al servidor.");
                        }
                    } else {
                        echo "ERROR, El archivo de certificado tiene una extensión no permitida.";
                        //sendToConsole("error", "El archivo de documentación o fotos tiene una extensión no permitida.");
                    }
                    break;
                case 'No procede':
                    $queryUpdateStatus = pg_query($link, "UPDATE apc_principal SET estatus='$selectedStatus',fecha_certificacion=null,fecha_ult_modif='$fechaActual',usuario_ult_modif='$userLogged',numero_sedema=null,ruta_certificado=null,motivo_no_procede='$motivoNoProcede' WHERE id_principal='$idAPC'");
                    if ($queryUpdateStatus) {
                        echo "OK, Se actualizó correctamente el APC.";
                    } else {
                        echo "ERROR, No se pudo actualizar la información del APC en la BD.";
                    }
                    break;
                case 'Regresado a SEDEMA por error':
                    $result = pg_query($link, "UPDATE apc_principal SET estatus='$selectedStatus',fecha_ult_modif='$fechaActual',usuario_ult_modif='$userLogged',motivo_no_procede='$motivoNoProcede' WHERE id_principal='$idAPC'");
                    if ($result) {
                        echo "OK, Se actualizó correctamente el APC.";
                    } else {
                        echo "ERROR, No se pudo actualizar la información del APC en la BD.";
                    }
                    break;
            }

        } else if ($selectedAction == 'Subir shapefile') {
            #VALIDAMOS SI SE ESPECIFICO UN ARCHIVO EN EL CAMPO SHAPEFILE
            $haveShapefile = haveShapefile($_FILES["fbShapefileModalEdit"]);
            ##REVISAMOS QUE EL ARCHIVO SHAPEFILE SEA VALIDO
            if ($haveShapefile) {
                $shapefileValid = isExtensionValid($_FILES['fbShapefileModalEdit'], 'zip');
                if ($shapefileValid) {
                    $folderNameAPC = convertToValidPath($nombreAPC);
                    $generalPath = "documentacion/F" . $folio . "_" . $folderNameAPC . "/";
                    $shapefilePath = $generalPath . "Shapefile/";
                    $shapeFileName = "F" . $folio . "_" . $folderNameAPC . "_shapefile.zip";

                    #VALIDATE ###########################################################
                    if (file_exists($generalPath) || @mkdir($generalPath)) {
                        mkdir($shapefilePath, 0755, true);
                        ##ELIMINAR TODO EL CONTENIDO DEL DIRECTORIO SHAPEFILE
                        $files = glob($shapefilePath . '*');
                        foreach ($files as $file) {
                            if (is_file($file)) {
                                unlink($file);
                            }
                        }
                        $origenShapefile = $_FILES["fbShapefileModalEdit"]["tmp_name"];
                        $destinoShapefile = $shapefilePath . $shapeFileName;
                        if (@move_uploaded_file($origenShapefile, $destinoShapefile)) {
                            ##DESCOMPRIMIMOS EL SHAPEFILE
                            $zip = new ZipArchive;
                            $res = $zip->open($destinoShapefile);
                            if ($res === true) {
                                $extraido = $zip->extractTo($shapefilePath);
                                $zip->close();
                                if ($extraido === true) {
                                    //sendToConsole('log', 'OK, Se extrajo correctamente el archivo shapefile.');
                                    ##LEEMOS LA INFORMACION DEL SHAPEFILE CON EL PLUGIN 'PHP SHAPEFILE'
                                    $filasEnElShapefile = 0;

                                    try {
                                        $arrayWithSHPFiles = glob($shapefilePath . "*.shp");
                                        $fullPathToSHP = $arrayWithSHPFiles[0];
                                        $ShapeFile = new ShapeFile($fullPathToSHP);
                                        while ($record = $ShapeFile->getRecord(ShapeFile::GEOMETRY_WKT)) {
                                            if ($record['dbf']['_deleted']) {
                                                continue;
                                            }
                                            $filasEnElShapefile++;
                                            $geom = $record['shp'];
                                            $dbfUTF8 = $record['dbf'];
                                            $dbfISO8859 = array_map("utf8_decode", $dbfUTF8);
                                        }

                                        ###### DEBUG ENCODING #################
                                        /*echo "\n\n";
                                        print_r($geom);
                                        echo "\n\n";
                                        print_r($dbfISO8859);*/
                                        #######################################

                                        #VERIFICAMOS QUE LA GEOMETRIA SEA UN POLIGONO (SOLO ACEPTAREMOS POLIGONOS O MULTIPOLIGONOS)
                                        if (strpos($geom, 'POLYGON') !== false || strpos($geom, 'MULTIPOLYGON') !== false) {
                                            //sendToConsole('log', 'OK, Si es un poligono');

                                            #VERIFICAMOS QUE LOS PROPIETARIOS COINCIDAN (FORMULARIO Y SHAPEFILE) ##################
                                            //sendToConsole("warn", "VALIDANDO PROPIETARIOS");

                                            #OBTENEMOS LOS PROPIETARIOS ACTUALES DE LA BASE DE DATOS
                                            $propietarios = array();
                                            $stringPropietariosIDs = "";
                                            $arrayPropietariosNamesSortedASC = array();
                                            $queryGetCurrentPropietaries = pg_query($link, "SELECT id_propietario FROM prop_apcs WHERE id_apc = $idAPC");
                                            while ($row = pg_fetch_assoc($queryGetCurrentPropietaries)) {
                                                array_push($propietarios, $row['id_propietario']);
                                            }
                                            $stringPropietariosIDs = join(',', $propietarios);

                                            $sqlGetPropietariesSortedASC = "SELECT id, name
                                            FROM (
                                                SELECT propietary.id_propietario AS id, trim(replace(propietary.nombre_completo, '  ', ' ')) AS name
                                                FROM (
                                                    SELECT id_propietario, (nombre || ' ' || apat || ' ' || amat) AS nombre_completo
                                                    FROM public.propietario
                                                    ORDER BY nombre ASC
                                                ) AS propietary
                                            ) AS final
                                            WHERE id IN ($stringPropietariosIDs)
                                            ORDER BY name ASC";
                                            $queryGetPropietariesSortedASC = pg_query($link, $sqlGetPropietariesSortedASC);
                                            while ($row = pg_fetch_assoc($queryGetPropietariesSortedASC)) {
                                                array_push($arrayPropietariosNamesSortedASC, $row['name']);
                                            }

                                            #DATOS DEL SHAPEFILE
                                            $arrayPropietariosShapefile = explode(",", $dbfISO8859['PROPIETARI']);
                                            $countPropietariosShapefile = count($arrayPropietariosShapefile);
                                            $countPropietariosDatabase = count($arrayPropietariosNamesSortedASC);

                                            if ($countPropietariosDatabase == $countPropietariosShapefile) {
                                                //sendToConsole('log', 'OK, Los propietarios coinciden en numero');
                                                $countPropietariosMatches = 0;
                                                foreach ($arrayPropietariosNamesSortedASC as $currentPropietarioDB) {
                                                    $currentPropietarioDB = trim($currentPropietarioDB);
                                                    foreach ($arrayPropietariosShapefile as $currentPropietarioShapefile) {
                                                        $currentPropietarioShapefile = trim($currentPropietarioShapefile);
                                                        if ($currentPropietarioDB == $currentPropietarioShapefile) {
                                                            //sendToConsole("log", "    (match) " . $currentPropietarioDB . "==" . $currentPropietarioShapefile);
                                                            $countPropietariosMatches++;
                                                        } else {
                                                            //sendToConsole("log", "    (x) " . $currentPropietarioDB . "==" . $currentPropietarioShapefile);
                                                        }
                                                    }
                                                }

                                                if ($countPropietariosMatches == $countPropietariosShapefile) {
                                                    //sendToConsole('log', 'OK, coincidieron todos los propietarios.');
                                                    #VERIFICAMOS QUE EL NOMBRE DEL APC COINCIDA (BASE DE DATOS Y SHAPEFILE)
                                                    //sendToConsole("warn", "VALIDANDO NOMBRE DE APC");
                                                    $nombreAPCShape = trim($dbfISO8859['NOMBRE_APC']);
                                                    //sendToConsole("log", $nombreAPC . "==" . $nombreAPCShape);
                                                    if ($nombreAPC == $nombreAPCShape) {
                                                        //sendToConsole("log", "OK, Coinciden los nombres del APC.");

                                                        $resultInsertGeomToPostgis = insertGeomToPostgis($link, $arrayPropietariosNamesSortedASC, $nombreAPC, $folio, $geom);
                                                        if ($resultInsertGeomToPostgis) {
                                                            echo "OK, Se insertó correctamente el shapefile del APC.";
                                                        } else {
                                                            echo "ERROR, No se pudo insertar la geometría del APC en la BD.";
                                                            //sendToConsole("error", "No se pudo insertar la geometría del APC en la BD.");
                                                        }

                                                    } else {
                                                        ##ELIMINAR TODO EL CONTENIDO Y LA CARPETA RECIEN CREADA (EVITAR BASURA EN EL SERVIDOR).
                                                        $pathTruncated = substr($generalPath, 0, -1);
                                                        recursiveRemoveDirectory($pathTruncated);
                                                        echo "ERROR, No coinciden los nombres del APC.";
                                                        //sendToConsole("error", "No coinciden los nombres del APC.");
                                                    }
                                                    ################################################################################################
                                                } else {
                                                    echo "ERROR, Los nombres de los propietarios no coinciden.";
                                                    //sendToConsole("error", "No coinciden todos los propietarios.");
                                                }
                                            } else {
                                                echo "ERROR, El numero de propietarios es diferente.";
                                            }
                                            ################################################################################################

                                        }

                                    } catch (ShapeFileException $e) {
                                        //sendToConsole("error", $e->getCode() . ' (' . $e->getErrorType() . '): ' . $e->getMessage());
                                        $error = $e->getErrorType() . " - " . $e->getMessage();
                                        echo "ERROR," . $error;
                                        exit();
                                    }

                                }
                            }

                        } else {
                            echo "ERROR, No se pudo subir el archivo shapefile al servidor.";
                            //sendToConsole("error", "No se pudo subir el archivo shapefile al servidor.");
                        }
                    }

                } else {
                    echo "ERROR, La extensión del archivo shapefile no es valida.";
                    //sendToConsole("error", "La extensión del archivo shapefile no es valida.");
                }

            }

        }

    } else {
        echo "ERROR,Ingresaste información vacía.";
    }

} else {
    echo "ERROR,Ocurrió un problema con sus credenciales.";
}
