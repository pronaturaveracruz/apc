<?php
	include "../includes/conexion.php";
	$link = ConectarsePostgreSQL();

	$cargadosDatagrid = isset($_POST['cargadosDatagrid']) ? $_POST['cargadosDatagrid'] : '';

	if($cargadosDatagrid == '')
	{
		$myQuery= "SELECT propietary.id_propietario, trim(replace(propietary.nombre_completo, '  ', ' ')) AS nc_fixed_spaces
		FROM 
		(
			SELECT id_propietario, (nombre || ' ' || apat || ' ' || amat) AS nombre_completo 
			FROM public.propietario 
			ORDER BY nombre ASC
		)AS propietary
		ORDER BY propietary.nombre_completo ASC";		
	}
	else
	{
		$where ="";

		$arrayCargadosDatagrid = explode(",",$cargadosDatagrid);
		for($i=0;$i<count($arrayCargadosDatagrid);$i++)
		{
			if($i==0)
				$where.= "WHERE propietary.id_propietario !='".$arrayCargadosDatagrid[$i]."'";	
			else
				$where.= " AND propietary.id_propietario !='".$arrayCargadosDatagrid[$i]."'";
		}

		$myQuery= "SELECT propietary.id_propietario, trim(replace(propietary.nombre_completo, '  ', ' ')) AS nc_fixed_spaces
		FROM 
		(
			SELECT id_propietario, (nombre || ' ' || apat || ' ' || amat) AS nombre_completo 
			FROM public.propietario 
			ORDER BY nombre ASC
		)AS propietary ".$where."
		ORDER BY propietary.nombre_completo ASC";
	}

	$sql = pg_query($link, $myQuery);
	$items = array();	
	while($row = pg_fetch_object($sql))
	{
		array_push($items, $row);
	}
	echo json_encode($items);
?>