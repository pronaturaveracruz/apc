<!-- MODAL AGREGAR -->
<div id="modalAdd" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color:SeaGreen;" id="labelInsert">Agregar nuevo registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="alertInsertOkModalAdd" class="alert alert-success alert-dismissible fade show text-center d-none"
                    role="alert">
                    <label id="labelInsertOkModalAdd"></label>
                </div>

                <div id="alertErrorModalAdd" class="alert alert-danger alert-dismissible fade show text-center d-none"
                    role="alert">
                    <label id="labelErrorModalAdd"></label>
                </div>

                <form id="formAddNew" method="post" enctype="multipart/form-data">

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbPropietario" name="cbPropietario" style="width:100%;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <input id="tagPropietarios" name="tagPropietarios[]" style="width:100%">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="tbFolio" name="tbFolio" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="tbNombreAPC" name="tbNombreAPC" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="nbHectareas" name="nbHectareas" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="nbHectareasCertificadas" name="nbHectareasCertificadas" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbEstado" name="cbEstado" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbMunicipio" name="cbMunicipio" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbLocalidad" name="cbLocalidad" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="dbFechaIngreso" name="dbFechaIngreso" style="width:100%;">
                        </div>
                    </div>

                    <!-- OBJETIVOS DE LA CERTIFICACION -->
                    <div class="row" style="border: 0px solid coral; margin:15px 0;">
                        <div class="col-12" style="border: 0px solid green;">
                            <span style="width:100%; color:SeaGreen;">
                                Objetivos de la certificación
                                <hr>
                            </span>

                            <input id="cbVegetacion" name="cbVegetacion" style="width:100%;">
                            <input id="tagVegetacion" name="tagVegetacion[]" style="width:100%;">

                            <span style="width:100%;">&nbsp;</span>

                            <input id="cbArboles" name="cbArboles" style="width:100%;">
                            <input id="tagArboles" name="tagArboles[]" style="width:100%">

                            <span style="width:100%;">&nbsp;</span>

                            <input id="cbMamiferos" name="cbMamiferos" style="width:100%;">
                            <input id="tagMamiferos" name="tagMamiferos[]" style="width:100%">

                            <span style="width:100%;">&nbsp;</span>

                            <input id="cbAves" name="cbAves" style="width:100%;">
                            <input id="tagAves" name="tagAves[]" style="width:100%">

                            <span style="width:100%;">&nbsp;</span>

                            <input id="cbCuerposAgua" name="cbCuerposAgua" style="width:100%;">
                            <input id="tagCuerposAgua" name="tagCuerposAgua[]" style="width:100%">
                        </div>
                    </div>



                    <div class="row" style="border: 0px solid coral; margin:15px 0;">
                        <div class="col-12" style="border: 0px solid green;">
                            <span style="width:100%;">
                                <hr></span>
                        </div>
                    </div>
                    <!-- END OBJETIVOS DE LA CERTIFICACION -->

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbTecnico" name="cbTecnico" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbZona" name="cbZona" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbProyecto" name="cbProyecto" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbFase" name="cbFase" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="tbVigenciaAPC" name="tbVigenciaAPC" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbTienePlanManejo" name="cbTienePlanManejo" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="tbVigenciaPlanManejo" name="tbVigenciaPlanManejo" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbTenencia" name="cbTenencia" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="tbObservaciones" name="tbObservaciones" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbPerteneceAlSello" name="cbPerteneceAlSello" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbMarca" name="cbMarca" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="fbDocumentos" name="fbDocumentos" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="fbFotos" name="fbFotos" style="width:100%;">
                        </div>
                    </div>

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="fbShapefile" name="fbShapefile" style="width:100%;">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button id="btnSaveFormAddNew" type="submit" class="btn btn-save-form-add-new">Agregar</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL AGREGAR NUEVO PROPIETARIO -->
<div id="modalAddNewPropietary" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color:DimGrey;" id="labelInsert">Agregar nuevo propietario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="alertErrorModalAddNewPropietary" class="alert alert-danger alert-dismissible fade show text-center d-none"
                    role="alert">
                    <label id="labelErrorModalAddNewPropietary"></label>
                </div>
                <form id="formAddNewPropietary" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-12">
                            <input id="tbNombrePropietario" name="tbNombrePropietario" style="width:100%;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input id="tbAmatPropietario" name="tbAmatPropietario" style="width:100%;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input id="tbApatPropietario" name="tbApatPropietario" style="width:100%;">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnSaveNewPropietary" type="submit" class="btn btn-save-form-add-new">Guardar y regresar</button>
            </div>
        </div>
    </div>
</div>


<!-- MODAL EDITAR -->
<div id="modalEdit" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color:DarkOrange;" id="labelInsert">Editar registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="alertInsertOkModalEdit" class="alert alert-success alert-dismissible fade show text-center d-none"
                    role="alert">
                    <label id="labelInsertOkModalEdit"></label>
                </div>

                <div id="alertErrorModalEdit" class="alert alert-danger alert-dismissible fade show text-center d-none"
                    role="alert">
                    <label id="labelErrorModalEdit"></label>
                </div>

                <form id="formEdit" method="post" enctype="multipart/form-data">

                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green; font-size:0.8em;">

                            <input id="tbIdAPC" name="tbIdAPC" style="width:100%;">    
                            <input id="tbNombreAPCTempModalEdit" name="tbNombreAPCTempModalEdit" style="width:100%;">
                            <input id="tbFolioTempModalEdit" name="tbFolioTempModalEdit" style="width:100%">
                            <input id="tbEstatusTempModalEdit" name="tbEstatusTempModalEdit" style="width:100%">
                            <input id="tbGeomPointTempModalEdit" name="tbGeomPointTempModalEdit" style="width:100%">
                            <input id="tbGeomPolyTempModalEdit" name="tbGeomPolyTempModalEdit" style="width:100%">
                            <input id="tbGeomResultTempModalEdit" name="tbGeomResultTempModalEdit" style="width:100%">
                            
                            <span style="color:Dimgray; display:block; margin-bottom:15px; font-size:1.2em;">                             
                                <span id="selectedNombreAPCModalEdit">Nombre del APC</span> | <span id="selectedFolioModalEdit">Folio</span><br>
                                <span id="selectedEstatusModalEdit">Estatus</span><br>
                                <span id="selectedTieneShapefileModalEdit">Tiene shapefile</span>                        
                            </span>
                            
                        </div>
                    </div>

                    <!-- SELECCION DE UNA ACCIÓN -->
                    <div class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbOpcionModalEdit" name="cbOpcionModalEdit" style="width:100%;">
                        </div>
                    </div>

                    <!-- SELECCIONÓ ACCION: CAMBIAR ESTATUS -->
                    <div id="statusFields"class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="cbEstatusModalEdit" name="cbEstatusModalEdit" style="width:100%;">
                        </div>
                    </div>

                    <!-- SELECCIONÓ ACCION: SUBIR SHAPEFILE -->
                    <div id="shapefileFields"class="row" style="border: 0px solid coral;">
                        <div class="col-12" style="border: 0px solid green;">
                            <input id="fbShapefileModalEdit" name="fbShapefileModalEdit" style="width:100%;">
                        </div>
                    </div>

                    <!-- SELECCIONÓ STATUS: NO PROCEDE -->
                    <div id="noProcedeWrapper">
                        <input id="tbMotivoNoProcedeRegresadoModalEdit" name="tbMotivoNoProcedeRegresadoModalEdit" style="width:100%;">
                    </div>

                    <!-- SELECCIONÓ STATUS: CERTIFICADA -->
                    <div id="certificadaWrapper">
                        <input id="dbFechaCertificacionModalEdit" name="dbFechaCertificacionModalEdit" style="width:100%;">
                        <input id="tbNumeroSEDEMAModalEdit" name="tbNumeroSEDEMAModalEdit" style="width:100%;">
                        <input id="fbCertificadoModalEdit" name="fbCertificadoModalEdit" style="width:100%;">
                    </div>

                    







                    





                </form>
            </div>
            <div class="modal-footer">
                <button id="btnSaveFormEdit" type="submit" class="btn btn-save-form-edit">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>