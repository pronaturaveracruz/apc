<?php
date_default_timezone_set("Mexico/General");
session_start();
$userLogged = isset($_SESSION['userLogged']) ? $_SESSION['userLogged'] : '';
$fecha_actual = date('Y-m-d');
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

if ($userLogged != '') {

    #RECUPERAMOS VARIABLES
    $nombre = trim($_POST['tbNombrePropietario']);
    $apat = trim($_POST['tbAmatPropietario']);
    $amat = trim($_POST['tbApatPropietario']);

    //PONEMOS LA PRIMERA LETRA COMO MAYUSCULA
    $nombre = ucfirst(mb_strtolower($nombre));
    $apat = ucfirst(mb_strtolower($apat));
    $amat = ucfirst(mb_strtolower($amat));

    #VERIFICAMOS QUE LOS DATOS ESTEN DEFINIDOS Y QUE NO ESTEN VACIÓS
    if (isset($nombre) && $nombre != '' && isset($apat) && $apat != '' && isset($amat) && $amat != '') {

        #VERIFICAR SI ESE USUARIO YA SE INGRESO ANTERIORMENTE A LA BASE DE DATOS
        $aux = 0;
        $queryDuplicado = pg_query($link, "SELECT COUNT(*)
        FROM propietario
        WHERE
              lower(unaccent(nombre)) = lower('$nombre') AND
              lower(unaccent(apat)) = lower('$apat') AND
              lower(unaccent(amat)) = lower('$amat')");

        while ($rowDuplicado = pg_fetch_row($queryDuplicado)) {
            if ($rowDuplicado[0] > 0) {
                $aux++;
            }
        }

        #INICIAMOS LAS VALIDACIONES
        if ($aux > 0) {
            echo "ERROR,Este propietario ya lo ingresaste anteriormente.";
        } else {
            #INSERTAMOS EN LA BASE DE DATOS
            $sqlInsert = "INSERT INTO propietario (nombre, apat, amat, fecha_ult_modif, usuario_ult_modif) VALUES ('$nombre','$apat','$amat','$fecha_actual','$userLogged') RETURNING id_propietario";
            $result = pg_query($link, $sqlInsert);
            $affectedRows = pg_affected_rows($result);
            if ($affectedRows > 0) {
                $insert_row = pg_fetch_row($result);
                $insert_id = $insert_row[0];
                echo "OK,<b>¡Muy bien!</b> Agregaste correctamente el nuevo propietario.," . $insert_id;
            } else {
                echo "ERROR,No se pudó insertar.";
            }
        }
    } else {
        echo "ERROR,Ingresaste información vacía.<br>Corrígela para poder continuar.";
    }
} else {
    echo "ERROR,Ocurrió un problema con sus credenciales";
}
