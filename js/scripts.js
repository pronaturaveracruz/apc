//## PERSONALIZANDO EL NAVBAR ###########################################################
navbarTitle.innerHTML = "APC";
showBackButton(true);
var items = [{
        //state: 'active',
        id: 'link-consultas',
        href: 'index.php',
        target: '_self',
        label: 'Consultas'
    },
    {
        id: 'link-geoportal',
        href: 'gis/index.php',
        target: '_blank',
        label: 'Geoportal'
    },
    {
        id: 'link-galleta',
        href: 'galleta/index.php',
        target: '_blank',
        label: 'Galleta'
    }
];
addLinks(items);
//#####################################################################################


//## VARIABLES GLOBALES ###########################################################
var arraySelectedPropietariesForTagBox = [];
var arraySelectedVegetationForTagBox = [];
var arraySelectedTreesForTagBox = [];
var arraySelectedMammalsForTagBox = [];
var arraySelectedBirdsForTagBox = [];
var arraySelectedWaterBodiesForTagBox = [];
var linkConsultas = document.getElementById('link-consultas');
//#####################################################################################

// Defaults
linkConsultas.classList.add('active');

//#FUNCTIONS
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function updateRowNumbers() {
    var data = $("#dg").datagrid('getData');
    var filtered = data.total;
    var all = data.all;
    $('#rowsMessage').html('Mostrando <span style="color:IndianRed; text-decoration: underline;">' + filtered + '</span> de ' + all + '.');
}

function loadTooltips() {
    //SHAPEFILE ICON
    $('.ttpWithShapefile').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'SkyBlue',
                borderColor: 'SkyBlue '
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 120,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttWithShapefile'
            });
        }
    });

    $('.ttpWithoutShapefile').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'lightgray',
                borderColor: 'lightgray '
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 95,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttWithoutShapefile'
            });
        }
    });


    //STATUS
    $('.ttpStatusCertified').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'MediumSeaGreen',
                borderColor: 'MediumSeaGreen '
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 80,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttStatusCertified'
            });
        }
    });

    $('.ttpStatusDecreed').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'Teal',
                borderColor: 'Teal '
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 80,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttStatusDecreed'
            });
        }
    });

    $('.ttpStatusInProgress').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'SandyBrown',
                borderColor: 'SandyBrown '
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 80,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttStatusInProgress'
            });
        }
    });

    $('.ttpStatusNotProceed').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'Crimson',
                borderColor: 'Crimson '
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 80,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttStatusNotProceed'
            });
        }
    });

    $('.ttpStatusReturned').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'SteelBlue',
                borderColor: 'SteelBlue '
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 120,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttStatusReturned'
            });
        }
    });

    //HECTAREAS CERTIFICADAS
    $('.ttpHectareas').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: '#d89e00',
                borderColor: '#ffecba'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>Hectareas certificadas: ' + param + ' ha.</span>',
                border: false,
                bodyCls: 'panelttHectareas'
            });
        }
    });
    //VEGETACION
    $('.ttpVegetacion').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'SeaGreen',
                borderColor: 'SeaGreen'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttVegetacion'
            });
        }
    });


    //OBSERVACIONES CERTIFICACION
    $('.ttpOVegetacion').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'DodgerBlue',
                borderColor: 'DodgerBlue'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttOVegetacion'
            });
        }
    });

    //OBSERVACIONES
    $('.ttpObservaciones').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'DodgerBlue',
                borderColor: 'DodgerBlue'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttObservaciones'
            });
        }
    });


    //ARBOLES
    $('.ttpArboles').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'ForestGreen',
                borderColor: 'ForestGreen'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttArboles'
            });
        }
    });
    //MAMIFEROS
    $('.ttpMamiferos').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'Maroon',
                borderColor: 'Maroon'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttMamiferos'
            });
        }
    });
    //AVES
    $('.ttpAves').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'Chocolate',
                borderColor: 'Chocolate'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttAves'
            });
        }
    });
    //ANFIBIOS
    $('.ttpAnfibios').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'OliveDrab',
                borderColor: 'OliveDrab'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttAnfibios'
            });
        }
    });
    //EPIFITAS
    $('.ttpEpifitas').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'MediumSlateBlue',
                borderColor: 'MediumSlateBlue'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttEpifitas'
            });
        }
    });
    //CUERPOS DE AGUA
    $('.ttpCuerposAgua').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'SkyBlue',
                borderColor: 'SkyBlue'
            });
        },
        onUpdate: function (content) {
            var opts = $(this).tooltip('options');
            var param = opts.param;
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>' + param + '</span>',
                border: false,
                bodyCls: 'panelttCuerposAgua'
            });
        }
    });
    //SIN ESPECIES
    $('.ttpSinEspecies').tooltip({
        content: function () {
            return $('<div></div>')
        },
        position: 'right',
        onShow: function () {
            var t = $(this);
            t.tooltip('tip').css({
                backgroundColor: 'IndianRed',
                borderColor: 'IndianRed'
            });
        },
        onUpdate: function (content) {
            content.panel({
                width: 200,
                height: 'auto',
                content: '<span>No se registró ninguna especie</span>',
                border: false,
                bodyCls: 'panelttSinEspecies'
            });
        }
    });
}

function exportToExcel() {
    var folioFilterValue = $('#dg').datagrid('getFilterRule', 'folio');
    var nombre_apcFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_apc');
    var nombre_completoFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_completo');
    var hectareasFilterValue = $('#dg').datagrid('getFilterRule', 'hectareas');
    var estadoFilterValue = $('#dg').datagrid('getFilterRule', 'estado');
    var municipioFilterValue = $('#dg').datagrid('getFilterRule', 'municipio');
    var localidadFilterValue = $('#dg').datagrid('getFilterRule', 'localidad');
    var fecha_ingresoFilterValue = $('#dg').datagrid('getFilterRule', 'fecha_ingreso');
    var estatusFilterValue = $('#dg').datagrid('getFilterRule', 'estatus');
    var fecha_certificacionFilterValue = $('#dg').datagrid('getFilterRule', 'fecha_certificacion');
    var numero_sedemaFilterValue = $('#dg').datagrid('getFilterRule', 'numero_sedema');
    var vegetacionFilterValue = $('#dg').datagrid('getFilterRule', 'vegetacion');
    var especiesFilterValue = $('#dg').datagrid('getFilterRule', 'especies');
    var tecnicoFilterValue = $('#dg').datagrid('getFilterRule', 'tecnico');
    var descripcion_zonaFilterValue = $('#dg').datagrid('getFilterRule', 'descripcion_zona');
    var nombre_proyectoFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_proyecto');
    var faseFilterValue = $('#dg').datagrid('getFilterRule', 'fase');
    var cuenta_plan_manejoFilterValue = $('#dg').datagrid('getFilterRule', 'cuenta_plan_manejo');
    var tenenciaFilterValue = $('#dg').datagrid('getFilterRule', 'tenencia');
    var selloFilterValue = $('#dg').datagrid('getFilterRule', 'organizacion');
    var marcaFilterValue = $('#dg').datagrid('getFilterRule', 'org_marca');
    var figuraLegalFilterValue = $('#dg').datagrid('getFilterRule', 'figura_legal');

    if (folioFilterValue)
        folioFilterValue = folioFilterValue.field + "|" + folioFilterValue.op + "|" + folioFilterValue.value;
    else
        folioFilterValue = "";
    if (nombre_apcFilterValue)
        nombre_apcFilterValue = nombre_apcFilterValue.field + "|" + nombre_apcFilterValue.op + "|" + nombre_apcFilterValue.value;
    else
        nombre_apcFilterValue = "";
    if (nombre_completoFilterValue)
        nombre_completoFilterValue = nombre_completoFilterValue.field + "|" + nombre_completoFilterValue.op + "|" + nombre_completoFilterValue.value;
    else
        nombre_completoFilterValue = "";
    if (hectareasFilterValue)
        hectareasFilterValue = hectareasFilterValue.field + "|" + hectareasFilterValue.op + "|" + hectareasFilterValue.value;
    else
        hectareasFilterValue = "";
    if (estadoFilterValue)
        estadoFilterValue = estadoFilterValue.field + "|" + estadoFilterValue.op + "|" + estadoFilterValue.value;
    else
        estadoFilterValue = "";
    if (municipioFilterValue)
        municipioFilterValue = municipioFilterValue.field + "|" + municipioFilterValue.op + "|" + municipioFilterValue.value;
    else
        municipioFilterValue = "";
    if (localidadFilterValue)
        localidadFilterValue = localidadFilterValue.field + "|" + localidadFilterValue.op + "|" + localidadFilterValue.value;
    else
        localidadFilterValue = "";
    if (fecha_ingresoFilterValue)
        fecha_ingresoFilterValue = fecha_ingresoFilterValue.field + "|" + fecha_ingresoFilterValue.op + "|" + fecha_ingresoFilterValue.value;
    else
        fecha_ingresoFilterValue = "";
    if (estatusFilterValue)
        estatusFilterValue = estatusFilterValue.field + "|" + estatusFilterValue.op + "|" + estatusFilterValue.value;
    else
        estatusFilterValue = "";
    if (fecha_certificacionFilterValue)
        fecha_certificacionFilterValue = fecha_certificacionFilterValue.field + "|" + fecha_certificacionFilterValue.op + "|" + fecha_certificacionFilterValue.value;
    else
        fecha_certificacionFilterValue = "";
    if (numero_sedemaFilterValue)
        numero_sedemaFilterValue = numero_sedemaFilterValue.field + "|" + numero_sedemaFilterValue.op + "|" + numero_sedemaFilterValue.value;
    else
        numero_sedemaFilterValue = "";
    if (vegetacionFilterValue)
        vegetacionFilterValue = vegetacionFilterValue.field + "|" + vegetacionFilterValue.op + "|" + vegetacionFilterValue.value;
    else
        vegetacionFilterValue = "";
    if (especiesFilterValue)
        especiesFilterValue = especiesFilterValue.field + "|" + especiesFilterValue.op + "|" + especiesFilterValue.value;
    else
        especiesFilterValue = "";
    if (tecnicoFilterValue)
        tecnicoFilterValue = tecnicoFilterValue.field + "|" + tecnicoFilterValue.op + "|" + tecnicoFilterValue.value;
    else
        tecnicoFilterValue = "";
    if (descripcion_zonaFilterValue)
        descripcion_zonaFilterValue = descripcion_zonaFilterValue.field + "|" + descripcion_zonaFilterValue.op + "|" + descripcion_zonaFilterValue.value;
    else
        descripcion_zonaFilterValue = "";
    if (nombre_proyectoFilterValue)
        nombre_proyectoFilterValue = nombre_proyectoFilterValue.field + "|" + nombre_proyectoFilterValue.op + "|" + nombre_proyectoFilterValue.value;
    else
        nombre_proyectoFilterValue = "";
    if (faseFilterValue)
        faseFilterValue = faseFilterValue.field + "|" + faseFilterValue.op + "|" + faseFilterValue.value;
    else
        faseFilterValue = "";
    if (cuenta_plan_manejoFilterValue)
        cuenta_plan_manejoFilterValue = cuenta_plan_manejoFilterValue.field + "|" + cuenta_plan_manejoFilterValue.op + "|" + cuenta_plan_manejoFilterValue.value;
    else
        cuenta_plan_manejoFilterValue = "";
    if (tenenciaFilterValue)
        tenenciaFilterValue = tenenciaFilterValue.field + "|" + tenenciaFilterValue.op + "|" + tenenciaFilterValue.value;
    else
        tenenciaFilterValue = "";
    ///
    if (selloFilterValue)
        selloFilterValue = selloFilterValue.field + "|" + selloFilterValue.op + "|" + selloFilterValue.value;
    else
        selloFilterValue = "";
    if (marcaFilterValue)
        marcaFilterValue = marcaFilterValue.field + "|" + marcaFilterValue.op + "|" + marcaFilterValue.value;
    else
        marcaFilterValue = "";
    if (figuraLegalFilterValue)
        figuraLegalFilterValue = figuraLegalFilterValue.field + "|" + figuraLegalFilterValue.op + "|" + figuraLegalFilterValue.value;
    else
        figuraLegalFilterValue = "";

    $.post("exportToExcel.php", {
            folio: folioFilterValue,
            nombre_apc: nombre_apcFilterValue,
            nombre_completo: nombre_completoFilterValue,
            hectareas: hectareasFilterValue,
            estado: estadoFilterValue,
            municipio: municipioFilterValue,
            localidad: localidadFilterValue,
            fecha_ingreso: fecha_ingresoFilterValue,
            estatus: estatusFilterValue,
            fecha_certificacion: fecha_certificacionFilterValue,
            numero_sedema: numero_sedemaFilterValue,
            vegetacion: vegetacionFilterValue,
            especies: especiesFilterValue,
            tecnico: tecnicoFilterValue,
            descripcion_zona: descripcion_zonaFilterValue,
            nombre_proyecto: nombre_proyectoFilterValue,
            fase: faseFilterValue,
            cuenta_plan_manejo: cuenta_plan_manejoFilterValue,
            tenencia: tenenciaFilterValue,
            sello: selloFilterValue,
            marca: marcaFilterValue,
            figura_legal: figuraLegalFilterValue
        },
        function (data) {
            var arrayData = data.split(',');
            var estadoPHP = arrayData[0];
            if (arrayData.length > 1) {
                var rutaCSV = arrayData[1];
                var nombreCSV = arrayData[1].substring(14);
                //console.log("rutaCSV: " + rutaCSV);
                //console.log("nombreCSV: " + nombreCSV);
                $('#alertOkText').html("Se generó correctamente el archivo: " + nombreCSV);
                $('#alertOk').removeClass('d-none');
                setTimeout(function () {
                    $('#alertOk').addClass('d-none');
                }, 8000);
                var link = document.createElement("a");
                document.body.appendChild(link); //For working in Firefox
                link.download = nombreCSV;
                link.href = rutaCSV;
                link.target = "_self"; //For working in Firefox
                link.click();
            }
        }
    );
}

function exportToShapefile(proyeccion) {
    var folioFilterValue = $('#dg').datagrid('getFilterRule', 'folio');
    var nombre_apcFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_apc');
    var nombre_completoFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_completo');
    var hectareasFilterValue = $('#dg').datagrid('getFilterRule', 'hectareas');
    var estadoFilterValue = $('#dg').datagrid('getFilterRule', 'estado');
    var municipioFilterValue = $('#dg').datagrid('getFilterRule', 'municipio');
    var localidadFilterValue = $('#dg').datagrid('getFilterRule', 'localidad');
    var fecha_ingresoFilterValue = $('#dg').datagrid('getFilterRule', 'fecha_ingreso');
    var estatusFilterValue = $('#dg').datagrid('getFilterRule', 'estatus');
    var fecha_certificacionFilterValue = $('#dg').datagrid('getFilterRule', 'fecha_certificacion');
    var numero_sedemaFilterValue = $('#dg').datagrid('getFilterRule', 'numero_sedema');
    var vegetacionFilterValue = $('#dg').datagrid('getFilterRule', 'vegetacion');
    var especiesFilterValue = $('#dg').datagrid('getFilterRule', 'especies');
    var tecnicoFilterValue = $('#dg').datagrid('getFilterRule', 'tecnico');
    var descripcion_zonaFilterValue = $('#dg').datagrid('getFilterRule', 'descripcion_zona');
    var nombre_proyectoFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_proyecto');
    var faseFilterValue = $('#dg').datagrid('getFilterRule', 'fase');
    var cuenta_plan_manejoFilterValue = $('#dg').datagrid('getFilterRule', 'cuenta_plan_manejo');
    var tenenciaFilterValue = $('#dg').datagrid('getFilterRule', 'tenencia');
    var selloFilterValue = $('#dg').datagrid('getFilterRule', 'organizacion');
    var marcaFilterValue = $('#dg').datagrid('getFilterRule', 'org_marca');
    var figuraLegalFilterValue = $('#dg').datagrid('getFilterRule', 'figura_legal');

    if (folioFilterValue)
        folioFilterValue = folioFilterValue.field + "|" + folioFilterValue.op + "|" + folioFilterValue.value;
    else
        folioFilterValue = "";
    if (nombre_apcFilterValue)
        nombre_apcFilterValue = nombre_apcFilterValue.field + "|" + nombre_apcFilterValue.op + "|" + nombre_apcFilterValue.value;
    else
        nombre_apcFilterValue = "";
    if (nombre_completoFilterValue)
        nombre_completoFilterValue = nombre_completoFilterValue.field + "|" + nombre_completoFilterValue.op + "|" + nombre_completoFilterValue.value;
    else
        nombre_completoFilterValue = "";
    if (hectareasFilterValue)
        hectareasFilterValue = hectareasFilterValue.field + "|" + hectareasFilterValue.op + "|" + hectareasFilterValue.value;
    else
        hectareasFilterValue = "";
    if (estadoFilterValue)
        estadoFilterValue = estadoFilterValue.field + "|" + estadoFilterValue.op + "|" + estadoFilterValue.value;
    else
        estadoFilterValue = "";
    if (municipioFilterValue)
        municipioFilterValue = municipioFilterValue.field + "|" + municipioFilterValue.op + "|" + municipioFilterValue.value;
    else
        municipioFilterValue = "";
    if (localidadFilterValue)
        localidadFilterValue = localidadFilterValue.field + "|" + localidadFilterValue.op + "|" + localidadFilterValue.value;
    else
        localidadFilterValue = "";
    if (fecha_ingresoFilterValue)
        fecha_ingresoFilterValue = fecha_ingresoFilterValue.field + "|" + fecha_ingresoFilterValue.op + "|" + fecha_ingresoFilterValue.value;
    else
        fecha_ingresoFilterValue = "";
    if (estatusFilterValue)
        estatusFilterValue = estatusFilterValue.field + "|" + estatusFilterValue.op + "|" + estatusFilterValue.value;
    else
        estatusFilterValue = "";
    if (fecha_certificacionFilterValue)
        fecha_certificacionFilterValue = fecha_certificacionFilterValue.field + "|" + fecha_certificacionFilterValue.op + "|" + fecha_certificacionFilterValue.value;
    else
        fecha_certificacionFilterValue = "";
    if (numero_sedemaFilterValue)
        numero_sedemaFilterValue = numero_sedemaFilterValue.field + "|" + numero_sedemaFilterValue.op + "|" + numero_sedemaFilterValue.value;
    else
        numero_sedemaFilterValue = "";
    if (vegetacionFilterValue)
        vegetacionFilterValue = vegetacionFilterValue.field + "|" + vegetacionFilterValue.op + "|" + vegetacionFilterValue.value;
    else
        vegetacionFilterValue = "";
    if (especiesFilterValue)
        especiesFilterValue = especiesFilterValue.field + "|" + especiesFilterValue.op + "|" + especiesFilterValue.value;
    else
        especiesFilterValue = "";
    if (tecnicoFilterValue)
        tecnicoFilterValue = tecnicoFilterValue.field + "|" + tecnicoFilterValue.op + "|" + tecnicoFilterValue.value;
    else
        tecnicoFilterValue = "";
    if (descripcion_zonaFilterValue)
        descripcion_zonaFilterValue = descripcion_zonaFilterValue.field + "|" + descripcion_zonaFilterValue.op + "|" + descripcion_zonaFilterValue.value;
    else
        descripcion_zonaFilterValue = "";
    if (nombre_proyectoFilterValue)
        nombre_proyectoFilterValue = nombre_proyectoFilterValue.field + "|" + nombre_proyectoFilterValue.op + "|" + nombre_proyectoFilterValue.value;
    else
        nombre_proyectoFilterValue = "";
    if (faseFilterValue)
        faseFilterValue = faseFilterValue.field + "|" + faseFilterValue.op + "|" + faseFilterValue.value;
    else
        faseFilterValue = "";
    if (cuenta_plan_manejoFilterValue)
        cuenta_plan_manejoFilterValue = cuenta_plan_manejoFilterValue.field + "|" + cuenta_plan_manejoFilterValue.op + "|" + cuenta_plan_manejoFilterValue.value;
    else
        cuenta_plan_manejoFilterValue = "";
    if (tenenciaFilterValue)
        tenenciaFilterValue = tenenciaFilterValue.field + "|" + tenenciaFilterValue.op + "|" + tenenciaFilterValue.value;
    else
        tenenciaFilterValue = "";
    ////
    if (selloFilterValue)
        selloFilterValue = selloFilterValue.field + "|" + selloFilterValue.op + "|" + selloFilterValue.value;
    else
        selloFilterValue = "";
    if (marcaFilterValue)
        marcaFilterValue = marcaFilterValue.field + "|" + marcaFilterValue.op + "|" + marcaFilterValue.value;
    else
        marcaFilterValue = "";
    if (figuraLegalFilterValue)
        figuraLegalFilterValue = figuraLegalFilterValue.field + "|" + figuraLegalFilterValue.op + "|" + figuraLegalFilterValue.value;
    else
        figuraLegalFilterValue = "";

    $.post("exportToShapefile.php", {
            folio: folioFilterValue,
            nombre_apc: nombre_apcFilterValue,
            nombre_completo: nombre_completoFilterValue,
            hectareas: hectareasFilterValue,
            estado: estadoFilterValue,
            municipio: municipioFilterValue,
            localidad: localidadFilterValue,
            fecha_ingreso: fecha_ingresoFilterValue,
            estatus: estatusFilterValue,
            fecha_certificacion: fecha_certificacionFilterValue,
            numero_sedema: numero_sedemaFilterValue,
            vegetacion: vegetacionFilterValue,
            especies: especiesFilterValue,
            tecnico: tecnicoFilterValue,
            descripcion_zona: descripcion_zonaFilterValue,
            nombre_proyecto: nombre_proyectoFilterValue,
            fase: faseFilterValue,
            cuenta_plan_manejo: cuenta_plan_manejoFilterValue,
            tenencia: tenenciaFilterValue,
            proyeccion: proyeccion,
            sello: selloFilterValue,
            marca: marcaFilterValue,
            figura_legal: figuraLegalFilterValue
        },
        function (data) {
            var htmlText = "";
            var arrayData = data.split(',');
            var estadoPHP = arrayData[0];
            var rutaShape = "";
            if (arrayData.length > 1) {
                //console.log("estadoPHP: " + estadoPHP);
                //////////////////////////////////////////////////////////////////////////
                if (estadoPHP === 'ok') {
                    htmlText = 'Se generó correctamente el archivo: ';
                    if (arrayData[1] === 'ambos') {
                        if (arrayData[2] == 1)
                            htmlText += ' 1 punto y ';
                        else
                            htmlText += ' ' + arrayData[2] + ' puntos y ';
                        if (arrayData[3] == 1)
                            htmlText += ' 1 polígono.';
                        else
                            htmlText += ' ' + arrayData[3] + ' polígonos.';
                        rutaShape = arrayData[4];
                    } else if (arrayData[1] === 'puntos') {
                        if (arrayData[2] == 1)
                            htmlText += ' Contenido: 1 punto.';
                        else
                            htmlText += ' Contenido: ' + arrayData[2] + ' puntos.';
                        rutaShape = arrayData[3];
                    } else if (arrayData[1] === 'poligonos') {
                        if (arrayData[2] == 1)
                            htmlText += ' Contenido: 1 polígono.';
                        else
                            htmlText += ' Contenido: ' + arrayData[2] + ' polígonos.';
                        rutaShape = arrayData[3];
                    }
                    $('#alertOkText').html(htmlText);
                    $('#alertOk').removeClass('d-none');
                    setTimeout(function () {
                        $('#alertOk').addClass('d-none');
                    }, 8000);
                    var nombreShape = rutaShape.substring(18);
                    //console.log("rutaShape: " + rutaShape);
                    //console.log("nombreShape: " + nombreShape);
                    var link = document.createElement("a");
                    document.body.appendChild(link); //For working in Firefox
                    link.download = nombreShape;
                    link.href = rutaShape;
                    link.target = "_self"; //For working in Firefox
                    link.click();
                } else {
                    htmlText = 'Error: ' + arrayData[1];
                    $('#alertErrorText').html(htmlText);
                    $('#alertError').removeClass('d-none');
                    setTimeout(function () {
                        $('#alertError').addClass('d-none');
                    }, 8000);
                }
                /////////////////////////////////////////////////////////////////////////////
            }
        }
    );
}

function exportFiles(elementos) {
    var folioFilterValue = $('#dg').datagrid('getFilterRule', 'folio');
    var nombre_apcFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_apc');
    var nombre_completoFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_completo');
    var hectareasFilterValue = $('#dg').datagrid('getFilterRule', 'hectareas');
    var estadoFilterValue = $('#dg').datagrid('getFilterRule', 'estado');
    var municipioFilterValue = $('#dg').datagrid('getFilterRule', 'municipio');
    var localidadFilterValue = $('#dg').datagrid('getFilterRule', 'localidad');
    var fecha_ingresoFilterValue = $('#dg').datagrid('getFilterRule', 'fecha_ingreso');
    var estatusFilterValue = $('#dg').datagrid('getFilterRule', 'estatus');
    var fecha_certificacionFilterValue = $('#dg').datagrid('getFilterRule', 'fecha_certificacion');
    var numero_sedemaFilterValue = $('#dg').datagrid('getFilterRule', 'numero_sedema');
    var vegetacionFilterValue = $('#dg').datagrid('getFilterRule', 'vegetacion');
    var especiesFilterValue = $('#dg').datagrid('getFilterRule', 'especies');
    var tecnicoFilterValue = $('#dg').datagrid('getFilterRule', 'tecnico');
    var descripcion_zonaFilterValue = $('#dg').datagrid('getFilterRule', 'descripcion_zona');
    var nombre_proyectoFilterValue = $('#dg').datagrid('getFilterRule', 'nombre_proyecto');
    var faseFilterValue = $('#dg').datagrid('getFilterRule', 'fase');
    var cuenta_plan_manejoFilterValue = $('#dg').datagrid('getFilterRule', 'cuenta_plan_manejo');
    var tenenciaFilterValue = $('#dg').datagrid('getFilterRule', 'tenencia');
    var selloFilterValue = $('#dg').datagrid('getFilterRule', 'organizacion');
    var marcaFilterValue = $('#dg').datagrid('getFilterRule', 'org_marca');
    var figuraLegalFilterValue = $('#dg').datagrid('getFilterRule', 'figura_legal');

    if (folioFilterValue)
        folioFilterValue = folioFilterValue.field + "|" + folioFilterValue.op + "|" + folioFilterValue.value;
    else
        folioFilterValue = "";
    if (nombre_apcFilterValue)
        nombre_apcFilterValue = nombre_apcFilterValue.field + "|" + nombre_apcFilterValue.op + "|" + nombre_apcFilterValue.value;
    else
        nombre_apcFilterValue = "";
    if (nombre_completoFilterValue)
        nombre_completoFilterValue = nombre_completoFilterValue.field + "|" + nombre_completoFilterValue.op + "|" + nombre_completoFilterValue.value;
    else
        nombre_completoFilterValue = "";
    if (hectareasFilterValue)
        hectareasFilterValue = hectareasFilterValue.field + "|" + hectareasFilterValue.op + "|" + hectareasFilterValue.value;
    else
        hectareasFilterValue = "";
    if (estadoFilterValue)
        estadoFilterValue = estadoFilterValue.field + "|" + estadoFilterValue.op + "|" + estadoFilterValue.value;
    else
        estadoFilterValue = "";
    if (municipioFilterValue)
        municipioFilterValue = municipioFilterValue.field + "|" + municipioFilterValue.op + "|" + municipioFilterValue.value;
    else
        municipioFilterValue = "";
    if (localidadFilterValue)
        localidadFilterValue = localidadFilterValue.field + "|" + localidadFilterValue.op + "|" + localidadFilterValue.value;
    else
        localidadFilterValue = "";
    if (fecha_ingresoFilterValue)
        fecha_ingresoFilterValue = fecha_ingresoFilterValue.field + "|" + fecha_ingresoFilterValue.op + "|" + fecha_ingresoFilterValue.value;
    else
        fecha_ingresoFilterValue = "";
    if (estatusFilterValue)
        estatusFilterValue = estatusFilterValue.field + "|" + estatusFilterValue.op + "|" + estatusFilterValue.value;
    else
        estatusFilterValue = "";
    if (fecha_certificacionFilterValue)
        fecha_certificacionFilterValue = fecha_certificacionFilterValue.field + "|" + fecha_certificacionFilterValue.op + "|" + fecha_certificacionFilterValue.value;
    else
        fecha_certificacionFilterValue = "";
    if (numero_sedemaFilterValue)
        numero_sedemaFilterValue = numero_sedemaFilterValue.field + "|" + numero_sedemaFilterValue.op + "|" + numero_sedemaFilterValue.value;
    else
        numero_sedemaFilterValue = "";
    if (vegetacionFilterValue)
        vegetacionFilterValue = vegetacionFilterValue.field + "|" + vegetacionFilterValue.op + "|" + vegetacionFilterValue.value;
    else
        vegetacionFilterValue = "";
    if (especiesFilterValue)
        especiesFilterValue = especiesFilterValue.field + "|" + especiesFilterValue.op + "|" + especiesFilterValue.value;
    else
        especiesFilterValue = "";
    if (tecnicoFilterValue)
        tecnicoFilterValue = tecnicoFilterValue.field + "|" + tecnicoFilterValue.op + "|" + tecnicoFilterValue.value;
    else
        tecnicoFilterValue = "";
    if (descripcion_zonaFilterValue)
        descripcion_zonaFilterValue = descripcion_zonaFilterValue.field + "|" + descripcion_zonaFilterValue.op + "|" + descripcion_zonaFilterValue.value;
    else
        descripcion_zonaFilterValue = "";
    if (nombre_proyectoFilterValue)
        nombre_proyectoFilterValue = nombre_proyectoFilterValue.field + "|" + nombre_proyectoFilterValue.op + "|" + nombre_proyectoFilterValue.value;
    else
        nombre_proyectoFilterValue = "";
    if (faseFilterValue)
        faseFilterValue = faseFilterValue.field + "|" + faseFilterValue.op + "|" + faseFilterValue.value;
    else
        faseFilterValue = "";
    if (cuenta_plan_manejoFilterValue)
        cuenta_plan_manejoFilterValue = cuenta_plan_manejoFilterValue.field + "|" + cuenta_plan_manejoFilterValue.op + "|" + cuenta_plan_manejoFilterValue.value;
    else
        cuenta_plan_manejoFilterValue = "";
    if (tenenciaFilterValue)
        tenenciaFilterValue = tenenciaFilterValue.field + "|" + tenenciaFilterValue.op + "|" + tenenciaFilterValue.value;
    else
        tenenciaFilterValue = "";
    /////
    if (selloFilterValue)
        selloFilterValue = selloFilterValue.field + "|" + selloFilterValue.op + "|" + selloFilterValue.value;
    else
        selloFilterValue = "";
    if (marcaFilterValue)
        marcaFilterValue = marcaFilterValue.field + "|" + marcaFilterValue.op + "|" + marcaFilterValue.value;
    else
        marcaFilterValue = "";
    if (figuraLegalFilterValue)
        figuraLegalFilterValue = figuraLegalFilterValue.field + "|" + figuraLegalFilterValue.op + "|" + figuraLegalFilterValue.value;
    else
        figuraLegalFilterValue = "";

    $.post("exportFiles.php", {
            folio: folioFilterValue,
            nombre_apc: nombre_apcFilterValue,
            nombre_completo: nombre_completoFilterValue,
            hectareas: hectareasFilterValue,
            estado: estadoFilterValue,
            municipio: municipioFilterValue,
            localidad: localidadFilterValue,
            fecha_ingreso: fecha_ingresoFilterValue,
            estatus: estatusFilterValue,
            fecha_certificacion: fecha_certificacionFilterValue,
            numero_sedema: numero_sedemaFilterValue,
            vegetacion: vegetacionFilterValue,
            especies: especiesFilterValue,
            tecnico: tecnicoFilterValue,
            descripcion_zona: descripcion_zonaFilterValue,
            nombre_proyecto: nombre_proyectoFilterValue,
            fase: faseFilterValue,
            cuenta_plan_manejo: cuenta_plan_manejoFilterValue,
            tenencia: tenenciaFilterValue,
            elementos: elementos,
            sello: selloFilterValue,
            marca: marcaFilterValue,
            figura_legal: figuraLegalFilterValue
        },
        function (data) {
            var arrayData = data.split(',');
            var estadoPHP = arrayData[0];
            if (arrayData.length > 1) {
                //console.log("estadoPHP: " + estadoPHP);
                //////////////////////////////////////////////////////////////////////////////////
                if (estadoPHP === 'ok') {
                    var rutaZIP = arrayData[1];
                    var nombreZIP = arrayData[2];
                    $('#alertOkText').html("Se generó correctamente el archivo: " + nombreZIP);
                    $('#alertOk').removeClass('d-none');
                    setTimeout(function () {
                        $('#alertOk').addClass('d-none');
                    }, 8000);
                    var link = document.createElement("a");
                    document.body.appendChild(link); //For working in Firefox
                    link.download = nombreZIP;
                    link.href = rutaZIP;
                    link.target = "_self"; //For working in Firefox
                    link.click();
                } else {
                    htmlText = 'Ocurrió un error!';
                    $('#alertErrorText').html(htmlText);
                    $('#alertError').removeClass('d-none');
                    setTimeout(function () {
                        $('#alertError').addClass('d-none');
                    }, 8000);
                }
                ///////////////////////////////////////////////////////////////////////////////
            }
        }
    );
}

function saveNewPropietary() {
    $('#formAddNewPropietary').form('submit', {
        url: 'saveNewPropietary.php',
        onSubmit: function (param) {
            return $(this).form('enableValidation').form('validate');
        },
        success: function (result) {
            //console.log(result);                	
            var arrayResult = result.split(",");
            switch (arrayResult[0]) {
                case "ERROR":
                    console.error("Error: " + arrayResult[1]);
                    $('#alertErrorModalAddNewPropietary').removeClass('d-none');
                    $('#labelErrorModalAddNewPropietary').html(arrayResult[1]);
                    setTimeout(function () {
                        $('#alertErrorModalAddNewPropietary').addClass('d-none');
                    }, 3000);
                    break;
                case "OK":
                    console.log("Ok: " + arrayResult[1]);
                    $('#modalAddNewPropietary').modal('hide');
                    $('#cbPropietario').combobox('reload');
                    $('#alertInsertOkModalAdd').removeClass('d-none');
                    $('#labelInsertOkModalAdd').html(arrayResult[1]);
                    //LE ASIGNAMOS AL COMBOBOX AL ID DEL USUARIO RECIEN CREADO
                    var idPropietarioRecienCreado = arrayResult[2];
                    $('#cbPropietario').combobox('select', idPropietarioRecienCreado);
                    setTimeout(function () {
                        $('#alertInsertOkModalAdd').addClass('d-none');
                        $('#formAddNew').find('.textbox-f').textbox('resize');
                    }, 3000);
                    break;
                default:
                    break;
            }
        }
    });
}

function saveAddNew() {
    $('#formAddNew').form('submit', {
        url: 'addNew.php',
        onSubmit: function (param) {
            return $(this).form('enableValidation').form('validate');
        },
        success: function (result) {
            var arrayResult = result.split(",");
            switch (arrayResult[0]) {
                case "ERROR":
                    var res = arrayResult[1].match(/^.+\./);
                    console.error("Error: " + res);
                    $('#formAddNew').addClass('d-none');
                    $('#alertErrorModalAdd').removeClass('d-none');
                    $('#labelErrorModalAdd').html(res);
                    $('#btnSaveFormAddNew').addClass('d-none');
                    setTimeout(function () {
                        $('#alertErrorModalAdd').addClass('d-none');
                        $('#formAddNew').removeClass('d-none');
                        $('#formAddNew').find('.textbox-f').textbox('resize');
                        $('#btnSaveFormAddNew').removeClass('d-none');
                    }, 3000);
                    break;
                case "OK":
                    var res = arrayResult[1].match(/^.+\./);
                    console.log("Ok: " + res);
                    $('#formAddNew').addClass('d-none');
                    $('#formAddNew').form('clear');
                    $('#alertInsertOkModalAdd').removeClass('d-none');
                    $('#labelInsertOkModalAdd').html(res);
                    //HIDE SAVE BUTTON WHILE SHOWING THE ALERT
                    $('#btnSaveFormAddNew').addClass('d-none');
                    setTimeout(function () {
                        $('#alertInsertOkModalAdd').addClass('d-none');
                        $('#modalAdd').modal('hide');
                        $('#formAddNew').removeClass('d-none');
                        $('#formAddNew').find('.textbox-f').textbox('resize');
                        $('#dg').datagrid('reload');
                        //REACTIVATE SAVE MODAL BUTTON
                        $('#btnSaveFormAddNew').removeClass('d-none');
                    }, 1000);
                    ///RESETEAR EL FORMULARIO
                    arraySelectedPropietariesForTagBox = [];
                    arraySelectedVegetationForTagBox = [];
                    arraySelectedTreesForTagBox = [];
                    arraySelectedMammalsForTagBox = [];
                    arraySelectedBirdsForTagBox = [];
                    arraySelectedWaterBodiesForTagBox = [];

                    $('#cbPropietario').combobox('options').queryParams.cargadosDatagrid = '';
                    $('#cbVegetacion').combobox('options').queryParams.cargadosDatagrid = '';
                    $('#cbArboles').combobox('options').queryParams.cargadosDatagrid = '';
                    $('#cbMamiferos').combobox('options').queryParams.cargadosDatagrid = '';
                    $('#cbAves').combobox('options').queryParams.cargadosDatagrid = '';
                    $('#cbCuerposAgua').combobox('options').queryParams.cargadosDatagrid = '';

                    $('#cbPropietario').combobox('reload');
                    $('#cbVegetacion').combobox('reload');
                    $('#cbArboles').combobox('reload');
                    $('#cbMamiferos').combobox('reload');
                    $('#cbAves').combobox('reload');
                    $('#cbCuerposAgua').combobox('reload');

                    $('#formAddNew').form('clear');
                    break;
                default:
                    break;
            }
            //////////////////// END SWITCH
        }
    });
}

function updateAPC() {
    $('#formEdit').form('submit', {
        url: 'updateAPC.php',
        onSubmit: function (param) {
            return $(this).form('enableValidation').form('validate');
        },
        success: function (result) {
            var arrayResult = result.split(",");
            //console.log(arrayResult);
            switch (arrayResult[0]) {
                case "ERROR":
                    var res = arrayResult[1].match(/^.+\./);
                    console.error("Error: " + res);
                    $('#formEdit').addClass('d-none');
                    $('#alertErrorModalEdit').removeClass('d-none');
                    $('#labelErrorModalEdit').html(res);
                    $('#btnSaveFormEdit').addClass('d-none');
                    setTimeout(function () {
                        $('#alertErrorModalEdit').addClass('d-none');
                        $('#formEdit').removeClass('d-none');
                        $('#formEdit').find('.textbox-f').textbox('resize');
                        $('#btnSaveFormEdit').removeClass('d-none');
                    }, 3000);
                    break;
                case "OK":
                    var res = arrayResult[1].match(/^.+\./);
                    console.log("Ok: " + res);
                    $('#formEdit').addClass('d-none');
                    $('#formEdit').form('clear');
                    $('#alertInsertOkModalEdit').removeClass('d-none');
                    $('#alertInsertOkModalEdit').html(res);
                    //HIDE SAVE BUTTON WHILE SHOWING THE ALERT
                    $('#btnSaveFormEdit').addClass('d-none');
                    setTimeout(function () {
                        $('#alertInsertOkModalEdit').addClass('d-none');
                        $('#modalEdit').modal('hide');
                        $('#formEdit').removeClass('d-none');
                        $('#formEdit').find('.textbox-f').textbox('resize');
                        $('#dg').datagrid('reload');
                        //REACTIVATE SAVE MODAL BUTTON
                        $('#btnSaveFormEdit').removeClass('d-none');
                    }, 1000);
                    ///RESETEAR EL FORMULARIO
                    $('#formEdit').form('clear');
                    break;
                default:
                    break;
            }
            //////////////////// END SWITCH
        }
    });
}

function statusFieldsModalEdit(action) {
    //TODO
    //console.log('statusFieldsModalEdit: ' + action);
    switch (action) {
        case 'show':
            $('#cbEstatusModalEdit').combobox('clear').combobox('reset');
            $('#cbEstatusModalEdit').combobox('enable');
            $('#statusFields').show();
            break;
        case 'hide':
            $('#cbEstatusModalEdit').combobox('clear').combobox('reset');
            $('#cbEstatusModalEdit').combobox('disable');
            $('#statusFields').hide();
            break;
    }
    $('#cbEstatusModalEdit').textbox('resize');
    $('#fbShapefileModalEdit').textbox('resize');
}

function shapefileFieldsModalEdit(action) {
    //TODO
    //console.log('shapefileFieldsModalEdit: ' + action);
    $('#fbShapefileModalEdit').filebox('clear').filebox('reset');

    switch (action) {
        case 'show':
            //$('#fbShapefileModalEdit').filebox('clear').filebox('reset');
            $('#fbShapefileModalEdit').filebox('enable');
            $('#shapefileFields').show();
            break;
        case 'hide':
            //$('#fbShapefileModalEdit').filebox('clear').filebox('reset');
            $('#fbShapefileModalEdit').filebox('disable');
            $('#shapefileFields').hide();
            break;
    }
    $('#cbEstatusModalEdit').textbox('resize');
    $('#fbShapefileModalEdit').textbox('resize');
}

function noProcedeWrapperModalEdit(action) {
    switch (action) {
        case 'show':
            $('#tbMotivoNoProcedeRegresadoModalEdit').textbox('clear').textbox('reset');
            $('#tbMotivoNoProcedeRegresadoModalEdit').textbox('enable');
            $('#noProcedeWrapper').show();
            break;
        case 'hide':
            $('#tbMotivoNoProcedeRegresadoModalEdit').textbox('clear').textbox('reset');
            $('#tbMotivoNoProcedeRegresadoModalEdit').textbox('disable');
            $('#noProcedeWrapper').hide();
            break;
    }
    //$('#cbEstatusModalEdit').textbox('resize');
    //$('#fbShapefileModalEdit').textbox('resize');
    $('#tbMotivoNoProcedeRegresadoModalEdit').textbox('resize');
}

function certificadaWrapperModalEdit(action) {
    switch (action) {
        case 'show':
            $('#dbFechaCertificacionModalEdit').datebox('clear').textbox('reset');
            $('#dbFechaCertificacionModalEdit').datebox('enable');
            $('#tbNumeroSEDEMAModalEdit').textbox('clear').textbox('reset');
            $('#tbNumeroSEDEMAModalEdit').textbox('enable');
            $('#fbCertificadoModalEdit').filebox('clear').filebox('reset');
            $('#fbCertificadoModalEdit').filebox('enable');
            $('#certificadaWrapper').show();
            break;
        case 'hide':
            $('#dbFechaCertificacionModalEdit').datebox('clear').textbox('reset');
            $('#dbFechaCertificacionModalEdit').datebox('disable');
            $('#tbNumeroSEDEMAModalEdit').textbox('clear').textbox('reset');
            $('#tbNumeroSEDEMAModalEdit').textbox('disable');
            $('#fbCertificadoModalEdit').filebox('clear').filebox('reset');
            $('#fbCertificadoModalEdit').filebox('disable');
            $('#certificadaWrapper').hide();
            break;
    }
    //$('#cbEstatusModalEdit').textbox('resize');
    //$('#fbShapefileModalEdit').textbox('resize');
    //$('#tbMotivoNoProcedeRegresadoModalEdit').textbox('resize');
    $('#dbFechaCertificacionModalEdit').textbox('resize');
    $('#tbNumeroSEDEMAModalEdit').textbox('resize');
    $('#fbCertificadoModalEdit').textbox('resize');
}



//#FORMATTERS
function formatHectareas(value, row, index) {
    var redondeada = parseFloat(value).toFixed(2);
    return '<span class=\'ttpHectareas\' data-options=\'param:' + row.hectareas_certificadas + '\'>' + redondeada + ' ha.</span>';
}

function formatDate(value, row, index) {
    if (value != null) {
        var fecha = value.split("-");
        var anyo = fecha[0];
        var mes = fecha[1];
        var dia = fecha[2];
        return '<span>' + dia + '/' + mes + '/' + anyo + '</span>';
    } else {
        return "<span style='font-style: italic;'>Pendiente</span>";
    }
}

function formatShapefileIcon(value, row, index) {
    if (value == 'Shapefile guardado')
        return '<span class="ttpWithShapefile" data-options="param:\'' + value + '\'"><i class="fas fa-globe-americas with-shapefile"></i></span>';
    else if (value == 'Sin shapefile')
        return '<span class="ttpWithoutShapefile" data-options="param:\'' + value + '\'"><i class="fas fa-globe-americas without-shapefile"></i></span>';
}

function formatStatus(value, row, index) {
    if (value == 'Certificada') {
        return '<span class="ttpStatusCertified" data-options="param:\'' + value + '\'"><i class="fas fa-check-circle status-certified"></i></span>';
    } else if (value == 'Decretada') {
        return '<span class="ttpStatusDecreed" data-options="param:\'' + value + '\'"><i class="fas fa-shield-alt status-decreed"></i></span>';
    } else if (value == 'En proceso') {
        return '<span class="ttpStatusInProgress" data-options="param:\'' + value + '\'"><i class="fas fa-clock status-in-progress"></i></span>';
    } else if (value == 'No procede') {
        return '<span class="ttpStatusNotProceed" data-options="param:\'' + value + '\'"><i class="fas fa-times-circle status-not-proceed"></i></span>';
    } else if (value == 'Regresado a SEDEMA por error') {
        return '<span class="ttpStatusReturned" data-options="param:\'' + value + '\'"><i class="fas fa-arrow-circle-left status-returned"></i></span>';
    }
}

function formatNSedema(value, row, index) {
    if (value == null) return "<span style='font-style: italic;'>Pendiente</span>";
    else return "<span>" + value + "</span>";
}

function formatMotivo(value, row, index) {
    if (value == null) return "<span style='font-style: italic;'>No disponible</span>";
    else return "<span>" + value + "</span>";
}

function formatVegetacion(value, row, index) {
    var vegetacion = row.vegetacion;
    vegetacion = replaceAll(vegetacion, ",", "<br>");
    return '<span class="ttpVegetacion" data-options="param:\'' + vegetacion + '\'"><i class="fab fa-pagelines SeaGreen"></i></span>';
}

function formatOVegetacion(value, row, index) {
    var OVegetacion = row.observaciones_vegetacion;
    if (OVegetacion === '')
        OVegetacion = 'Ninguna';
    return '<span class="ttpOVegetacion" data-options="param:\'' + OVegetacion + '\'"><i class="fas fa-info-circle DodgerBlue"></i></span>';
}

function formatEspecies(value, row, index) {
    var especies = row.especies;
    var arboles = "<b>Arboles</b><br>-----------------------------------<br>";
    var mamiferos = "<b>Mamiferos</b><br>-----------------------------------<br>";
    var aves = "<b>Aves</b><br>-----------------------------------<br>";
    var anfibios = "<b>Anfibios</b><br>-----------------------------------<br>";
    var epifitas = "<b>Epifitas</b><br>-----------------------------------<br>";
    var cuerposAgua = "<b>Cuerpos de agua</b><br>-----------------------------------<br>";
    var contadorArboles = 0;
    var contadorMamiferos = 0;
    var contadorAves = 0;
    var contadorAnfibios = 0;
    var contadorEpifitas = 0;
    var contadorCuerposAgua = 0;

    if (especies == '') {
        return '<span class="ttpSinEspecies"><i class="fas fa-exclamation-circle IndianRed"></span>';
    } else {
        especies = especies.split(',');
        for (var i = 0; i < especies.length; i++) {
            var especie_actual = especies[i];
            especie_actual = especie_actual.split(':');
            var categoria = especie_actual[0];
            var especie = especie_actual[1];
            categoria = categoria.trim();
            especie = especie.trim();
            if (categoria == 'Arboles') {
                if (contadorArboles === 0) {
                    arboles += (+contadorArboles + 1) + '. ' + especie;
                    contadorArboles++;
                } else {
                    arboles += '<br>' + (+contadorArboles + 1) + '. ' + especie;
                    contadorArboles++;
                }
            } else if (categoria == 'Mamiferos') {
                if (contadorMamiferos === 0) {
                    mamiferos += (+contadorMamiferos + 1) + '. ' + especie;
                    contadorMamiferos++;
                } else {
                    mamiferos += '<br>' + (+contadorMamiferos + 1) + '. ' + especie;
                    contadorMamiferos++;
                }
            } else if (categoria == 'Aves') {
                if (contadorAves === 0) {
                    aves += (+contadorAves + 1) + '. ' + especie;
                    contadorAves++;
                } else {
                    aves += '<br>' + (+contadorAves + 1) + '. ' + especie;
                    contadorAves++;
                }
            } else if (categoria == 'Anfibios') {
                if (contadorAnfibios === 0) {
                    anfibios += (+contadorAnfibios + 1) + '. ' + especie;
                    contadorAnfibios++;
                } else {
                    anfibios += '<br>' + (+contadorAnfibios + 1) + '. ' + especie;
                    contadorAnfibios++;
                }
            } else if (categoria == 'Epifitas') {
                if (contadorEpifitas === 0) {
                    epifitas += (+contadorEpifitas + 1) + '. ' + especie;
                    contadorEpifitas++;
                } else {
                    epifitas += '<br>' + (+contadorEpifitas + 1) + '. ' + especie;
                    contadorEpifitas++;
                }
            } else if (categoria == 'Cuerpos de agua') {
                if (contadorCuerposAgua === 0) {
                    cuerposAgua += (+contadorCuerposAgua + 1) + '. ' + especie;
                    contadorCuerposAgua++;
                } else {
                    cuerposAgua += '<br>' + (+contadorCuerposAgua + 1) + '. ' + especie;
                    contadorCuerposAgua++;
                }
            }
        }

        if (contadorArboles === 0)
            arboles = 'No se registraron arboles';
        if (contadorMamiferos === 0)
            mamiferos = 'No se registraron mamiferos'
        if (contadorAves === 0)
            aves = 'No se registraron aves'
        if (contadorAnfibios === 0)
            anfibios = 'No se registraron anfibios'
        if (contadorEpifitas === 0)
            epifitas = 'No se registraron epifitas'
        if (contadorCuerposAgua === 0)
            cuerposAgua = 'No se registraron cuerpos de agua'

        return '<span style="padding-right:10px;" class="ttpArboles" data-options="param:\'' + arboles + '\'"><i class="fas fa-tree ForestGreen"></i></span><span style="padding-right:10px;" class="ttpMamiferos" data-options="param:\'' + mamiferos + '\'"><i class="fas fa-paw Brown"></i></span><span style="padding-right:10px;" class="ttpAves" data-options="param:\'' + aves + '\'"><i class="fas fa-crow Chocolate"></i></span><span style="padding-right:10px;" class="ttpAnfibios" data-options="param:\'' + anfibios + '\'"><i class="fas fa-frog OliveDrab"></i></span><span style="padding-right:10px;" class="ttpEpifitas" data-options="param:\'' + epifitas + '\'"><i class="fas fa-spa MediumSlateBlue"></i></span><span style="padding-right:10px;" class="ttpCuerposAgua" data-options="param:\'' + cuerposAgua + '\'"><i class="fas fa-tint SkyBlue"></i></span>';
    }

}

function formatObservaciones(value, row, index) {

    var observaciones = row.observaciones;
    if (observaciones === '' || observaciones === null)
        observaciones = 'Ninguna';
    else {
        //observaciones = observaciones.replace(/[\n\r"']/g, "");
        observaciones = observaciones.replace(new RegExp("[\n\r\"\']", 'g'), "");
    }
    return '<span class="ttpObservaciones" data-options="param:\'' + observaciones + '\'"><i class="fas fa-info-circle DodgerBlue"></i></span>';
}

function formatSello(value, row, index) {
    if (value == 'Si')
        return "<img src='images/logo_sello_biodiversidad.png'>";
    else
        return "<span>" + value + "</span>";
}



//#JEASYUI OVERRIDES
//ONLY NUMBERS IN TEXTBOX
$.extend($.fn.validatebox.defaults.rules, {
    onlyNumbers: {
        validator: function (value, param) {
            if (/^\d+$/.test(value))
                return value;
        },
        message: 'Solo se aceptan numeros'
    }
});

//FUNCION PARA FORMATEAR LOS CAMPOS DATEBOX (GUIONES, DIAGONALES, ETC.)
$.fn.datebox.defaults.formatter = function (date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
};
$.fn.datebox.defaults.parser = function (s) {
    if (s) {
        var a = s.split('-');
        var d = new Number(a[2]);
        var m = new Number(a[1]);
        var y = new Number(a[0]);
        var dd = new Date(y, m - 1, d);
        return dd;
    } else {
        return new Date();
    }
};

//FUNCION PARA MOSTRAR U OCULTAR LOS TEXBOX
$.extend($.fn.textbox.methods, {
    show: function (jq) {
        return jq.each(function () {
            $(this).next().show();
        })
    },
    hide: function (jq) {
        return jq.each(function () {
            $(this).next().hide();
        })
    }
});

//FUNCIÓN PARA BORRAR ELEMENTOS ESPECIFICOS DEL COMBOBOX
$.extend($.fn.combobox.methods, {
    appendItem: function (jq, item) {
        return jq.each(function () {
            var state = $.data(this, 'combobox');
            var opts = state.options;
            var items = $(this).combobox('getData');
            items.push(item);
            $(this).combobox('panel').append('<div id="' + state.itemIdPrefix + '_' + (items.length - 1) + '"  class="combobox-item">' + (opts.formatter ? opts.formatter.call(this, item) : item[opts.textField]) + '</div>')
        })
    },
    deleteItem: function (jq, index) {
        return jq.each(function () {
            var state = $.data(this, 'combobox');
            $(this).combobox('getData').splice(index, 1);
            var panel = $(this).combobox('panel');
            panel.children('.combobox-item:gt(' + index + ')').each(function () {
                var id = $(this).attr('id');
                var i = id.substr(state.itemIdPrefix.length + 1);
                $(this).attr('id', state.itemIdPrefix + '_' + (parseInt(i) - 1));
            });
            panel.children('.combobox-item:eq(' + index + ')').remove();
        })
    },
    makeLetterSelectable: function (jq) {
        return jq.each(function () {
            var cc = $(this);
            cc.combobox('textbox').bind('keydown', function (e) {
                var s = String.fromCharCode(e.keyCode);
                var opts = cc.combobox('options');
                var data = cc.combobox('getData');
                var state = $.data(cc[0], 'combobox');
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    if (item[opts.textField].toLowerCase().substring(0, 1) == s.toLowerCase()) {
                        if (typeof state.index == 'undefined' || (typeof state.index != 'undefined' && i > state.index)) {
                            cc.combobox('select', item[opts.valueField]);
                            state.index = i;
                            break;
                        }
                    }
                    if (i == data.length - 1 && typeof state.index != 'undefined') {
                        state.index = undefined;
                        arguments.callee(e);
                    }
                }
            });
        });
    }
});



//#JQUERY
$(function () {

    //#### COMPORTAMIENTO GENERAL ###########################################################
    $('#modalAdd').on('shown.bs.modal', function () {
        $('#formAddNew').form('disableValidation').form('validate');
        $(this).find('.textbox-f').each(function () {
            $(this).next().css({
                width: 1,
                height: 1
            });
        }).textbox('resize');
    });

    $('#modalAddNewPropietary').on('shown.bs.modal', function () {
        $('#formAddNewPropietary')
            .form('clear')
            .form('disableValidation').form('validate');
        $(this).find('.textbox-f').each(function () {
            $(this).next().css({
                width: 1,
                height: 1
            });
        }).textbox('resize');

        setTimeout(function () {
            $('#tbNombrePropietario').textbox('textbox').focus();
        }, 0);
    });

    $('#modalAddNewPropietary').on('hidden.bs.modal', function () {
        $('#modalAdd').modal('show');
    });

    $('#modalEdit').on('shown.bs.modal', function () {
        $('#formEdit').form('disableValidation').form('validate');
        $(this).find('.textbox-f').each(function () {
            $(this).next().css({
                width: 1,
                height: 1
            });
        }).textbox('resize');
        //ESCONDER LOS INPUTS AUXILIARES AL ABRIR EL MODAL
        $('#tbIdAPC').textbox('hide');
        $('#tbNombreAPCTempModalEdit').textbox('hide');
        $('#tbFolioTempModalEdit').textbox('hide');
        $('#tbEstatusTempModalEdit').textbox('hide');
        $('#tbGeomPointTempModalEdit').textbox('hide');
        $('#tbGeomPolyTempModalEdit').textbox('hide');
        $('#tbGeomResultTempModalEdit').textbox('hide');
        //ESCONDER LOS WRAPPERS DE LOS INPUTS DEL MODAL EDIT
        $('#statusFields').hide();
        $('#shapefileFields').hide();
        $('#noProcedeWrapper').hide();
        $('#certificadaWrapper').hide();




        $('#cbOpcionModalEdit').combobox('clear');
        $('#cbEstatusModalEdit').combobox('clear');
        //$('#formEdit').form('clear');
        //tbEstatusTempModalEdit -> RESET
    });

    $('#modalAdd').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    $('#modalAddNewPropietary').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    $('#modalEdit').modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    //INICIALIZAMOS LAS VARIABLE PARA LOS FILTROS CON OPCIONES
    var hectareasOptionSelected = "";
    var fechaIngresoOptionSelected = "";
    var fechaCertificacionOptionSelected = "";

    //MARCAR LA PAGINA ACTIVA EN EL NAVBAR
    //$('#apc').addClass('active');

    //INICIALIZAR LOS TOOLTIP
    $('[data-toggle="tooltip"]').tooltip();

    //INICIALIZAR EL COMPLEMENTO "WOW" PARA ANIMACIONES
    wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: true,
        live: true
    });
    wow.init();
    //#####################################################################################



    //### COMPONENTES EASYUI ###########################################################
    //OBTENER EL HEIGHT TOTAL DEL VIEWPORT SIN EL ALTO DEL NAVBAR           
    var myNavBarHeight = $('#myNavBar').height();
    var oh = window.innerHeight - myNavBarHeight - 150;

    //REDIMENSIONAR EL DATAGRID CUANDO LA VENTANA CAMBIE DE TAMAÑO
    $(window).resize(function () {
        $('#dg').datagrid('resize');
    });

    $('#dg').datagrid({
        width: '100%',
        height: oh,
        nowrap: true,
        view: scrollview,
        remoteFilter: true,
        remoteSort: true,
        url: 'getAPC.php',
        idField: 'id_principal',
        rownumbers: true,
        singleSelect: true,
        filterDelay: 0,
        autoRowHeight: false,
        striped: true,
        pageSize: 100,
        toolbar: '#toolbar',
        onLoadSuccess: function (data) {
            console.log('onLoadSuccess');
            updateRowNumbers();
            loadTooltips();
        },
        onClickRow: function (index, row) {
            $('#btnEdit').linkbutton('enable');
        }
    }).datagrid('enableFilter', [{
            field: 'figura_legal',
            type: 'combobox',
            options: {
                editable: false,
                multiple: true,
                panelWidth: 110,
                panelHeight: 'auto',
                panelMaxHeight: 200,
                url: 'getFiguraLegalFilter.php',
                valueField: 'figura_legal',
                textField: 'figura_legal',
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );
                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'figura_legal').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        var valueConcat = "";
                        for (var i = 0; i < value.length; i++) {
                            if (i === 0)
                                valueConcat += value[i];
                            else
                                valueConcat += "," + value[i];
                        }
                        //console.log(valueConcat);
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'figura_legal',
                            op: 'contains',
                            value: valueConcat
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'geomcoordscclpoly',
            type: 'label'
        },
        {
            field: 'folio',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'folio').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'folio',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'nombre_apc',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );

                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'nombre_apc').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'nombre_apc',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'nombre_completo',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'nombre_completo').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'nombre_completo',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'hectareas',
            type: 'numberbox',
            options: {
                precision: 2,
                icons: [{
                        iconCls: 'fas fa-backspace backspaceIcon',
                        handler: function (e) {
                            $(e.data.target).numberbox('textbox').css({
                                backgroundColor: '#FFFFFF',
                                color: 'DimGrey'
                            });
                            $('#dg').datagrid('removeFilterRule', 'hectareas').datagrid('doFilter');
                        }
                    },
                    {
                        iconCls: 'fas fa-filter filterIcon',
                        handler: function (e) {
                            $('#menuFilterOptionsHectareas')
                                .menu('show', {
                                    left: e.pageX - 15,
                                    top: e.pageY + 14
                                })
                                .menu({
                                    onClick: function (item) {
                                        var hectareasValue = $(e.data.target).numberbox('getValue');
                                        switch (item.text) {
                                            case 'Igual':
                                                hectareasOptionSelected = 'equal';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_h')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_h')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_h')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                break;
                                            case 'Mayor que':
                                                hectareasOptionSelected = 'greater';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_h')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_h')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_h')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                break;
                                            case 'Menor que':
                                                hectareasOptionSelected = 'less';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_h')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_h')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_h')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                break;
                                        }
                                        $(e.data.target).numberbox('setValue', 'changed');
                                        $(e.data.target).numberbox('setValue', hectareasValue);
                                    }
                                });
                        }
                    }
                ],
                onChange: function (value) {
                    if (value != '') {
                        if (hectareasOptionSelected != '' && value != 'changed') {
                            $(this).numberbox('textbox').css({
                                backgroundColor: '#BAD7C9',
                                color: '#515D57'
                            });
                            $('#dg').datagrid('addFilterRule', {
                                field: 'hectareas',
                                op: hectareasOptionSelected,
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }


            }
        },
        {
            field: 'estado',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'estado').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'estado',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'municipio',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'municipio').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'municipio',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'localidad',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'localidad').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'localidad',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'fecha_ingreso',
            type: 'datebox',
            options: {
                editable: false,
                icons: [{
                        iconCls: 'fas fa-backspace backspaceIcon',
                        handler: function (e) {
                            $(e.data.target).datebox('textbox').css({
                                backgroundColor: '#FFFFFF',
                                color: 'DimGrey'
                            });
                            $('#dg').datagrid('removeFilterRule', 'fecha_ingreso').datagrid('doFilter');
                        }
                    },
                    {
                        iconCls: 'fas fa-filter filterIcon',
                        handler: function (e) {
                            $('#menuFilterOptionsFechaIngreso')
                                .menu('show', {
                                    left: e.pageX - 15,
                                    top: e.pageY + 14
                                })
                                .menu({
                                    onClick: function (item) {
                                        var fechaIngresoValue = $(e.data.target).datebox('getValue');
                                        switch (item.text) {
                                            case 'Igual':
                                                fechaIngresoOptionSelected = 'equal';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_fi')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_fi')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_fi')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                break;
                                            case 'Mayor que':
                                                fechaIngresoOptionSelected = 'greater';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_fi')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_fi')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_fi')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                break;
                                            case 'Menor que':
                                                fechaIngresoOptionSelected = 'less';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_fi')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_fi')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_fi')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                break;
                                        }
                                        $(e.data.target).datebox('setValue', '2000-01-01');
                                        $(e.data.target).datebox('setValue', fechaIngresoValue);
                                    }
                                });
                        }
                    }
                ],
                onChange: function (value) {
                    if (value != '') {
                        if (fechaIngresoOptionSelected != '' && value != '2000-01-01') {
                            $(this).numberbox('textbox').css({
                                backgroundColor: '#BAD7C9',
                                color: '#515D57'
                            });
                            $('#dg').datagrid('addFilterRule', {
                                field: 'fecha_ingreso',
                                op: fechaIngresoOptionSelected,
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            }
        },
        {
            field: 'estatus',
            type: 'combobox',
            options: {
                editable: false,
                panelWidth: 250,
                panelHeight: 'auto',
                url: 'getEstatusFilter.php',
                valueField: 'descripcion_estatus',
                textField: 'descripcion_estatus',
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );

                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'estatus').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'estatus',
                            op: 'equal',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'fecha_certificacion',
            type: 'datebox',
            options: {
                editable: false,
                icons: [{
                        iconCls: 'fas fa-backspace backspaceIcon',
                        handler: function (e) {
                            $(e.data.target).datebox('textbox').css({
                                backgroundColor: '#FFFFFF',
                                color: 'DimGrey'
                            });
                            $('#dg').datagrid('removeFilterRule', 'fecha_certificacion').datagrid('doFilter');
                        }
                    },
                    {
                        iconCls: 'fas fa-filter filterIcon',
                        handler: function (e) {
                            $('#menuFilterOptionsFechaCertificacion')
                                .menu('show', {
                                    left: e.pageX - 15,
                                    top: e.pageY + 14
                                })
                                .menu({
                                    onClick: function (item) {
                                        var fechaCertificacionValue = $(e.data.target).datebox('getValue');
                                        switch (item.text) {
                                            case 'Igual':
                                                fechaCertificacionOptionSelected = 'equal';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_fc')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_fc')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_fc')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                break;
                                            case 'Mayor que':
                                                fechaCertificacionOptionSelected = 'greater';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_fc')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_fc')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_fc')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                break;
                                            case 'Menor que':
                                                fechaCertificacionOptionSelected = 'less';
                                                $(this).menu('setIcon', {
                                                    target: $('#m-equal_fc')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-greater_fc')[0],
                                                    iconCls: 'far fa-square'
                                                });
                                                $(this).menu('setIcon', {
                                                    target: $('#m-less_fc')[0],
                                                    iconCls: 'far fa-check-square'
                                                });
                                                break;
                                        }
                                        $(e.data.target).datebox('setValue', '2000-01-01');
                                        $(e.data.target).datebox('setValue', fechaCertificacionValue);
                                    }
                                });
                        }
                    }
                ],
                onChange: function (value) {
                    if (value != '') {
                        if (fechaCertificacionOptionSelected != '' && value != '2000-01-01') {
                            $(this).numberbox('textbox').css({
                                backgroundColor: '#BAD7C9',
                                color: '#515D57'
                            });
                            $('#dg').datagrid('addFilterRule', {
                                field: 'fecha_certificacion',
                                op: fechaCertificacionOptionSelected,
                                value: value
                            }).datagrid('doFilter');
                        }
                    }
                }
            }
        },
        {
            field: 'numero_sedema',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'numero_sedema').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'numero_sedema',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'motivo_no_procede',
            type: 'label'
        },
        {
            field: 'vegetacion',
            type: 'combobox',
            options: {
                editable: false,
                multiple: true,
                panelWidth: 200,
                panelHeight: 'auto',
                url: 'getVegetacionFilter.php',
                valueField: 'tipo_vegetacion',
                textField: 'tipo_vegetacion',
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );

                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'vegetacion').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        //console.log(value);
                        var valueConcat = "";
                        for (var i = 0; i < value.length; i++) {
                            if (i === 0)
                                valueConcat += value[i];
                            else
                                valueConcat += "," + value[i];
                        }
                        //console.log(valueConcat);
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'vegetacion',
                            op: 'contains',
                            value: valueConcat
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        /*{
            field: 'observaciones_vegetacion',
            type: 'label'
        },*/
        {
            field: 'especies',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'especies').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'especies',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'tecnico',
            type: 'combobox',
            options: {
                editable: false,
                panelWidth: 200,
                panelHeight: 'auto',
                url: 'getTecnicoFilter.php',
                valueField: 'nombre_completo',
                textField: 'nombre_completo',
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );

                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'tecnico').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'tecnico',
                            op: 'equal',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'descripcion_zona',
            type: 'combobox',
            options: {
                editable: false,
                panelWidth: 'auto',
                panelHeight: 'auto',
                panelMinWidth: 400,
                panelMaxWidth: 400,
                multiple: true,
                url: 'getZonasFilter.php',
                valueField: 'descripcion_zona',
                textField: 'descripcion_zona',
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );

                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'descripcion_zona').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        //console.log(value);
                        var valueConcat = "";
                        for (var i = 0; i < value.length; i++) {
                            if (i === 0)
                                valueConcat += value[i];
                            else
                                valueConcat += "," + value[i];
                        }
                        //console.log(valueConcat);
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'descripcion_zona',
                            op: 'contains',
                            value: valueConcat
                        }).datagrid('doFilter');
                    }

                    //HACEMOS DEPENDIENTE EL COMBOBOX DE PROYECTO
                    $("input[name='nombre_proyecto']").each(function () {
                        $(this).combobox('reload', 'getProyectosFilter.php?zonas=' + value);
                    });
                }
            }
        },
        {
            field: 'nombre_proyecto',
            type: 'combobox',
            options: {
                editable: false,
                multiple: true,
                panelWidth: 'auto',
                panelHeight: 'auto',
                panelMinWidth: 500,
                panelMaxWidth: 500,
                url: 'getProyectosFilter.php',
                valueField: 'nombre_proyecto',
                textField: 'nombre_proyecto',
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );

                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'nombre_proyecto').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        //console.log(value);
                        var valueConcat = "";
                        for (var i = 0; i < value.length; i++) {
                            if (i === 0)
                                valueConcat += value[i];
                            else
                                valueConcat += "," + value[i];
                        }
                        //console.log(valueConcat);
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'nombre_proyecto',
                            op: 'contains',
                            value: valueConcat
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'fase',
            type: 'textbox',
            options: {
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).textbox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'fase').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).textbox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'fase',
                            op: 'contains',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'proteccion_legal',
            type: 'label'
        },
        {
            field: 'cuenta_plan_manejo',
            type: 'combobox',
            options: {
                editable: false,
                panelWidth: 'auto',
                panelHeight: 'auto',
                panelMinWidth: 200,
                valueField: 'text',
                textField: 'text',
                data: [{
                    'text': 'Si'
                }, {
                    'text': 'No'
                }],
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'cuenta_plan_manejo').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'cuenta_plan_manejo',
                            op: 'equal',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'ruta_plan_de_manejo',
            type: 'label'
        },
        {
            field: 'tenencia',
            type: 'combobox',
            options: {
                editable: false,
                panelWidth: 'auto',
                panelHeight: 'auto',
                panelMinWidth: 100,
                valueField: 'text',
                textField: 'text',
                data: [{
                    'text': 'Ejido'
                }, {
                    'text': 'Privado'
                }],
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'tenencia').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'tenencia',
                            op: 'equal',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'organizacion',
            type: 'combobox',
            options: {
                editable: false,
                panelWidth: 'auto',
                panelHeight: 'auto',
                panelMinWidth: 130,
                valueField: 'text',
                textField: 'text',
                data: [{
                    'text': 'Si'
                }, {
                    'text': 'No'
                }],
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'organizacion').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'organizacion',
                            op: 'notequal',
                            value: value
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'org_marca',
            type: 'combobox',
            options: {
                editable: false,
                multiple: true,
                panelWidth: 250,
                panelHeight: 'auto',
                panelMaxHeight: 200,
                url: 'getOrgMarcaFilter.php',
                valueField: 'org_marca',
                textField: 'org_marca',
                icons: [{
                    iconCls: 'fas fa-backspace backspaceIcon',
                    handler: function (e) {
                        //console.log(  $(e.data.target)  );
                        $(e.data.target).combobox('textbox').css({
                            backgroundColor: '#FFFFFF',
                            color: 'DimGrey'
                        });
                        $('#dg').datagrid('removeFilterRule', 'org_marca').datagrid('doFilter');
                    }
                }],
                onChange: function (value) {
                    if (value != '') {
                        //console.log(value);
                        var valueConcat = "";
                        for (var i = 0; i < value.length; i++) {
                            if (i === 0)
                                valueConcat += value[i];
                            else
                                valueConcat += "," + value[i];
                        }
                        //console.log(valueConcat);
                        $(this).combobox('textbox').css({
                            backgroundColor: '#BAD7C9',
                            color: '#515D57'
                        });
                        $('#dg').datagrid('addFilterRule', {
                            field: 'org_marca',
                            op: 'contains',
                            value: valueConcat
                        }).datagrid('doFilter');
                    }
                }
            }
        },
        {
            field: 'ruta_doc_ingresados',
            type: 'label'
        },
        {
            field: 'ruta_fotografias',
            type: 'label'
        },
        {
            field: 'ruta_certificado',
            type: 'label'
        }
    ]);

    $('#btnAdd').linkbutton({
        plain: true,
        onClick: function () {
            //Behaviour
        }
    }).tooltip({
        content: '<span style="font-size:12px;">Agregar</span>',
        position: 'top',
        onShow: function () {
            $(this).tooltip('tip').css({
                backgroundColor: 'SeaGreen',
                fontFamily: 'Roboto-Regular',
                fontWeight: 'bold',
                opacity: 0.9,
                borderColor: 'SeaGreen',
                color: '#FFFFFF',
                margin: 'auto'
            });
        }
    });

    $('#btnEdit').linkbutton({
        plain: true,
        disabled: true,
        onClick: function () {
            $('#modalEdit').modal('show');
            var rowSelected = $('#dg').datagrid('getSelected');
            console.log(rowSelected);
            $('#tbIdAPC').textbox('setValue', rowSelected.id_principal);
            $('#tbNombreAPCTempModalEdit').textbox('setValue', rowSelected.nombre_apc);
            $('#selectedNombreAPCModalEdit').html('<span style="font-weight: bold;">' + rowSelected.nombre_apc + '</span>');
            $('#tbFolioTempModalEdit').textbox('setValue', rowSelected.folio);
            $('#selectedFolioModalEdit').text(rowSelected.folio);
            $('#tbEstatusTempModalEdit').textbox('setValue', rowSelected.estatus);

            var statusIcon = "";
            switch(rowSelected.estatus) {
                case 'Certificada':
                    statusIcon = '<i class="fas fa-check-circle status-certified"></i>';
                break;
                case 'Decretada':
                    statusIcon = '<i class="fas fa-shield-alt status-decreed"></i>';
                break;
                case 'En proceso':
                    statusIcon = '<i class="fas fa-clock status-in-progress"></i>';
                break;
                case 'No procede':
                    statusIcon = '<i class="fas fa-times-circle status-not-proceed"></i>';
                break;
                case 'Regresado a SEDEMA por error':
                    statusIcon = '<i class="fas fa-arrow-circle-left status-returned"></i>';
                break;
            }
            $('#selectedEstatusModalEdit').html(statusIcon + "&nbsp;" + rowSelected.estatus);





            $('#tbGeomPointTempModalEdit').textbox('setValue', rowSelected.geom_point);
            $('#tbGeomPolyTempModalEdit').textbox('setValue', rowSelected.geom_poly);
        }
    }).tooltip({
        content: '<span style="font-size:12px;">Editar</span>',
        position: 'top',
        onShow: function () {
            $(this).tooltip('tip').css({
                backgroundColor: 'DarkOrange ',
                fontFamily: 'Roboto-Regular',
                fontWeight: 'bold',
                opacity: 0.9,
                borderColor: 'DarkOrange ',
                color: '#FFFFFF',
                margin: 'auto'
            });
        }
    });

    $('#btnReload').linkbutton({
        plain: true,
        onClick: function () {
            //Return to original color all filters textbox.
            /*$('.searchInput').find("input").each(function (index, element) {
                element.style.backgroundColor = "white";
                element.style.color = "IndianRed";
            });*/

            $('.datagrid-filter-row .textbox, .datagrid-filter-row .textbox .textbox-text').css({
                backgroundColor: '#FFFFFF',
                color: 'DimGrey'
            });

            $('#dg').datagrid('removeFilterRule').datagrid('doFilter');
        }
    }).tooltip({
        content: '<span style="font-size:12px;">Recargar tabla</span>',
        position: 'top',
        onShow: function () {
            $(this).tooltip('tip').css({
                backgroundColor: 'DarkSlateBlue',
                fontFamily: 'Roboto-Regular',
                fontWeight: 'bold',
                opacity: 0.9,
                borderColor: 'DarkSlateBlue',
                color: '#FFFFFF',
                margin: 'auto'
            });
        }
    });

    $('#btnDownload').menubutton({
        plain: true,
        menu: '#menuDownload'
    }).tooltip({
        content: '<span style="font-size:12px;">Descargar</span>',
        position: 'top',
        onShow: function () {
            $(this).tooltip('tip').css({
                backgroundColor: 'SkyBlue',
                fontFamily: 'Roboto-Regular',
                fontWeight: 'bold',
                opacity: 0.9,
                borderColor: 'SkyBlue',
                color: '#FFFFFF',
                margin: 'auto'
            });
        }
    });

    $('#menuDownload').menu({
        minWidth: 260,
        onClick: function (item) {
            switch (item.name) {
                case 'exportToExcel':
                    exportToExcel();
                    break;
                case 'exportShapefileCCL':
                    exportToShapefile('ccl');
                    break;
                case 'exportShapefileUTM14':
                    exportToShapefile('utm14');
                    break;
                case 'exportShapefileUTM15':
                    exportToShapefile('utm15');
                    break;
                case 'exportDocumentacion':
                    exportFiles('documentacion');
                    break;
                case 'exportFotos':
                    exportFiles('fotos');
                    break;
                case 'exportCertificados':
                    exportFiles('certificados');
                    break;
                case 'exportDFC':
                    exportFiles('dfc');
                    break;
            }
        }
    });

    //FORMULARIO AGREGAR NUEVO REGISTRO
    $('#cbPropietario').combobox({
        required: false,
        hasDownArrow: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        prompt: 'Buscar propietario',
        label: '<span class="label-textbox">Propietario</span>',
        labelPosition: 'top',
        valueField: 'id',
        textField: 'name',
        url: 'getPropietariosByName.php',
        queryParams: {
            cargadosDatagrid: ''
        },
        limitToList: true,
        iconWidth: 22,
        icons: [{
            iconCls: 'user-plus-icon-seagreen',
            handler: function (e) {
                $('#modalAddNewPropietary').modal('show');
                $('#modalAdd').modal('hide');
            }
        }],
        onSelect: function (rec) {
            arraySelectedPropietariesForTagBox.push(rec.name);
            $('#tagPropietarios').combobox('setValues', arraySelectedPropietariesForTagBox);

            var stringSelectedPropietariesName = arraySelectedPropietariesForTagBox.join();
            var queryParams = $('#cbPropietario').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedPropietariesName;


            setTimeout(function () {
                $('#cbPropietario').combobox('clear').combobox('reset');
            }, 0);

            $('#cbPropietario').combobox('reload');
        }
    });

    $('#tagPropietarios').tagbox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        label: '<span class="label-textbox">Propietarios seleccionados</span>',
        labelPosition: 'top',
        limitToList: true,
        hasDownArrow: false,
        valueField: 'id',
        textField: 'id',
        onRemoveTag(value) {
            for (var i = 0; i < arraySelectedPropietariesForTagBox.length; i++) {
                if (arraySelectedPropietariesForTagBox[i] === value) {
                    arraySelectedPropietariesForTagBox.splice(i, 1);
                }
            }

            var stringSelectedPropietariesName = arraySelectedPropietariesForTagBox.join();
            var queryParams = $('#cbPropietario').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedPropietariesName;
            $('#cbPropietario').combobox('reload');
        }
    });

    $('#tbFolio').textbox({
        required: true,
        label: '<span class="label-textbox">Folio</span>',
        labelPosition: 'top',
    });

    $('#tbNombreAPC').textbox({
        required: true,
        label: '<span class="label-textbox">Nombre del APC</span>',
        labelPosition: 'top',
    });

    $('#nbHectareas').numberbox({
        required: true,
        label: '<span class="label-textbox">Hectareas</span>',
        labelPosition: 'top',
        precision: 2,
        min: 0
    });

    $('#nbHectareasCertificadas').numberbox({
        required: true,
        label: '<span class="label-textbox">Hectareas certificadas</span>',
        labelPosition: 'top',
        precision: 2,
        min: 0
    });

    $('#cbEstado').combobox({
        required: true,
        //editable: false,
        limitToList: true,
        hasDownArrow: false,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Estado</span>',
        labelPosition: 'top',
        valueField: 'nom_estado',
        textField: 'nom_estado',
        url: 'getEstados.php',
        onSelect: function (rec) {
            var url = 'getMunicipios.php?estado=' + rec.nom_estado;
            $('#cbMunicipio').combobox('reload', url);
        }
    });

    $('#cbMunicipio').combobox({
        required: true,
        //editable: false,
        limitToList: true,
        hasDownArrow: false,
        panelHeight: 'auto',
        panelMaxHeight: 150,
        label: '<span class="label-textbox">Municipio</span>',
        labelPosition: 'top',
        valueField: 'nom_municipio',
        textField: 'nom_municipio',
        url: 'getMunicipios.php',
        onSelect: function (rec) {
            var url = 'getLocalidades.php?municipio=' + rec.nom_municipio;
            $('#cbLocalidad').combobox('reload', url);
        }
    });

    $('#cbLocalidad').combobox({
        required: true,
        //editable: false,
        limitToList: true,
        hasDownArrow: false,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Localidad</span>',
        labelPosition: 'top',
        valueField: 'nom_localidad',
        textField: 'nom_localidad'
    });

    $('#dbFechaIngreso').datebox({
        required: true,
        editable: false,
        label: '<span class="label-textbox">Fecha de ingreso</span>',
        labelPosition: 'top',
    }).datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return date <= d1;
        }
    });

    //### INPUTS DE OBJETIVO DE LA CERTIFICACION #############################
    $('#cbVegetacion').combobox({
        required: false,
        hasDownArrow: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        prompt: 'Buscar vegetación',
        label: '<span class="label-textbox"><i class="fab fa-pagelines SeaGreen"></i>&nbsp;Vegetación</span>',
        labelPosition: 'top',
        valueField: 'id',
        textField: 'name',
        url: 'getVegetacionByName.php',
        queryParams: {
            cargadosDatagrid: ''
        },
        limitToList: true,
        onSelect: function (rec) {
            arraySelectedVegetationForTagBox.push(rec.name);
            $('#tagVegetacion').combobox('setValues', arraySelectedVegetationForTagBox);

            var stringSelectedVegetationName = arraySelectedVegetationForTagBox.join();
            var queryParams = $('#cbVegetacion').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedVegetationName;
            setTimeout(function () {
                $('#cbVegetacion').combobox('clear').combobox('reset');
            }, 0);
            $('#cbVegetacion').combobox('reload');
        }
    });

    $('#tagVegetacion').tagbox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        label: '<span class="label-textbox">Vegetación seleccionada</span>',
        labelPosition: 'top',
        limitToList: true,
        hasDownArrow: false,
        valueField: 'id',
        textField: 'id',
        onRemoveTag(value) {
            for (var i = 0; i < arraySelectedVegetationForTagBox.length; i++) {
                if (arraySelectedVegetationForTagBox[i] === value) {
                    arraySelectedVegetationForTagBox.splice(i, 1);
                }
            }
            var stringSelectedVegetationName = arraySelectedVegetationForTagBox.join();
            var queryParams = $('#cbVegetacion').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedVegetationName;
            $('#cbVegetacion').combobox('reload');
        }
    });

    $('#cbArboles').combobox({
        required: false,
        hasDownArrow: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        prompt: 'Buscar arboles',
        label: '<span class="label-textbox"><i class="fas fa-tree ForestGreen"></i>&nbsp;Arboles</span>',
        labelPosition: 'top',
        valueField: 'id',
        textField: 'name',
        url: 'getArbolesByName.php',
        queryParams: {
            cargadosDatagrid: ''
        },
        limitToList: true,
        iconWidth: 22,
        icons: [{
            iconCls: 'ban-icon',
            handler: function (e) {
                arraySelectedTreesForTagBox = [];
                $('#cbArboles').combobox('options').queryParams.cargadosDatagrid = '';
                $('#cbArboles').combobox('reload');
                $('#tagArboles').tagbox('clear').tagbox('setValue', 'Sin especies');
            }
        }],
        onSelect: function (rec) {
            arraySelectedTreesForTagBox.push(rec.name);
            $('#tagArboles').combobox('setValues', arraySelectedTreesForTagBox);
            var stringSelectedTreesName = arraySelectedTreesForTagBox.join();
            var queryParams = $('#cbArboles').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedTreesName;
            setTimeout(function () {
                $('#cbArboles').combobox('clear').combobox('reset');
            }, 0);
            $('#cbArboles').combobox('reload');
        }
    });

    $('#tagArboles').tagbox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        label: '<span class="label-textbox">Arboles seleccionados</span>',
        labelPosition: 'top',
        limitToList: true,
        hasDownArrow: false,
        valueField: 'id',
        textField: 'id',
        onRemoveTag(value) {
            for (var i = 0; i < arraySelectedTreesForTagBox.length; i++) {
                if (arraySelectedTreesForTagBox[i] === value) {
                    arraySelectedTreesForTagBox.splice(i, 1);
                }
            }
            var stringSelectedTreesName = arraySelectedTreesForTagBox.join();
            var queryParams = $('#cbArboles').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedTreesName;
            $('#cbArboles').combobox('reload');
        }
    });

    $('#cbMamiferos').combobox({
        required: false,
        hasDownArrow: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        prompt: 'Buscar mamiferos',
        label: '<span class="label-textbox"><i class="fas fa-paw Brown"></i>&nbsp;Mamiferos</span>',
        labelPosition: 'top',
        valueField: 'id',
        textField: 'name',
        url: 'getMamiferosByName.php',
        queryParams: {
            cargadosDatagrid: ''
        },
        limitToList: true,
        iconWidth: 22,
        icons: [{
            iconCls: 'ban-icon',
            handler: function (e) {
                arraySelectedMammalsForTagBox = [];
                $('#cbMamiferos').combobox('options').queryParams.cargadosDatagrid = '';
                $('#cbMamiferos').combobox('reload');
                $('#tagMamiferos').tagbox('clear').tagbox('setValue', 'Sin especies');
            }
        }],
        onSelect: function (rec) {
            arraySelectedMammalsForTagBox.push(rec.name);
            $('#tagMamiferos').combobox('setValues', arraySelectedMammalsForTagBox);
            var stringSelectedMammalsName = arraySelectedMammalsForTagBox.join();
            var queryParams = $('#cbMamiferos').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedMammalsName;
            setTimeout(function () {
                $('#cbMamiferos').combobox('clear').combobox('reset');
            }, 0);
            $('#cbMamiferos').combobox('reload');
        }
    });

    $('#tagMamiferos').tagbox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        label: '<span class="label-textbox">Mamiferos seleccionados</span>',
        labelPosition: 'top',
        limitToList: true,
        hasDownArrow: false,
        valueField: 'id',
        textField: 'id',
        onRemoveTag(value) {
            for (var i = 0; i < arraySelectedMammalsForTagBox.length; i++) {
                if (arraySelectedMammalsForTagBox[i] === value) {
                    arraySelectedMammalsForTagBox.splice(i, 1);
                }
            }
            var stringSelectedMammalsName = arraySelectedMammalsForTagBox.join();
            var queryParams = $('#cbMamiferos').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedMammalsName;
            $('#cbMamiferos').combobox('reload');
        }
    });

    $('#cbAves').combobox({
        required: false,
        hasDownArrow: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        prompt: 'Buscar aves',
        label: '<span class="label-textbox"><i class="fas fa-crow Chocolate"></i>&nbsp;Aves</span>',
        labelPosition: 'top',
        valueField: 'id',
        textField: 'name',
        url: 'getAvesByName.php',
        queryParams: {
            cargadosDatagrid: ''
        },
        limitToList: true,
        iconWidth: 22,
        icons: [{
            iconCls: 'ban-icon',
            handler: function (e) {
                arraySelectedBirdsForTagBox = [];
                $('#cbAves').combobox('options').queryParams.cargadosDatagrid = '';
                $('#cbAves').combobox('reload');
                $('#tagAves').tagbox('clear').tagbox('setValue', 'Sin especies');
            }
        }],
        onSelect: function (rec) {
            arraySelectedBirdsForTagBox.push(rec.name);
            $('#tagAves').combobox('setValues', arraySelectedBirdsForTagBox);
            var stringSelectedBirdsName = arraySelectedBirdsForTagBox.join();
            var queryParams = $('#cbAves').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedBirdsName;
            setTimeout(function () {
                $('#cbAves').combobox('clear').combobox('reset');
            }, 0);
            $('#cbAves').combobox('reload');
        }
    });

    $('#tagAves').tagbox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        label: '<span class="label-textbox">Aves seleccionadas</span>',
        labelPosition: 'top',
        limitToList: true,
        hasDownArrow: false,
        valueField: 'id',
        textField: 'id',
        onRemoveTag(value) {
            for (var i = 0; i < arraySelectedBirdsForTagBox.length; i++) {
                if (arraySelectedBirdsForTagBox[i] === value) {
                    arraySelectedBirdsForTagBox.splice(i, 1);
                }
            }
            var stringSelectedBirdsName = arraySelectedBirdsForTagBox.join();
            var queryParams = $('#cbAves').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedBirdsName;
            $('#cbAves').combobox('reload');
        }
    });

    $('#cbCuerposAgua').combobox({
        required: false,
        hasDownArrow: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        prompt: 'Buscar cuerpos de agua',
        label: '<span class="label-textbox"><i class="fas fa-tint SkyBlue"></i>&nbsp;Cuerpos de agua</span>',
        labelPosition: 'top',
        valueField: 'id',
        textField: 'name',
        url: 'getCuerposAguaByName.php',
        queryParams: {
            cargadosDatagrid: ''
        },
        limitToList: true,
        iconWidth: 22,
        icons: [{
            iconCls: 'ban-icon',
            handler: function (e) {
                arraySelectedWaterBodiesForTagBox = [];
                $('#cbCuerposAgua').combobox('options').queryParams.cargadosDatagrid = '';
                $('#cbCuerposAgua').combobox('reload');
                $('#tagCuerposAgua').tagbox('clear').tagbox('setValue', 'Sin especies');
            }
        }],
        onSelect: function (rec) {
            arraySelectedWaterBodiesForTagBox.push(rec.name);
            $('#tagCuerposAgua').combobox('setValues', arraySelectedWaterBodiesForTagBox);
            var stringSelectedWaterBodiesName = arraySelectedWaterBodiesForTagBox.join();
            var queryParams = $('#cbCuerposAgua').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedWaterBodiesName;
            setTimeout(function () {
                $('#cbCuerposAgua').combobox('clear').combobox('reset');
            }, 0);
            $('#cbCuerposAgua').combobox('reload');
        }
    });

    $('#tagCuerposAgua').tagbox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        panelMaxHeight: 200,
        label: '<span class="label-textbox">Cuerpos de agua seleccionados</span>',
        labelPosition: 'top',
        limitToList: true,
        hasDownArrow: false,
        valueField: 'id',
        textField: 'id',
        onRemoveTag(value) {
            for (var i = 0; i < arraySelectedWaterBodiesForTagBox.length; i++) {
                if (arraySelectedWaterBodiesForTagBox[i] === value) {
                    arraySelectedWaterBodiesForTagBox.splice(i, 1);
                }
            }
            var stringSelectedWaterBodiesName = arraySelectedWaterBodiesForTagBox.join();
            var queryParams = $('#cbCuerposAgua').combobox('options').queryParams;
            queryParams.cargadosDatagrid = stringSelectedWaterBodiesName;
            $('#cbCuerposAgua').combobox('reload');
        }
    });
    //########################################################################

    $('#cbTecnico').combobox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Técnico</span>',
        labelPosition: 'top',
        valueField: 'id_tecnico',
        textField: 'nombre_completo',
        url: 'getTecnico.php'
    });

    $('#tbVigenciaAPC').numberbox({
        required: true,
        min: 1,
        max: 99,
        label: '<span class="label-textbox">Vigencia del APC</span>',
        labelPosition: 'top'
    });

    $('#cbTienePlanManejo').combobox({
        editable: false,
        panelHeight: 'auto',
        label: '<span class="label-textbox">¿Tiene plan de manejo?</span>',
        labelPosition: 'top',
        required: true,
        valueField: 'text',
        textField: 'text',
        data: [{
            'text': 'Si'
        }, {
            'text': 'No'
        }],
        onChange: function () {
            if ($(this).combobox('getValue') == 'Si') {
                $('#tbVigenciaPlanManejo').numberbox('enable');
            } else {
                $('#tbVigenciaPlanManejo').numberbox('disable');
            }
        }
    });

    $('#tbVigenciaPlanManejo').numberbox({
        required: true,
        min: 1,
        max: 99,
        label: '<span class="label-textbox">Vigencia del plan de manejo</span>',
        labelPosition: 'top'
    });

    $('#cbTenencia').combobox({
        editable: false,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Tenencia</span>',
        labelPosition: 'top',
        required: true,
        valueField: 'text',
        textField: 'text',
        data: [{
            'text': 'Ejido'
        }, {
            'text': 'Privado'
        }]
    });

    $('#tbObservaciones').textbox({
        height: 100,
        required: false,
        multiline: true,
        label: '<span class="label-textbox">Observaciones</span>',
        labelPosition: 'top'
    });

    $('#cbPerteneceAlSello').combobox({
        panelHeight: 'auto',
        label: '<span class="label-textbox">¿Pertenece al sello de biodiversidad?</span>',
        labelPosition: 'top',
        editable: false,
        required: true,
        valueField: 'text',
        textField: 'text',
        data: [{
            'text': 'Si'
        }, {
            'text': 'No'
        }],
        onChange: function () {
            if ($(this).combobox('getValue') == 'Si') {
                $('#cbMarca').combobox('enable');
            } else {
                $('#cbMarca').combobox('clear');
                $('#cbMarca').combobox('disable');
            }
        }
    });

    $('#cbMarca').combobox({
        editable: false,
        panelHeight: 'auto',
        required: true,
        label: '<span class="label-textbox">Selecciona la marca del sello</span>',
        labelPosition: 'top',
        valueField: 'id_marca',
        textField: 'org_marca',
        url: 'getOrgMarcaFilter.php'
    });

    $('#cbZona').combobox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Zona</span>',
        labelPosition: 'top',
        valueField: 'id_zona',
        textField: 'descripcion_zona',
        url: 'getZona.php',
        onSelect: function (rec) {
            $('#cbProyecto').combobox('clear');
            $('#cbFase').combobox('clear');
            var url = 'getProyectos.php?zona=' + rec.id_zona;
            $('#cbProyecto').combobox('reload', url);
        }
    });

    $('#cbProyecto').combobox({
        height: 70,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Proyecto</span>',
        labelPosition: 'top',
        required: true,
        editable: false,
        panelHeight: 'auto',
        multiline: true,
        valueField: 'id_proyecto',
        textField: 'nombre_proyecto',
        onSelect: function (rec) {
            $('#cbFase').combobox('clear');
            var url = 'getFase.php?proyecto=' + rec.id_proyecto;
            $('#cbFase').combobox('reload', url);
        }
    });

    $('#cbFase').combobox({
        required: true,
        editable: false,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Fase</span>',
        labelPosition: 'top',
        valueField: 'fase',
        textField: 'fase'
    });

    $('#fbDocumentos').filebox({
        required: true,
        label: '<span class="label-textbox">Documentos ingresados</span>',
        labelPosition: 'top',
        buttonText: '<i class="fas fa-file-pdf Maroon"></i>',
        accept: 'application/pdf'
    });

    $('#fbFotos').filebox({
        required: true,
        label: '<span class="label-textbox">Archivo de fotos</span>',
        labelPosition: 'top',
        buttonText: '<i class="fas fa-file-word RoyalBlue"></i>',
        accept: 'application/msword'
    });

    $('#fbShapefile').filebox({
        required: false,
        label: '<span class="label-textbox">Shapefile</span>',
        labelPosition: 'top',
        buttonText: '<i class="fas fa-file-archive GoldenRod"></i>',
        accept: 'aplication/zip'
    });

    $("#btnSaveFormAddNew").click(function () {
        saveAddNew();
    });


    //FORMULARIO AGREGAR NUEVO PROPIETARIO
    $('#tbNombrePropietario').textbox({
        required: true,
        label: '<span class="label-textbox">Nombre</span>',
        //prompt: 'Escribe el nombre del propietario',
        labelPosition: 'top'
    });

    $('#tbAmatPropietario').textbox({
        required: true,
        label: '<span class="label-textbox">Apellido paterno</span>',
        //prompt: 'Escribe el apellido paterno',
        labelPosition: 'top'
    });

    $('#tbApatPropietario').textbox({
        required: true,
        label: '<span class="label-textbox">Apellido materno</span>',
        //prompt: 'Escribe el apellido materno',
        labelPosition: 'top'
    });

    $("#btnSaveNewPropietary").click(function () {
        saveNewPropietary();
    });

    //#####################################################################################


    //FORMULARIO EDITAR REGISTRO
    $('#tbIdAPC').textbox({
        required: true,
        editable: false
    });

    $('#tbNombreAPCTempModalEdit').textbox({
        required: true,
        editable: false
    });

    $('#tbFolioTempModalEdit').textbox({
        required: true,
        editable: false
    });

    $('#tbEstatusTempModalEdit').textbox({
        required: true,
        editable: false,
        onChange: function (value) {
            //FUNCION PARA MOSTRAR LOS ELEMENTOS DIFERENTES AL ESTATUS ACTUAL
            var data = [];
            switch (value) {
                case 'Certificada':
                    data = [{
                        'text': 'Regresado a SEDEMA por error'
                    }];
                    $('#cbEstatusModalEdit').combobox('loadData', data);
                    break;
                case 'En proceso':
                    data = [{
                        'text': 'Certificada'
                    }, {
                        'text': 'No procede'
                    }];
                    $('#cbEstatusModalEdit').combobox('loadData', data);
                    break;
                case 'No procede':
                    data = [];
                    $('#cbEstatusModalEdit').combobox('loadData', data);
                    break;
                case 'Regresado a SEDEMA por error':
                    data = [{
                        'text': 'Certificada'
                    }];
                    $('#cbEstatusModalEdit').combobox('loadData', data);
                    break;
            }

        }
    });

    $('#tbGeomPointTempModalEdit').textbox({
        required: false,
        editable: false,
        onChange: function () {
            //FORZAMOS A 'tbGeomResultTempModalEdit' A CAMBIAR SU VALOR PARA DISPARAR SU EVENTO
            $('#tbGeomResultTempModalEdit').textbox('setValue', ' ');
            $('#tbGeomResultTempModalEdit').textbox('clear');
        }
    });

    $('#tbGeomPolyTempModalEdit').textbox({
        required: false,
        editable: false,
        onChange: function () {
            //FORZAMOS A 'tbGeomResultTempModalEdit' A CAMBIAR SU VALOR PARA DISPARAR SU EVENTO
            $('#tbGeomResultTempModalEdit').textbox('setValue', ' ');
            $('#tbGeomResultTempModalEdit').textbox('clear');
        }
    });

    $('#tbGeomResultTempModalEdit').textbox({
        required: true,
        editable: false,
        onChange: function () {
            var geom_puntos = $('#tbGeomPointTempModalEdit').textbox('getValue');
            var geom_poligonos = $('#tbGeomPolyTempModalEdit').textbox('getValue')
            if (geom_puntos === '' && geom_poligonos === '') {
                $('#tbGeomResultTempModalEdit').textbox('setValue', 'Sin shapefile');
                $('#selectedTieneShapefileModalEdit').html('<i class="fas fa-globe-americas without-shapefile"></i>&nbsp;Sin shapefile');
                $('#cbOpcionModalEdit').combobox('loadData', [{
                    'text': 'Cambiar estatus'
                }, {
                    'text': 'Subir shapefile'
                }]);
            } else {
                $('#tbGeomResultTempModalEdit').textbox('setValue', 'Con shapefile');
                $('#selectedTieneShapefileModalEdit').html('<i class="fas fa-globe-americas with-shapefile"></i>&nbsp;Shapefile guardado');
                $('#cbOpcionModalEdit').combobox('loadData', [{
                    'text': 'Cambiar estatus'
                }]);
            }
        }
    });

    $('#cbOpcionModalEdit').combobox({
        required: true,
        editable: false,
        limitToList: true,
        hasDownArrow: true,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Selecciona una acción</span>',
        labelPosition: 'top',
        valueField: 'text',
        textField: 'text',
        data: [{
            'text': 'Cambiar estatus'
        }, {
            'text': 'Subir shapefile'
        }],
        onSelect: function (rec) {
            switch (rec.text) {
                case 'Cambiar estatus':
                    //console.log("selecciono cambiar estatus");
                    statusFieldsModalEdit('show');
                    shapefileFieldsModalEdit('hide');
                    noProcedeWrapperModalEdit('hide');
                    certificadaWrapperModalEdit('hide');
                    break;
                case 'Subir shapefile':
                    //console.log("selecciono subir el shapefile");
                    shapefileFieldsModalEdit('show');
                    statusFieldsModalEdit('hide');
                    noProcedeWrapperModalEdit('hide');
                    certificadaWrapperModalEdit('hide');
                    break;
                default:
            }
        }
    });

    //STATUS FIELDS
    $('#cbEstatusModalEdit').combobox({
        required: true,
        editable: false,
        limitToList: true,
        hasDownArrow: true,
        panelHeight: 'auto',
        label: '<span class="label-textbox">Selecciona el estatus</span>',
        labelPosition: 'top',
        valueField: 'text',
        textField: 'text',
        data: [{
            'text': 'Certificada'
        }, {
            'text': 'No procede'
        }, {
            'text': 'Regresado a SEDEMA por error'
        }],
        onSelect: function (rec) {
            console.log(rec.text);
            //VALIDAMOS LA OPCION SELECCIONADA PARA MOSTRAR/OCULTAR LOS INPUTS CORRECTOS
            switch (rec.text) {
                case 'Certificada':
                    noProcedeWrapperModalEdit('hide');
                    certificadaWrapperModalEdit('show');
                    break;
                case 'No procede':
                    certificadaWrapperModalEdit('hide');
                    noProcedeWrapperModalEdit('show');
                    break;
                case 'Regresado a SEDEMA por error':
                    certificadaWrapperModalEdit('hide');
                    noProcedeWrapperModalEdit('show');
                    break;
            }



        }
    });


    //DEPENDANT FIELDS
    $('#tbMotivoNoProcedeRegresadoModalEdit').textbox({
        required: true,
        height: 100,
        multiline: true,
        label: '<span class="label-textbox">Motivo no procede/ regresado</span>',
        labelPosition: 'top'
    });

    $('#dbFechaCertificacionModalEdit').datebox({
        required: true,
        label: '<span class="label-textbox">Fecha de certificación</span>',
        labelPosition: 'top'
    });

    $('#tbNumeroSEDEMAModalEdit').textbox({
        required: true,
        label: '<span class="label-textbox">Numero SEDEMA</span>',
        labelPosition: 'top'
    });

    $('#fbCertificadoModalEdit').filebox({
        required: true,
        label: '<span class="label-textbox">Certificado</span>',
        labelPosition: 'top',
        buttonText: '<i class="fas fa-file-pdf Maroon"></i>',
        accept: 'application/pdf'
    });


    //SHAPEFILE FIELDS
    $('#fbShapefileModalEdit').filebox({
        required: true,
        label: '<span class="label-textbox">Shapefile</span>',
        labelPosition: 'top',
        buttonText: '<i class="fas fa-file-archive GoldenRod"></i>',
        accept: 'aplication/zip'
    });

    $("#btnSaveFormEdit").click(function () {
        updateAPC();
    });
    //#####################################################################################



    //##MEDIA QUERIES
    //#1. RECREAR EL DATAGRID SIN LAS 'frozenColumns' EN RESOLUCIONES MENORES A 767PX
    /*var tablet = window.matchMedia('screen and (max-width: 767px)');
    tablet.addListener(validation);

    function validation(event) {
        if (event.matches) {            .
            $('#dg').datagrid({
                title: 'Cambió el titulo',
                frozenColumns:[[

                ]],
                columns:[[
                    {field:'estatus',title:'Estatus',width:60,align:'center'},
                    {field:'folio',title:'Folio',width:80,align:'left'},
                    {field:'nombre_apc',title:'Nombre del APC',width:150,align:'left'},
                    {field:'nombre_completo',title:'Propietario(s)',width:250, align:'left'}
                ]]
            });
        } else {
            //Revert frozen columns
        }
    }
    validation(tablet);*/

});