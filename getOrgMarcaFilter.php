<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$sql = pg_query($link, "SELECT id_marca, (organizacion || ' - ' || marca) AS org_marca FROM marca_sello ORDER BY organizacion ASC");
$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
