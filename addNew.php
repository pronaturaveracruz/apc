<?php

## PHP ShapeFile - Gaspare Sganga #####################################################
require_once 'includes/php-shapefile-2.4.3/src/ShapeFileAutoloader.php';
\ShapeFile\ShapeFileAutoloader::register();
use \ShapeFile\ShapeFile;
#######################################################################################

## Date configurations #################################################################
date_default_timezone_set("Mexico/General");
$fechaActual = date('Y-m-d');
#######################################################################################

## Session ############################################################################
session_start();
$userLogged = isset($_SESSION['userLogged']) ? $_SESSION['userLogged'] : '';
#######################################################################################

## Database connection ################################################################
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();
$linkMySQL = ConectarseMySQLMegaBD();
#######################################################################################

## Functions ################################################################
function isExtensionValid($file, $extensionsExpected)
{
    $valid = 0;
    if ($file['error'] === 0) {
        if (preg_match("/\.\w+$/", $file['name'], $matches)) {
            $arrayExploded = explode(",", $extensionsExpected);
            foreach ($arrayExploded as $currentExtension) {
                if ($matches[0] === '.' . $currentExtension) {
                    $valid++;
                }
            }
            if ($valid > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}

function recursiveRemoveDirectory($path)
{
    $files = glob($path . '/*');
    foreach ($files as $file) {
        if (is_dir($file)) {
            recursiveRemoveDirectory($file);
        } else {
            unlink($file);
        }
    }
    rmdir($path);
    return;
}

function haveShapefile($file)
{
    if ($file["error"] === 0) {
        return true;
    }
    return false;
}

function convertToValidPath($input)
{
    $input = str_replace('.', '_', $input);
    $input = str_replace('/', '_', $input);
    $input = str_replace('-', '_', $input);
    $input = str_replace('?', '_', $input);
    $input = str_replace('*', '_', $input);
    $input = str_replace('+', '_', $input);
    $input = str_replace('%', '_', $input);
    $input = str_replace(':', '_', $input);
    $input = str_replace('<', '_', $input);
    $input = str_replace('>', '_', $input);
    $input = str_replace('|', '_', $input);
    $input = str_replace(',', '_', $input);
    $input = str_replace('ñ', 'n', $input);
    $input = str_replace('á', 'a', $input);
    $input = str_replace('é', 'e', $input);
    $input = str_replace('í', 'i', $input);
    $input = str_replace('ó', 'o', $input);
    $input = str_replace('ú', 'u', $input);
    $input = str_replace('Ñ', 'n', $input);
    $input = str_replace('Á', 'a', $input);
    $input = str_replace('É', 'e', $input);
    $input = str_replace('Í', 'i', $input);
    $input = str_replace('Ó', 'o', $input);
    $input = str_replace('Ú', 'u', $input);
    $input = strtolower($input);
    $input = ucwords($input);
    $input = str_replace(' ', '', $input);
    return $input;
}

function sendToConsole($type, $message)
{
    switch ($type) {
        case 'log':
            echo "<script>console.log('" . $message . "');</script>";
            break;
        case 'warn':
            echo "<script>console.warn('" . $message . "');</script>";
            break;
        case 'error':
            echo "<script>console.error('" . $message . "');</script>";
            break;
    }
}

function insertDataToDB(
    $link,
    $linkMySQL,
    $arrayPropietariosIDs,
    $folio,
    $nombreAPC,
    $hectareas,
    $hectareasCertificadas,
    $estado,
    $municipio,
    $localidad,
    $fechaIngreso,
    $vegetaciones,
    $arboles,
    $mamiferos,
    $aves,
    $cuerposAgua,
    $tecnico,
    $zona,
    $proyecto,
    $fase,
    $vigenciaAPC,
    $tienePlanManejo,
    $vigenciaPlanManejo,
    $tenencia,
    $observaciones,
    $perteneceAlSello,
    $marca,
    $userLogged,
    $fechaActual
) {
    $vigenciaPlanManejo = $vigenciaPlanManejo != '' ? $vigenciaPlanManejo : 0;

    #VERIFICAR SI ESE FOLIO YA FUE INGRESADO A LA BASE DE DATOS #################
    $cuantosRepetidos = 0;
    $queryFolioDuplicado = pg_query($link, "SELECT COUNT(*) FROM apc_principal WHERE folio = '$folio'");
    while ($rowFolioDuplicado = pg_fetch_row($queryFolioDuplicado)) {
        if ($rowFolioDuplicado[0] > 0) {
            $cuantosRepetidos++;
        }
    }
    #############################################################################

    ##EN CASO DE NO TENER REPETIDO EL FOLIO CONTINUAMOS###
    if ($cuantosRepetidos > 0) {
        echo "ERROR,Este folio ya lo ingresaste anteriormente." . "\n";
        //sendToConsole("error", "Este folio ya lo ingresaste anteriormente.");
    } else {
        #VERIFICAMOS SI LAS EXTENSIONES DE LOS ARCHIVOS ADJUNTOS SON CORRECTAS [PDF, DOCX]
        $docsValid = isExtensionValid($_FILES['fbDocumentos'], 'pdf');
        $fotosValid = isExtensionValid($_FILES['fbFotos'], 'doc,docx');

        if ($docsValid && $fotosValid) {
            //sendToConsole("log", "Extensiones de docs y fotos validas");
            #HACER PROCEDIMIENTOS SIN SHAPEFILE
            #1. PREPARAMOS LOS NOMBRES DE LAS CARPETAS DESTINO         
            $folderNameAPC = convertToValidPath($nombreAPC);
            ##DEFINIMOS LAS RUTAS FINALES
            $generalPath = "documentacion/F" . $folio . "_" . $folderNameAPC . "/";
            $docsPath = $generalPath . "Documentos_ingresados/";
            $picturesPath = $generalPath . "Fotos/";
            ##DEFINIMOS LOS NOMBRES DE ARCHIVOS
            $docsFileName = "F" . $folio . "_" . $folderNameAPC . "_doc.pdf";
            preg_match("/\.\w+$/", $_FILES['fbFotos']['name'], $matches);
            $picturesFileName = "F" . $folio . "_" . $folderNameAPC . "_fotos" . $matches[0];

            //sendToConsole("log", "DOCS PATH: " . $docsPath);
            //sendToConsole("log", "PIC PATH: " . $picturesPath);

            #SUBIMOS LOS ARCHIVOS ADJUNTOS
            $contadorArchivosUpload = 0;
            if (file_exists($generalPath) || @mkdir($generalPath)) {
                mkdir($docsPath, 0755, true);
                $origenDocumentos = $_FILES["fbDocumentos"]["tmp_name"];
                $destinoDocumentos = $docsPath . $docsFileName;
                if (@move_uploaded_file($origenDocumentos, $destinoDocumentos)) {
                    $contadorArchivosUpload++;
                    //sendToConsole("log", "Archivo de documentacion: UPLOAD OK");
                } else {
                    echo "ERROR, No se pudo subir al servidor el archivo de documentación.";
                    //sendToConsole("error", "No se pudo subir al servidor el archivo de documentación.");
                }
            } else {
                echo "ERROR, No se pudo crear en el servidor la carpeta de documentación.";
                //sendToConsole("error", "No se pudo crear en el servidor la carpeta de documentación.");
            }

            if (file_exists($generalPath) || @mkdir($generalPath)) {
                mkdir($picturesPath, 0755, true);
                $origenFotos = $_FILES["fbFotos"]["tmp_name"];
                $destinoFotos = $picturesPath . $picturesFileName;
                if (@move_uploaded_file($origenFotos, $destinoFotos)) {
                    $contadorArchivosUpload++;
                    //sendToConsole("log", "Archivo de fotos: UPLOAD OK");
                } else {
                    echo "ERROR, No se pudo subir al servidor el archivo de fotos.";
                    //sendToConsole("error", "No se pudo subir al servidor el archivo de fotos.");
                }
            } else {
                echo "ERROR, No se pudo crear en el servidor la carpeta de fotos.";
                //sendToConsole("error", "No se pudo crear en el servidor la carpeta de fotos.");
            }

            if ($contadorArchivosUpload === 2) {
                //sendToConsole("log", "OK, Ambos archivos se subieron correctamente (Documentos y fotos).");

                ##COMENZAMOS LOS INSERTS EN LA BASE DE DATOS
                #1. HACEMOS CONSULTA PARA DETERMINAR QUE 'ID_CZP' LE CORRESPONDE EN BASE A LAS VARIABLES POST
                $sqlZonaProyecto = pg_query($link, "SELECT id_czp FROM concentrado_zona_proyecto WHERE id_zona = $zona AND id_proyecto = $proyecto AND fase = '$fase'");
                while ($rowZonaProyecto = pg_fetch_row($sqlZonaProyecto)) {
                    $id_czp = $rowZonaProyecto[0];
                }
                #############################################################################################

                #2. DETERMINAMOS SI INSERTAREMOS EL 'id_marca' EN LA BD. SI NO NO (DEJARLO CON VALOR: NULL) ###########
                if ($marca == '') {
                    //sendToConsole('log', 'NO tuvo marca');
                    $queryInsert = "INSERT INTO apc_principal(
                        folio,
                        nombre_apc,
                        hectareas,
                        hectareas_certificadas,
                        estado,
                        municipio,
                        localidad,
                        fecha_ingreso,
                        estatus,
                        tecnico,
                        vigencia,
                        proteccion_legal,
                        cuenta_plan_manejo,
                        vigencia_plan_manejo,
                        tenencia,
                        observaciones,
                        fecha_ult_modif,
                        usuario_ult_modif,
                        ruta_doc_ingresados,
                        ruta_fotografias,
                        id_czp,
                        id_figura_legal)
                    VALUES ('$folio',
                    '$nombreAPC',
                    $hectareas,
                    $hectareasCertificadas,
                    '$estado',
                    '$municipio',
                    '$localidad',
                    '$fechaIngreso',
                    'En proceso',
                    $tecnico,
                    $vigenciaAPC,
                    'Gobierno del Estado de Veracruz',
                    '$tienePlanManejo',
                    $vigenciaPlanManejo,
                    '$tenencia',
                    '$observaciones',
                    '$fechaActual',
                    '$userLogged',
                    '$destinoDocumentos',
                    '$destinoFotos',
                    $id_czp,
                    1)";
                } else {
                    //sendToConsole('log', 'SI tuvo marca');
                    $queryInsert = "INSERT INTO apc_principal(
                        folio,
                        nombre_apc,
                        hectareas,
                        hectareas_certificadas,
                        estado,
                        municipio,
                        localidad,
                        fecha_ingreso,
                        estatus,
                        tecnico,
                        vigencia,
                        proteccion_legal,
                        cuenta_plan_manejo,
                        vigencia_plan_manejo,
                        tenencia,
                        observaciones,
                        fecha_ult_modif,
                        usuario_ult_modif,
                        ruta_doc_ingresados,
                        ruta_fotografias,
                        id_czp,
                        id_marca,
                        id_figura_legal)
                    VALUES ('$folio',
                    '$nombreAPC',
                    $hectareas,
                    $hectareasCertificadas,
                    '$estado',
                    '$municipio',
                    '$localidad',
                    '$fechaIngreso',
                    'En proceso',
                    $tecnico,
                    $vigenciaAPC,
                    'Gobierno del Estado de Veracruz',
                    '$tienePlanManejo',
                    $vigenciaPlanManejo,
                    '$tenencia',
                    '$observaciones',
                    '$fechaActual',
                    '$userLogged',
                    '$destinoDocumentos',
                    '$destinoFotos',
                    $id_czp,
                    $marca,
                    1)";
                }
                #############################################################################################

                #3. INSERTAMOS LOS DATOS EN LA TABLA 'apc_principal'.
                //echo "\n\n" . $queryInsert;
                $resultInsert = pg_query($link, $queryInsert);
                if ($resultInsert) {
                    $idLastInserted = pg_query($link, "SELECT MAX(id_principal) FROM apc_principal");
                    $rowLastInserted = pg_fetch_row($idLastInserted);
                    $lastInserted = $rowLastInserted[0];
                    #4. INSERTAMOS LOS PROPIETARIOS EN LA TABLA 'prop_apcs'.
                    $insertPropietaries = insertPropietaries($link, $arrayPropietariosIDs, $lastInserted);
                    if ($insertPropietaries) {
                        #5. INSERTAMOS LA VEGETACION EN LA TABLA 'tv_apcs'.
                        //sendToConsole("log", "Insertando la vegetacion...");
                        $insertVegetation = insertVegetation($link, $vegetaciones, $lastInserted);
                        if ($insertVegetation) {
                            //sendToConsole("log", "Se insertó correctamente toda la vegetación.");
                            #6. INSERTAMOS LOS OBJETIVOS DE LA CERTIFICACIÓN
                            //sendToConsole("log", "Insertando los objetivos de la certificación");
                            $insertObjetivosCertificacion = insertObjetivosCertificacion($link, $linkMySQL, $arboles, $mamiferos, $aves, $cuerposAgua, $lastInserted);
                            if ($insertObjetivosCertificacion) {
                                //sendToConsole("log", "Se insertaron correctamente todos los OC.");
                                return true;
                            }

                        }

                    } else {
                        echo "ERROR, No se pudieron insertar los propietarios en la BD.";
                        //sendToConsole("error", "No se pudieron insertar los propietarios en la BD.");
                    }

                }
                #############################################################################################

            } else {
                echo "ERROR, No se pudieron subir los archivos al servidor.";
                //sendToConsole("error", "No se pudieron subir los archivos al servidor.");
            }
        } else {
            echo "ERROR, El archivo de documentación o fotos tiene una extensión no permitida.";
            //sendToConsole("error", "El archivo de documentación o fotos tiene una extensión no permitida.");
        }
    } //FIN DEL ELSE (CUANTOS REPETIDOS > 0)

    #####################
    return false;
}

function insertPropietaries($link, $arrayPropietariosIDs, $lastInserted)
{
    $counterInsertsOK = 0;
    $countArrayPropietariosIDs = count($arrayPropietariosIDs);
    foreach ($arrayPropietariosIDs as $currentPropietarioID) {
        $resultPropietaries = pg_query($link, "INSERT INTO prop_apcs(id_propietario,id_apc) VALUES ('$currentPropietarioID','$lastInserted')");
        if ($resultPropietaries) {
            $counterInsertsOK++;
        }
    }
    if ($counterInsertsOK == $countArrayPropietariosIDs) {
        return true;
    }
    return false;
}

function insertVegetation($link, $vegetaciones, $lastInserted)
{
    $stringVegetaciones = "";
    $arrayVegetationsIDs = array();
    foreach ($vegetaciones as $vegetacion) {
        $stringVegetaciones .= "'" . $vegetacion . "',";
    }
    $stringVegetaciones = substr($stringVegetaciones, 0, -1);
    $sqlGetVegetationsIDs = "SELECT id
    FROM (
            SELECT
            id_tipo_vegetacion                      AS id,
            (tipo_vegetacion || ' - ' || categoria) AS name
            FROM public.tipo_vegetacion
            WHERE categoria != 'No especificada'
        ) AS final
    WHERE name IN ($stringVegetaciones)";
    $queryGetVegetationsIDs = pg_query($link, $sqlGetVegetationsIDs);
    while ($row = pg_fetch_assoc($queryGetVegetationsIDs)) {
        array_push($arrayVegetationsIDs, $row['id']);
    }
    $counterInsertsOK = 0;
    $countArrayVegetationsIDs = count($arrayVegetationsIDs);
    $order = 1;
    foreach ($arrayVegetationsIDs as $currentVegetacionID) {
        $resultVegetations = pg_query($link, "INSERT INTO tv_apcs(id_apc, id_tipo_vegetacion, orden) VALUES ('$lastInserted', '$currentVegetacionID', $order)");
        if ($resultVegetations) {
            $counterInsertsOK++;
            $order++;
        }
    }
    if ($counterInsertsOK == $countArrayVegetationsIDs) {
        return true;
    }
    return false;
}

function insertObjetivosCertificacion($link, $linkMySQL, $arboles, $mamiferos, $aves, $cuerposAgua, $lastInserted)
{
    $globalCounterSuccess = 0;
    $goal = 4;
    #1.ARBOLES
    $stringArboles = "";
    $arrayTreesIDs = array();
    foreach ($arboles as $arbol) {
        $stringArboles .= "'" . $arbol . "',";
    }
    $stringArboles = substr($stringArboles, 0, -1);
    //echo "\nstringArboles: " . $stringArboles;
    if ($stringArboles != 'Sin especies') {
        $sqlGetTreesIDs = "SELECT id
        FROM (
            SELECT
                id_num AS id,
                nombre_cientifico AS name
            FROM especies
        ) AS final
        WHERE name IN ($stringArboles)
        ORDER BY name ASC";
        $queryGetTreesIDs = mysqli_query($linkMySQL, $sqlGetTreesIDs);
        while ($row = mysqli_fetch_assoc($queryGetTreesIDs)) {
            array_push($arrayTreesIDs, $row['id']);
        }
        $counterInsertsOK = 0;
        $countArrayTreesIDs = count($arrayTreesIDs);
        foreach ($arrayTreesIDs as $currentTreeID) {
            $queryGetScientificName = mysqli_query($linkMySQL, "SELECT nombre_cientifico FROM especies WHERE id_num='$currentTreeID'");
            $rowScientificName = mysqli_fetch_row($queryGetScientificName);
            $currentTreeScientificName = $rowScientificName[0];
            $resultTrees = pg_query($link, "INSERT INTO objetivo_certificacion(id_apc, id_especie, categoria, nombre_cientifico) VALUES ('$lastInserted', '$currentTreeID', 'Arboles', '$currentTreeScientificName')");
            if ($resultTrees) {
                $counterInsertsOK++;
            }
        }
        if ($counterInsertsOK == $countArrayTreesIDs) {
            $globalCounterSuccess++;
        }
    } else if ($stringArboles == 'Sin especies') {
        $globalCounterSuccess++;
    }

    #2. MAMIFEROS
    $stringMamiferos = "";
    $arrayMammalsIDs = array();
    foreach ($mamiferos as $mamifero) {
        $stringMamiferos .= "'" . $mamifero . "',";
    }
    $stringMamiferos = substr($stringMamiferos, 0, -1);
    //echo "\nstringMamiferos: " . $stringMamiferos;
    if ($stringMamiferos != 'Sin especies') {
        $sqlGetMammalsIDs = "SELECT id
        FROM (
            SELECT idMamiferos AS id, nombre_cientifico AS name
            FROM mamiferos
        ) AS final
        WHERE name IN ($stringMamiferos)
        ORDER BY name ASC";
        $queryGetMammalsIDs = mysqli_query($linkMySQL, $sqlGetMammalsIDs);
        while ($row = mysqli_fetch_assoc($queryGetMammalsIDs)) {
            array_push($arrayMammalsIDs, $row['id']);
        }
        $counterInsertsOK = 0;
        $countArrayMammalsIDs = count($arrayMammalsIDs);
        foreach ($arrayMammalsIDs as $currentMammalID) {
            $queryGetScientificName = mysqli_query($linkMySQL, "SELECT nombre_cientifico FROM mamiferos WHERE idMamiferos='$currentMammalID'");
            $rowScientificName = mysqli_fetch_row($queryGetScientificName);
            $currentMammalScientificName = $rowScientificName[0];
            $resultMammals = pg_query($link, "INSERT INTO objetivo_certificacion(id_apc, id_especie, categoria, nombre_cientifico) VALUES ('$lastInserted', '$currentMammalID', 'Mamiferos', '$currentMammalScientificName')");
            if ($resultMammals) {
                $counterInsertsOK++;
            }
        }
        if ($counterInsertsOK == $countArrayMammalsIDs) {
            $globalCounterSuccess++;
        }
    } else if ($stringMamiferos == 'Sin especies') {
        $globalCounterSuccess++;
    }

    #3. AVES
    $stringAves = "";
    $arrayBirdsIDs = array();
    foreach ($aves as $ave) {
        $stringAves .= "'" . $ave . "',";
    }
    $stringAves = substr($stringAves, 0, -1);
    //echo "\nstringAves: " . $stringAves;
    if ($stringAves != 'Sin especies') {
        $sqlGetBirdsIDs = "SELECT id
        FROM (
            SELECT id_aves_ecoforestal AS id, nombre_cientifico AS name
            FROM aves_ecoforestal
        ) AS final
        WHERE name IN ($stringAves)
        ORDER BY name ASC";
        $queryGetBirdsIDs = mysqli_query($linkMySQL, $sqlGetBirdsIDs);
        while ($row = mysqli_fetch_assoc($queryGetBirdsIDs)) {
            array_push($arrayBirdsIDs, $row['id']);
        }
        $counterInsertsOK = 0;
        $countArrayBirdsIDs = count($arrayBirdsIDs);
        foreach ($arrayBirdsIDs as $currentBirdID) {
            $queryGetScientificName = mysqli_query($linkMySQL, "SELECT nombre_cientifico FROM aves_ecoforestal WHERE id_aves_ecoforestal='$currentBirdID'");
            $rowScientificName = mysqli_fetch_row($queryGetScientificName);
            $currentBirdScientificName = $rowScientificName[0];
            $resultBirds = pg_query($link, "INSERT INTO objetivo_certificacion(id_apc, id_especie, categoria, nombre_cientifico) VALUES ('$lastInserted', '$currentBirdID', 'Aves', '$currentBirdScientificName')");
            if ($resultMammals) {
                $counterInsertsOK++;
            }
        }
        if ($counterInsertsOK == $countArrayBirdsIDs) {
            $globalCounterSuccess++;
        }
    } else if ($stringAves == 'Sin especies') {
        $globalCounterSuccess++;
    }

    #4. CUERPOS DE AGUA
    $stringCuerposAgua = "";
    $arrayWaterBodiesIDs = array();
    foreach ($cuerposAgua as $cuerpoAgua) {
        $stringCuerposAgua .= "'" . $cuerpoAgua . "',";
    }
    $stringCuerposAgua = substr($stringCuerposAgua, 0, -1);
    //echo "\nstringCuerposAgua: " . $stringCuerposAgua;
    if ($stringCuerposAgua != 'Sin especies') {
        $sqlGetWaterBodiesIDs = "SELECT id
        FROM (
            SELECT idca AS id, nombre AS name
            FROM oc_cuerpos_agua
        ) AS final
        WHERE name IN ($stringCuerposAgua)
        ORDER BY name ASC";
        $queryGetWaterBodiesIDs = pg_query($link, $sqlGetWaterBodiesIDs);
        while ($row = pg_fetch_assoc($queryGetWaterBodiesIDs)) {
            array_push($arrayWaterBodiesIDs, $row['id']);
        }
        $counterInsertsOK = 0;
        $countArrayWaterBodiesIDs = count($arrayWaterBodiesIDs);
        foreach ($arrayWaterBodiesIDs as $currentWaterBodyID) {
            $queryGetName = pg_query($link, "SELECT nombre FROM oc_cuerpos_agua WHERE idca='$currentWaterBodyID'");
            $rowName = pg_fetch_row($queryGetName);
            $currentWaterBodiesName = $rowName[0];
            $resultWaterBodies = pg_query($link, "INSERT INTO objetivo_certificacion(id_apc, id_especie, categoria, nombre_cientifico) VALUES ('$lastInserted', '$currentWaterBodyID', 'Cuerpos de agua', '$currentWaterBodiesName')");
            if ($resultWaterBodies) {
                $counterInsertsOK++;
            }
        }
        if ($counterInsertsOK == $countArrayWaterBodiesIDs) {
            $globalCounterSuccess++;
        }
    } else if ($stringCuerposAgua == 'Sin especies') {
        $globalCounterSuccess++;
    }

    #5. FINAL
    if ($globalCounterSuccess === $goal) {
        return true;
    }
    return false;
}

function insertGeomToPostgis($link, $arrayPropietariosNamesSortedASC, $nombreAPC, $folio, $geom)
{
    $stringPropietariosNamesSortedASC = '';
    $stringPropietariosNamesSortedASC = join(',', $arrayPropietariosNamesSortedASC);
    //sendToConsole("log", "Intentando guardar en PostGIS");
    $sqlInsertGeom = "INSERT INTO shape_poligonos_ccl_merge_final(propietari, nombre_apc, folio, geom) VALUES ('$stringPropietariosNamesSortedASC', '$nombreAPC', '$folio', ST_Multi(ST_GeomFromText('$geom', 48402))     )";
    //echo "\n\n" . $sqlInsertGeom;
    $resultInsertGeom = pg_query($link, $sqlInsertGeom);
    if ($resultInsertGeom) {
        return true;
    }
    return false;
}
#######################################################################################

if ($userLogged != '') {
    #RECUPERAMOS VARIABLES NECESARIAS PARA DEFINIR LA RUTA DE LOS ARCHIVOS
    $propietarios = $_POST['tagPropietarios'];
    $folio = trim($_POST['tbFolio']);
    $nombreAPC = trim($_POST['tbNombreAPC']);
    $hectareas = trim($_POST['nbHectareas']);
    $hectareasCertificadas = trim($_POST['nbHectareasCertificadas']);
    $estado = trim($_POST['cbEstado']);
    $municipio = trim($_POST['cbMunicipio']);
    $localidad = trim($_POST['cbLocalidad']);
    $fechaIngreso = trim($_POST['dbFechaIngreso']);
    $vegetaciones = $_POST['tagVegetacion'];
    ####OBJETIVO DE LA CERTIFICACION
    $arboles = $_POST['tagArboles'];
    $mamiferos = $_POST['tagMamiferos'];
    $aves = $_POST['tagAves'];
    $cuerposAgua = $_POST['tagCuerposAgua'];
    ####ADICIONALES
    $tecnico = trim($_POST['cbTecnico']);
    $zona = trim($_POST['cbZona']);
    $proyecto = trim($_POST['cbProyecto']);
    $fase = trim($_POST['cbFase']);
    $vigenciaAPC = trim($_POST['tbVigenciaAPC']);
    $tienePlanManejo = trim($_POST['cbTienePlanManejo']);
    $vigenciaPlanManejo = trim($_POST['tbVigenciaPlanManejo']);
    $tenencia = trim($_POST['cbTenencia']);
    $observaciones = trim($_POST['tbObservaciones']); //Opcional
    $perteneceAlSello = trim($_POST['cbPerteneceAlSello']);
    $marca = trim($_POST['cbMarca']); //Opcional

    #VERIFICAMOS QUE LOS DATOS ESTEN DEFINIDOS Y QUE NO ESTEN VACIÓS
    if (
        isset($propietarios) && !empty($propietarios) &&
        isset($folio) && $folio != '' &&
        isset($nombreAPC) && $nombreAPC != '' &&
        isset($hectareas) && $hectareas != '' &&
        isset($hectareasCertificadas) && $hectareasCertificadas != '' &&
        isset($estado) && $estado != '' &&
        isset($municipio) && $municipio != '' &&
        isset($localidad) && $localidad != '' &&
        isset($fechaIngreso) && $fechaIngreso != '' &&
        isset($tecnico) && $tecnico != '' &&
        isset($zona) && $zona != '' &&
        isset($proyecto) && $proyecto != '' &&
        isset($fase) && $fase != '' &&
        isset($vigenciaAPC) && $vigenciaAPC != '' &&
        isset($tienePlanManejo) && $tienePlanManejo != '' &&
        isset($tenencia) && $tenencia != '' &&
        isset($perteneceAlSello) && $perteneceAlSello != ''
    ) {
        /*print_r($propietarios);
        echo "Folio: " . $folio . "\n";
        echo "nombreAPC: " . $nombreAPC . "\n";
        echo "hectareas: " . $hectareas . "\n";
        echo "hectareasCertificadas: " . $hectareasCertificadas . "\n";
        echo "estado: " . $estado . "\n";
        echo "municipio: " . $municipio . "\n";
        echo "localidad: " . $localidad . "\n";
        echo "fechaIngreso: " . $fechaIngreso . "\n";
        print_r($vegetaciones);
        print_r($arboles);
        print_r($mamiferos);
        print_r($aves);
        print_r($cuerposAgua);
        echo "tecnico: " . $tecnico . "\n";
        echo "zona: " . $zona . "\n";
        echo "proyecto: " . $proyecto . "\n";
        echo "fase: " . $fase . "\n";
        echo "vigenciaAPC: " . $vigenciaAPC . "\n";
        echo "tienePlanManejo: " . $tienePlanManejo . "\n";
        echo "vigenciaPlanManejo: " . $vigenciaPlanManejo . "\n";
        echo "tenencia: " . $tenencia . "\n";
        echo "observaciones: " . $observaciones . "\n";
        echo "perteneceAlSello: " . $perteneceAlSello . "\n";
        echo "marca: " . $marca . "\n";*/

        #OBTENEMOS LOS IDs DE LOS PROPIETARIOS ######################################
        $stringPropietarios = "";
        $arrayPropietariosIDs = array();
        $arrayPropietariosNamesSortedASC = array();
        foreach ($propietarios as $propietario) {
            $stringPropietarios .= "'" . $propietario . "',";
        }
        $stringPropietarios = substr($stringPropietarios, 0, -1);
        //DEBUG
        //echo "String de propietarios: " . $stringPropietarios;
        $sqlGetPropietariesIDs = "SELECT id, name
        FROM (
            SELECT propietary.id_propietario AS id, trim(replace(propietary.nombre_completo, '  ', ' ')) AS name
            FROM (
                SELECT id_propietario, (nombre || ' ' || apat || ' ' || amat) AS nombre_completo
                FROM public.propietario
                ORDER BY nombre ASC
            ) AS propietary
        ) AS final
        WHERE name IN ($stringPropietarios)
        ORDER BY name ASC";
        $queryGetPropietariesIDs = pg_query($link, $sqlGetPropietariesIDs);
        while ($row = pg_fetch_assoc($queryGetPropietariesIDs)) {
            array_push($arrayPropietariosIDs, $row['id']);
            array_push($arrayPropietariosNamesSortedASC, $row['name']);
        }
        //DEBUG
        //print_r($arrayPropietariosIDs);
        #############################################################################

        #VALIDAMOS SI SE ESPECIFICO UN ARCHIVO EN EL CAMPO SHAPEFILE
        $haveShapefile = haveShapefile($_FILES['fbShapefile']);

        ##REVISAMOS QUE EL ARCHIVO SHAPEFILE SEA VALIDO
        if ($haveShapefile) {
            $shapefileValid = isExtensionValid($_FILES['fbShapefile'], 'zip');
            if ($shapefileValid) {

                $folderNameAPC = convertToValidPath($nombreAPC);
                $generalPath = "documentacion/F" . $folio . "_" . $folderNameAPC . "/";
                $shapefilePath = $generalPath . "Shapefile/";
                $shapeFileName = "F" . $folio . "_" . $folderNameAPC . "_shapefile.zip";

                if (file_exists($generalPath) || @mkdir($generalPath)) {
                    mkdir($shapefilePath, 0755, true);
                    ##ELIMINAR TODO EL CONTENIDO DEL DIRECTORIO SHAPEFILE
                    $files = glob($shapefilePath . '*');
                    foreach ($files as $file) {
                        if (is_file($file)) {
                            unlink($file);
                        }
                    }
                    $origenShapefile = $_FILES["fbShapefile"]["tmp_name"];
                    $destinoShapefile = $shapefilePath . $shapeFileName;
                    if (@move_uploaded_file($origenShapefile, $destinoShapefile)) {
                        ##DESCOMPRIMIMOS EL SHAPEFILE
                        $zip = new ZipArchive;
                        $res = $zip->open($destinoShapefile);
                        if ($res === true) {
                            $extraido = $zip->extractTo($shapefilePath);
                            $zip->close();
                            if ($extraido === true) {
                                //sendToConsole('log', 'OK, Se extrajo correctamente el archivo shapefile.');
                                ##LEEMOS LA INFORMACION DEL SHAPEFILE CON EL PLUGIN 'PHP SHAPEFILE'
                                $filasEnElShapefile = 0;

                                try {
                                    $arrayWithSHPFiles = glob($shapefilePath . "*.shp");
                                    $fullPathToSHP = $arrayWithSHPFiles[0];
                                    $ShapeFile = new ShapeFile($fullPathToSHP);
                                    while ($record = $ShapeFile->getRecord(ShapeFile::GEOMETRY_WKT)) {
                                        if ($record['dbf']['_deleted']) {
                                            continue;
                                        }
                                        $filasEnElShapefile++;
                                        $geom = $record['shp'];
                                        $dbfUTF8 = $record['dbf'];
                                        $dbfISO8859 = array_map("utf8_decode", $dbfUTF8);
                                    }

                                    ###### DEBUG ENCODING #################
                                    /*echo "\n\n";
                                    print_r($geom);
                                    echo "\n\n";
                                    print_r($dbfISO8859);*/
                                    #######################################

                                    #VERIFICAMOS QUE LA GEOMETRIA SEA UN POLIGONO (SOLO ACEPTAREMOS POLIGONOS O MULTIPOLIGONOS)
                                    if (strpos($geom, 'POLYGON') !== false || strpos($geom, 'MULTIPOLYGON') !== false) {
                                        //sendToConsole('log', 'OK, Si es un poligono');

                                        #VERIFICAMOS QUE LOS PROPIETARIOS COINCIDAN (FORMULARIO Y SHAPEFILE) ##################
                                        //sendToConsole("warn", "VALIDANDO PROPIETARIOS");
                                        $arrayPropietariosShapefile = explode(",", $dbfISO8859['PROPIETARI']);
                                        $countPropietariosShapefile = count($arrayPropietariosShapefile);
                                        $countPropietariosForm = count($propietarios);
                                        if ($countPropietariosForm == $countPropietariosShapefile) {
                                            //sendToConsole('log', 'OK, Los propietarios coinciden en numero');
                                            $countPropietariosMatches = 0;
                                            foreach ($propietarios as $currentPropietarioForm) {
                                                $currentPropietarioForm = trim($currentPropietarioForm);
                                                foreach ($arrayPropietariosShapefile as $currentPropietarioShapefile) {
                                                    $currentPropietarioShapefile = trim($currentPropietarioShapefile);
                                                    if ($currentPropietarioForm == $currentPropietarioShapefile) {
                                                        //sendToConsole("log", "    (match) " . $currentPropietarioForm . "==" . $currentPropietarioShapefile);
                                                        $countPropietariosMatches++;
                                                    } else {
                                                        //sendToConsole("log", "    (x) " . $currentPropietarioForm . "==" . $currentPropietarioShapefile);
                                                    }
                                                }
                                            }
                                            if ($countPropietariosMatches == $countPropietariosShapefile) {
                                                //sendToConsole('log', 'OK, coincidieron todos los propietarios.');
                                                #VERIFICAMOS QUE EL NOMBRE DEL APC COINCIDA (FORMULARIO Y SHAPEFILE)
                                                //sendToConsole("warn", "VALIDANDO NOMBRE DE APC");
                                                $nombreAPCShape = trim($dbfISO8859['NOMBRE_APC']);
                                                //sendToConsole("log", $nombreAPC . "==" . $nombreAPCShape);
                                                if ($nombreAPC == $nombreAPCShape) {
                                                    //sendToConsole("log", "OK, Coinciden los nombres del APC.");
                                                    //INSERT IN DB
                                                    $resultInsertDataToDB = insertDataToDB(
                                                        $link,
                                                        $linkMySQL,
                                                        $arrayPropietariosIDs,
                                                        $folio,
                                                        $nombreAPC,
                                                        $hectareas,
                                                        $hectareasCertificadas,
                                                        $estado,
                                                        $municipio,
                                                        $localidad,
                                                        $fechaIngreso,
                                                        $vegetaciones,
                                                        $arboles,
                                                        $mamiferos,
                                                        $aves,
                                                        $cuerposAgua,
                                                        $tecnico,
                                                        $zona,
                                                        $proyecto,
                                                        $fase,
                                                        $vigenciaAPC,
                                                        $tienePlanManejo,
                                                        $vigenciaPlanManejo,
                                                        $tenencia,
                                                        $observaciones,
                                                        $perteneceAlSello,
                                                        $marca,
                                                        $userLogged,
                                                        $fechaActual
                                                    );
                                                    if ($resultInsertDataToDB) {
                                                        $resultInsertGeomToPostgis = insertGeomToPostgis($link, $arrayPropietariosNamesSortedASC, $nombreAPC, $folio, $geom);
                                                        if ($resultInsertGeomToPostgis) {
                                                            echo "OK, Se insertó correctamente el APC.";
                                                        } else {
                                                            echo "ERROR, No se pudo insertar la geometría del APC en la BD.";
                                                            //sendToConsole("error", "No se pudo insertar la geometría del APC en la BD.");
                                                        }

                                                    } else {
                                                        echo "ERROR, No se pudo insertar la información del APC en la BD.";
                                                        //sendToConsole("error", "No se pudo insertar la información del APC en la BD.");
                                                    }

                                                } else {
                                                    ##ELIMINAR TODO EL CONTENIDO Y LA CARPETA RECIEN CREADA (EVITAR BASURA EN EL SERVIDOR).
                                                    $pathTruncated = substr($generalPath, 0, -1);
                                                    recursiveRemoveDirectory($pathTruncated);

                                                    echo "ERROR, No coinciden los nombres del APC.";
                                                    //sendToConsole("error", "No coinciden los nombres del APC.");
                                                }
                                                ################################################################################################
                                            } else {
                                                echo "ERROR, Los nombres de los propietarios no coinciden.";
                                                //sendToConsole("error", "No coinciden todos los propietarios.");
                                            }
                                        } else {
                                            echo "ERROR, El numero de propietarios es diferente.";
                                        }
                                        ################################################################################################

                                    }

                                } catch (ShapeFileException $e) {
                                    //sendToConsole("error", $e->getCode() . ' (' . $e->getErrorType() . '): ' . $e->getMessage());
                                    $error = $e->getErrorType() . " - " . $e->getMessage();
                                    echo "ERROR," . $error;
                                    exit();
                                }

                            }
                        }

                    } else {
                        echo "ERROR, No se pudo subir el archivo shapefile al servidor.";
                        //sendToConsole("error", "No se pudo subir el archivo shapefile al servidor.");
                    }
                }

            } else {
                echo "ERROR, La extensión del archivo shapefile no es valida.";
                //sendToConsole("error", "La extensión del archivo shapefile no es valida.");
            }

        } else {
            //sendToConsole("warn", "No se cargó ningún shapefile.");
            $resultInsertDataToDB = insertDataToDB(
                $link,
                $linkMySQL,
                $arrayPropietariosIDs,
                $folio,
                $nombreAPC,
                $hectareas,
                $hectareasCertificadas,
                $estado,
                $municipio,
                $localidad,
                $fechaIngreso,
                $vegetaciones,
                $arboles,
                $mamiferos,
                $aves,
                $cuerposAgua,
                $tecnico,
                $zona,
                $proyecto,
                $fase,
                $vigenciaAPC,
                $tienePlanManejo,
                $vigenciaPlanManejo,
                $tenencia,
                $observaciones,
                $perteneceAlSello,
                $marca,
                $userLogged,
                $fechaActual
            );

            if ($resultInsertDataToDB) {
                echo "OK, Se insertó correctamente el APC.";
            } else {
                echo "ERROR, No se pudo insertar la información del APC en la BD.";
                //sendToConsole("error", "No se pudo insertar la información del APC en la BD.");
            }

        }

    } else {
        echo "ERROR,Ingresaste información vacía.";
    }

} else {
    echo "ERROR,Ocurrió un problema con sus credenciales.";
}
