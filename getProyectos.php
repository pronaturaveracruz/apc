<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$zona = $_GET['zona'];

$sql = pg_query($link, "SELECT concentrado_zona_proyecto.id_proyecto, proyecto.nombre_proyecto
	FROM concentrado_zona_proyecto
	LEFT JOIN proyecto ON concentrado_zona_proyecto.id_proyecto = proyecto.id_proyecto
	WHERE id_zona = $zona
	GROUP BY concentrado_zona_proyecto.id_proyecto, proyecto.nombre_proyecto
	ORDER BY MIN(id_czp) ASC");

$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
