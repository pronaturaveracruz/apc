<?php
error_reporting(0);
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$zonas = $_GET['zonas'];

$whereZonas = "";

if ($zonas) {
    $zona = explode(",", $zonas);
    for ($i = 0; $i < count($zona); $i++) {
        if ($i == 0) {
            $whereZonas .= " descripcion_zona = '$zona[$i]'";
        } else {
            $whereZonas .= " OR descripcion_zona = '$zona[$i]'";
        }

    }

    $sql = "SELECT DISTINCT nombre_proyecto
		FROM zonas
		INNER JOIN concentrado_zona_proyecto ON zonas.id_zona=concentrado_zona_proyecto.id_zona
		INNER JOIN proyecto ON concentrado_zona_proyecto.id_proyecto=proyecto.id_proyecto
		--WHERE concentrado_zona_proyecto.id_proyecto != '6' AND concentrado_zona_proyecto.id_proyecto!='7'
		AND
		(" . $whereZonas . ")
		ORDER BY nombre_proyecto ASC";
} else {
    $sql = "SELECT nombre_proyecto FROM proyecto ORDER BY nombre_proyecto ASC";
}

$sql = pg_query($link, $sql);
$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
