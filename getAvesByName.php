<?php
include "../includes/conexion.php";
$link = ConectarseMySQLMegaBD();

$cargadosDatagrid = isset($_POST['cargadosDatagrid']) ? $_POST['cargadosDatagrid'] : '';

if ($cargadosDatagrid == '') {
    $myQuery = "SELECT *
        FROM (
            SELECT id_aves_ecoforestal AS id, nombre_cientifico AS name
            FROM aves_ecoforestal
        ) AS final
        WHERE name != ''
        ORDER BY name ASC";

} else {
    $where = "";
    $arrayCargadosDatagrid = explode(",", $cargadosDatagrid);
    for ($i = 0; $i < count($arrayCargadosDatagrid); $i++) {
        if ($i == 0) {
            $where .= "WHERE name !='" . $arrayCargadosDatagrid[$i] . "'";
        } else {
            $where .= " AND name !='" . $arrayCargadosDatagrid[$i] . "'";
        }
    }
    $myQuery = "SELECT *
        FROM (
           SELECT id_aves_ecoforestal AS id, nombre_cientifico AS name
           FROM aves_ecoforestal
        ) AS final " . $where . "
		ORDER BY name ASC";
}

$sql = mysqli_query($link, $myQuery);
$items = array();
while ($row = mysqli_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
