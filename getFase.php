<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$proyecto = $_GET['proyecto'];

$sql = pg_query($link, "SELECT fase
	FROM concentrado_zona_proyecto
	WHERE id_proyecto = $proyecto
	GROUP BY fase
	ORDER BY fase ASC");

$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
