<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$sql = pg_query($link, "SELECT nom_estado FROM public.ubicacion GROUP BY nom_estado ORDER BY nom_estado ASC");
$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
