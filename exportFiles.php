<?php
error_reporting(0);
date_default_timezone_set("Mexico/General");
session_start();
$usuario_actual = $_SESSION['usuario'];
$fecha_actual = date("Ymd_His");
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

##FUNCIÓN PARA CONVERTIR UN ARRAY TIPO OBJETO A UN ARRAY CONVENCIONAL
function objectToArray($d)
{
    if (is_object($d)) {
        $d = get_object_vars($d); #Gets the properties of the given object with get_object_vars function
    }
    if (is_array($d)) {
        return array_map(__FUNCTION__, $d); #Return array converted to object, Using __FUNCTION__ (Magic constant), for recursive call
    } else {
        return $d; #Return array
    }
}
#FUNCION MULTIBYTE DE UCWORDS
if (!function_exists('mb_ucwords')) {
    function mb_ucwords($str)
    {
        return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
}

#VARIABLES POST
$folio = $_POST['folio'];
$nombre_apc = $_POST['nombre_apc'];
$nombre_completo = $_POST['nombre_completo'];
$hectareas = $_POST['hectareas'];
$estado = $_POST['estado'];
$municipio = $_POST['municipio'];
$localidad = $_POST['localidad'];
$fecha_ingreso = $_POST['fecha_ingreso'];
$estatus = $_POST['estatus'];
$fecha_certificacion = $_POST['fecha_certificacion'];
$numero_sedema = $_POST['numero_sedema'];
$vegetacion = $_POST['vegetacion'];
$especies = $_POST['especies'];
$tecnico = $_POST['tecnico'];
$descripcion_zona = $_POST['descripcion_zona'];
$nombre_proyecto = $_POST['nombre_proyecto'];
$fase = $_POST['fase'];
$cuenta_plan_manejo = $_POST['cuenta_plan_manejo'];
$tenencia = $_POST['tenencia'];
$elementos = $_POST['elementos'];
$sello = $_POST['sello'];
$marca = $_POST['marca'];
$figura_legal = $_POST['figura_legal'];

#CONSULTA GENERAL
$consultaBase = "FROM
		(
			SELECT ok2.id_principal, ok2.figura_legal, trim(replace(ok2.nombre_completo,'  ', ' ')) AS nombre_completo, ok2.folio, ok2.nombre_apc, ok2.hectareas,ok2.hectareas_certificadas,ok2.estado,ok2.municipio,ok2.localidad,ok2.fecha_ingreso,ok2.estatus,ok2.fecha_certificacion, ok2.numero_sedema, ok2.motivo_no_procede, ok2.vegetacion,ok2.observaciones_vegetacion, ok2.especies, ok2.tecnico, ok2.descripcion_zona, ok2.nombre_proyecto,ok2.fase, ok2.vigencia, ok2.proteccion_legal, ok2.cuenta_plan_manejo, ok2.vigencia_plan_manejo, ok2.tenencia, ok2.ruta_doc_ingresados, ok2.ruta_fotografias, ok2.ruta_certificado, ok2.ruta_plan_de_manejo, ok2.organizacion, ok2.org_marca
			FROM
			(
				SELECT ok.id_principal, ok.figura_legal, array_to_string(array_agg(ok.nombre_completo), ',') AS nombre_completo, ok.folio, ok.nombre_apc, ok.hectareas,ok.hectareas_certificadas,ok.estado,ok.municipio,ok.localidad,ok.fecha_ingreso,ok.estatus,ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion,ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.tenencia, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca
				FROM
				(
					SELECT apc_principal.id_principal, figura_legal.figura_legal, trim((propietario.nombre || ' ' || propietario.apat || ' ' || propietario.amat)) AS nombre_completo,apc_principal.folio,apc_principal.nombre_apc,apc_principal.hectareas,apc_principal.hectareas_certificadas,apc_principal.estado,apc_principal.municipio,apc_principal.localidad,apc_principal.fecha_ingreso,apc_principal.estatus,apc_principal.fecha_certificacion, apc_principal.numero_sedema, apc_principal.motivo_no_procede, resultado.vegetacion, apc_principal.observaciones_vegetacion, especies, (tecnico.nombre || ' ' || tecnico.apat || ' ' || tecnico.amat) AS tecnico,descripcion_zona,nombre_proyecto, fase, vigencia,proteccion_legal,cuenta_plan_manejo,vigencia_plan_manejo,tenencia,apc_principal.ruta_doc_ingresados, apc_principal.ruta_fotografias, apc_principal.ruta_certificado, apc_principal.ruta_plan_de_manejo, marca_sello.organizacion, (marca_sello.organizacion || ' - ' || marca_sello.marca) AS org_marca
					FROM
					(
						SELECT resultante.id_apc, resultante.vegetacion, array_to_string(array_agg(categoria || ':' || nombre_cientifico), ',') AS especies
						FROM
						(
							SELECT tv_apcs.id_apc, array_to_string(array_agg(categoria || ':' || tipo_vegetacion), ',') AS vegetacion
							FROM tv_apcs
							JOIN tipo_vegetacion ON tv_apcs.id_tipo_vegetacion=tipo_vegetacion.id_tipo_vegetacion
							GROUP BY tv_apcs.id_apc
							ORDER BY tv_apcs.id_apc
						) AS resultante
						LEFT JOIN objetivo_certificacion ON resultante.id_apc=objetivo_certificacion.id_apc
						GROUP BY resultante.id_apc,resultante.vegetacion
						ORDER BY resultante.id_apc
					) AS resultado
					JOIN apc_principal ON resultado.id_apc=apc_principal.id_principal
					JOIN prop_apcs ON apc_principal.id_principal=prop_apcs.id_apc
					JOIN public.propietario ON prop_apcs.id_propietario=propietario.id_propietario
					LEFT JOIN public.tecnico ON apc_principal.tecnico=tecnico.id_tecnico
					LEFT JOIN concentrado_zona_proyecto ON apc_principal.id_czp=concentrado_zona_proyecto.id_czp
					LEFT JOIN zonas ON concentrado_zona_proyecto.id_zona=zonas.id_zona
					LEFT JOIN proyecto ON concentrado_zona_proyecto.id_proyecto=proyecto.id_proyecto
					LEFT JOIN marca_sello ON apc_principal.id_marca = marca_sello.id_marca
                    LEFT JOIN figura_legal ON apc_principal.id_figura_legal = figura_legal.id_figura_legal
					ORDER BY propietario.nombre ASC
				) AS ok
				GROUP BY ok.folio, ok.figura_legal, ok.id_principal, ok.nombre_apc, ok.hectareas,ok.hectareas_certificadas,ok.estado,ok.municipio,ok.localidad,ok.fecha_ingreso,ok.estatus,ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion,ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.tenencia, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca
			) AS ok2
		)AS ok3
		LEFT JOIN shape_puntos_ccl ON ok3.nombre_apc=shape_puntos_ccl.nombre_apc AND ok3.folio=shape_puntos_ccl.folio AND ok3.nombre_completo=shape_puntos_ccl.propietari
		LEFT JOIN shape_poligonos_ccl_merge_final ON ok3.nombre_apc=shape_poligonos_ccl_merge_final.nombre_apc AND ok3.folio=shape_poligonos_ccl_merge_final.folio AND ok3.nombre_completo=shape_poligonos_ccl_merge_final.propietari
		WHERE ok3.id_principal>0";

#INICIALIZAMOS VARIABLES LOCALES
$filterArray = array();
$filtroActivo = 0;
$where = "";

#VALIDAR SI LAS VARIABLES POST TIENEN ALGO PARA FILTRAR
if ($folio !== '') {
    $folio = explode("|", $folio);
    array_push($filterArray, $folio);
    $filtroActivo++;
}
if ($nombre_apc !== '') {
    $nombre_apc = explode("|", $nombre_apc);
    array_push($filterArray, $nombre_apc);
    $filtroActivo++;
}
if ($nombre_completo !== '') {
    $nombre_completo = explode("|", $nombre_completo);
    array_push($filterArray, $nombre_completo);
    $filtroActivo++;
}
if ($hectareas !== '') {
    $hectareas = explode("|", $hectareas);
    array_push($filterArray, $hectareas);
    $filtroActivo++;
}
if ($estado !== '') {
    $estado = explode("|", $estado);
    array_push($filterArray, $estado);
    $filtroActivo++;
}
if ($municipio !== '') {
    $municipio = explode("|", $municipio);
    array_push($filterArray, $municipio);
    $filtroActivo++;
}
if ($localidad !== '') {
    $localidad = explode("|", $localidad);
    array_push($filterArray, $localidad);
    $filtroActivo++;
}
if ($fecha_ingreso !== '') {
    $fecha_ingreso = explode("|", $fecha_ingreso);
    array_push($filterArray, $fecha_ingreso);
    $filtroActivo++;
}
if ($estatus !== '') {
    $estatus = explode("|", $estatus);
    array_push($filterArray, $estatus);
    $filtroActivo++;
}
if ($fecha_certificacion !== '') {
    $fecha_certificacion = explode("|", $fecha_certificacion);
    array_push($filterArray, $fecha_certificacion);
    $filtroActivo++;
}
if ($numero_sedema !== '') {
    $numero_sedema = explode("|", $numero_sedema);
    array_push($filterArray, $numero_sedema);
    $filtroActivo++;
}
if ($vegetacion !== '') {
    $vegetacion = explode("|", $vegetacion);
    array_push($filterArray, $vegetacion);
    $filtroActivo++;
}
if ($especies !== '') {
    $especies = explode("|", $especies);
    array_push($filterArray, $especies);
    $filtroActivo++;
}
if ($tecnico !== '') {
    $tecnico = explode("|", $tecnico);
    array_push($filterArray, $tecnico);
    $filtroActivo++;
}
if ($descripcion_zona !== '') {
    $descripcion_zona = explode("|", $descripcion_zona);
    array_push($filterArray, $descripcion_zona);
    $filtroActivo++;
}
if ($nombre_proyecto !== '') {
    $nombre_proyecto = explode("|", $nombre_proyecto);
    array_push($filterArray, $nombre_proyecto);
    $filtroActivo++;
}
if ($fase !== '') {
    $fase = explode("|", $fase);
    array_push($filterArray, $fase);
    $filtroActivo++;
}
if ($cuenta_plan_manejo !== '') {
    $cuenta_plan_manejo = explode("|", $cuenta_plan_manejo);
    array_push($filterArray, $cuenta_plan_manejo);
    $filtroActivo++;
}
if ($tenencia !== '') {
    $tenencia = explode("|", $tenencia);
    array_push($filterArray, $tenencia);
    $filtroActivo++;
}
///
if ($sello !== '') {
    $sello = explode("|", $sello);
    array_push($filterArray, $sello);
    $filtroActivo++;
}
if ($marca !== '') {
    $marca = explode("|", $marca);
    array_push($filterArray, $marca);
    $filtroActivo++;
}
if ($figura_legal !== '') {
    $figura_legal = explode("|", $figura_legal);
    array_push($filterArray, $figura_legal);
    $filtroActivo++;
}

if ($filtroActivo > 0) {
    #CAMBIAMOS LOS VALORES DE LAS LLAVES DE NUMERICOS A TEXTO (PARA QUE QUEDEN IGUAL QUE EN 'get_apc.php')
    $filterArray = array_map(function ($tag) {
        return array(
            'field' => $tag[0],
            'op' => $tag[1],
            'value' => $tag[2],
        );
    }, $filterArray);

    $num_filter = count($filterArray);
    for ($i = 0; $i < $num_filter; $i++) {
        $filterField = $filterArray[$i]['field'];
        $filterOperator = $filterArray[$i]['op'];
        $filterValue = $filterArray[$i]['value'];

        #EXCEPCION PARA EL SELLO DE BIODIVERSIDAD
        if ($filterField == 'organizacion') {
            if ($filterValue == 'Si') {
                $filterValue = '';
                $where .= " AND ok3." . $filterField . " <> '" . $filterValue . "'";
            } else if ($filterValue == 'No') {
                $filterValue = '';
                $where .= " AND ok3." . $filterField . " IS NULL";
            }
        } else {

            $mValues = explode(",", $filterValue);
            $countmValues = count($mValues);
            for ($j = 0; $j < $countmValues; $j++) {
                $filterValue = $mValues[$j];
                if ($countmValues > 1) {
                    if ($j === 0) {
                        $where .= " AND(  (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))";
                    } else if ($j !== 0 && $j !== ($countmValues - 1)) {
                        $where .= " OR (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))";
                    } else if ($j === ($countmValues - 1)) {
                        $where .= " OR (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))  )";
                    }

                } else {
                    switch ($filterOperator) {
                        case 'contains':
                            $where .= " AND ( lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*') )";
                            break;
                        case 'equal';
                            $where .= " AND ok3." . $filterField . " = '" . $filterValue . "'";
                            break;
                        case 'notequal';
                            $where .= " AND ok3." . $filterField . " <> '" . $filterValue . "'";
                            break;
                        case 'beginwith';
                            $where .= " AND ok3." . $filterField . " LIKE '" . $filterValue . "%'";
                            break;
                        case 'endwith';
                            $where .= " AND ok3." . $filterField . " LIKE '%" . $filterValue . "'";
                            break;
                        case 'less';
                            $where .= " AND ok3." . $filterField . " < '" . $filterValue . "'";
                            break;
                        case 'lessorequal';
                            $where .= " AND ok3." . $filterField . " <= '" . $filterValue . "'";
                            break;
                        case 'greater';
                            $where .= " AND ok3." . $filterField . " > '" . $filterValue . "'";
                            break;
                        case 'greaterorequal';
                            $where .= " AND ok3." . $filterField . " >= '" . $filterValue . "'";
                            break;
                    }
                }
            }
        }
    }

    ##INSERTAR AQUI LAS CONSULTAS

    #1) CONDICIONAL PARA OBTENER LISTA DE ELEMENTOS DESEADOS
    if ($elementos === 'documentacion') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_doc_ingresados ";
    } else if ($elementos === 'fotos') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_fotografias ";
    } else if ($elementos === 'certificados') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_certificado ";
    } else if ($elementos === 'dfc') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_doc_ingresados, ok3.ruta_fotografias, ok3.ruta_certificado ";
    }

    $rs = pg_query($link, $mySelect . $consultaBase . $where);
    $cuantos = pg_affected_rows($rs);

    #2) DEFINIMOS LA RUTA Y VERIFICAMOS SI YA EXISTE LA CARPETA, SINO LA CREAMOS CON MKDIR
    $ruta_actual = "exportFiles/" . $usuario_actual . "/";
    if (file_exists($ruta_actual)) {
        #ELIMINAR TODOS LOS ARCHIVOS DEL DIRECTORIO 'exportFiles/[usuario]/'
        $files = glob($ruta_actual . '*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    } else {
        mkdir($ruta_actual);
        $oldmask = umask(0);
        chmod($ruta_actual, 0757);
    }

    #3) CREAMOS EL ARCHIVO ZIP PARA IR AGREGANDO ELEMENTOS
    switch ($elementos) {
        case 'documentacion':
            $archivoZip = $ruta_actual . $cuantos . "apcSoloDocumentacionSISAPC_" . $fecha_actual . ".zip";
            break;
        case 'fotos':
            $archivoZip = $ruta_actual . $cuantos . "apcSoloFotosSISAPC_" . $fecha_actual . ".zip";
            break;
        case 'certificados':
            $archivoZip = $ruta_actual . $cuantos . "apcSoloCertificadosSISAPC_" . $fecha_actual . ".zip";
            break;
        case 'dfc':
            $archivoZip = $ruta_actual . $cuantos . "apcDocumentacionFotosCertificadosSISAPC_" . $fecha_actual . ".zip";
            break;
        default:
            break;
    }
    $zip = new ZipArchive;
    $zip->open($archivoZip, ZipArchive::CREATE);

    #4) ITERAMOS LA CONSULTA Y FORMATEAMOS LOS NOMBRES PARA CRAR DINAMICAMENTE LAS CARPETAS
    while ($row = pg_fetch_assoc($rs)) {
        $folio = $row['folio'];
        $nombre_apc = $row['nombre_apc'];
        #FORMATEAMOS
        $nombre_apc = mb_strtolower($nombre_apc, 'UTF-8');
        $nombre_apc = mb_ucwords($nombre_apc);
        $nombre_apc = str_replace(' ', '', $nombre_apc);
        ##REEMPLAZAMOS CARACTERES NO PERMITIDOS POR GUIONES BAJOS
        $nombre_apc = str_replace('.', '_', $nombre_apc);
        $nombre_apc = str_replace('/', '_', $nombre_apc);
        $nombre_apc = str_replace('-', '_', $nombre_apc);
        $nombre_apc = str_replace('?', '_', $nombre_apc);
        $nombre_apc = str_replace('*', '_', $nombre_apc);
        $nombre_apc = str_replace('+', '_', $nombre_apc);
        $nombre_apc = str_replace('%', '_', $nombre_apc);
        $nombre_apc = str_replace(':', '_', $nombre_apc);
        $nombre_apc = str_replace('<', '_', $nombre_apc);
        $nombre_apc = str_replace('>', '_', $nombre_apc);
        $nombre_apc = str_replace('|', '_', $nombre_apc);
        $nombre_apc = str_replace(',', '_', $nombre_apc);
        ##REEMPLAZAMOS LA LETRA Ñ Y LETRAS ACENTUADAS PARA NO TENER PROBLEMAS EN LA RUTA
        $nombre_apc = str_replace('ñ', 'n', $nombre_apc);
        $nombre_apc = str_replace('á', 'a', $nombre_apc);
        $nombre_apc = str_replace('é', 'e', $nombre_apc);
        $nombre_apc = str_replace('í', 'i', $nombre_apc);
        $nombre_apc = str_replace('ó', 'o', $nombre_apc);
        $nombre_apc = str_replace('ú', 'u', $nombre_apc);
        ########
        $carpetaPrincipal = "F" . $folio . "_" . $nombre_apc;
        $zip->addEmptyDir($carpetaPrincipal);

        if ($elementos === 'documentacion') {
            if ($row['ruta_doc_ingresados'] != '') {
                $zip->addFile($row['ruta_doc_ingresados'], $carpetaPrincipal . "/Documentos_ingresados/" . $carpetaPrincipal . "_doc.pdf");
            }

        }
        if ($elementos === 'fotos') {
            if ($row['ruta_fotografias'] != '') {
                $zip->addFile($row['ruta_fotografias'], $carpetaPrincipal . "/Fotos/" . $carpetaPrincipal . "_fotos.docx");
            }

        }
        if ($elementos === 'certificados') {
            if ($row['ruta_certificado'] != '') {
                $zip->addFile($row['ruta_certificado'], $carpetaPrincipal . "/Certificado/" . $carpetaPrincipal . "_cer.pdf");
            }

        }
        if ($elementos === 'dfc') {
            if ($row['ruta_doc_ingresados'] != '') {
                $zip->addFile($row['ruta_doc_ingresados'], $carpetaPrincipal . "/Documentos_ingresados/" . $carpetaPrincipal . "_doc.pdf");
            }

            if ($row['ruta_fotografias'] != '') {
                $zip->addFile($row['ruta_fotografias'], $carpetaPrincipal . "/Fotos/" . $carpetaPrincipal . "_fotos.docx");
            }

            if ($row['ruta_certificado'] != '') {
                $zip->addFile($row['ruta_certificado'], $carpetaPrincipal . "/Certificado/" . $carpetaPrincipal . "_cer.pdf");
            }

        }
    }
    $zip->close();

    if ($zip) {
        if (!$archivoZip) {
            echo "error";
        } else {
            $nombreArchivo = str_replace($ruta_actual, "", $archivoZip);
            echo "ok," . $archivoZip . "," . $nombreArchivo;
        }
    }
    #FIN
} else #SIN FILTROS
{
    #1) CONDICIONAL PARA OBTENER LISTA DE ELEMENTOS DESEADOS
    if ($elementos === 'documentacion') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_doc_ingresados ";
    } else if ($elementos === 'fotos') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_fotografias ";
    } else if ($elementos === 'certificados') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_certificado ";
    } else if ($elementos === 'dfc') {
        $mySelect = "SELECT ok3.folio, ok3.nombre_apc, ok3.ruta_doc_ingresados, ok3.ruta_fotografias, ok3.ruta_certificado ";
    }

    $rs = pg_query($link, $mySelect . $consultaBase);
    $cuantos = pg_affected_rows($rs);

    #2) DEFINIMOS LA RUTA Y VERIFICAMOS SI YA EXISTE LA CARPETA, SINO LA CREAMOS CON MKDIR
    $ruta_actual = "exportFiles/" . $usuario_actual . "/";
    if (file_exists($ruta_actual)) {
        #ELIMINAR TODOS LOS ARCHIVOS DEL DIRECTORIO 'exportFiles/[usuario]/'
        $files = glob($ruta_actual . '*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    } else {
        mkdir($ruta_actual);
        $oldmask = umask(0);
        chmod($ruta_actual, 0757);
    }

    #3) CREAMOS EL ARCHIVO ZIP PARA IR AGREGANDO ELEMENTOS
    switch ($elementos) {
        case 'documentacion':
            $archivoZip = $ruta_actual . $cuantos . "apcSoloDocumentacionSISAPC_" . $fecha_actual . ".zip";
            break;
        case 'fotos':
            $archivoZip = $ruta_actual . $cuantos . "apcSoloFotosSISAPC_" . $fecha_actual . ".zip";
            break;
        case 'certificados':
            $archivoZip = $ruta_actual . $cuantos . "apcSoloCertificadosSISAPC_" . $fecha_actual . ".zip";
            break;
        case 'dfc':
            $archivoZip = $ruta_actual . $cuantos . "apcDocumentacionFotosCertificadosSISAPC_" . $fecha_actual . ".zip";
            break;
        default:
            break;
    }
    $zip = new ZipArchive;
    $zip->open($archivoZip, ZipArchive::CREATE);

    #4) ITERAMOS LA CONSULTA Y FORMATEAMOS LOS NOMBRES PARA CRAR DINAMICAMENTE LAS CARPETAS
    while ($row = pg_fetch_assoc($rs)) {
        $folio = $row['folio'];
        $nombre_apc = $row['nombre_apc'];
        #FORMATEAMOS
        $nombre_apc = mb_strtolower($nombre_apc, 'UTF-8');
        $nombre_apc = mb_ucwords($nombre_apc);
        $nombre_apc = str_replace(' ', '', $nombre_apc);
        ##REEMPLAZAMOS CARACTERES NO PERMITIDOS POR GUIONES BAJOS
        $nombre_apc = str_replace('.', '_', $nombre_apc);
        $nombre_apc = str_replace('/', '_', $nombre_apc);
        $nombre_apc = str_replace('-', '_', $nombre_apc);
        $nombre_apc = str_replace('?', '_', $nombre_apc);
        $nombre_apc = str_replace('*', '_', $nombre_apc);
        $nombre_apc = str_replace('+', '_', $nombre_apc);
        $nombre_apc = str_replace('%', '_', $nombre_apc);
        $nombre_apc = str_replace(':', '_', $nombre_apc);
        $nombre_apc = str_replace('<', '_', $nombre_apc);
        $nombre_apc = str_replace('>', '_', $nombre_apc);
        $nombre_apc = str_replace('|', '_', $nombre_apc);
        $nombre_apc = str_replace(',', '_', $nombre_apc);
        ##REEMPLAZAMOS LA LETRA Ñ Y LETRAS ACENTUADAS PARA NO TENER PROBLEMAS EN LA RUTA
        $nombre_apc = str_replace('ñ', 'n', $nombre_apc);
        $nombre_apc = str_replace('á', 'a', $nombre_apc);
        $nombre_apc = str_replace('é', 'e', $nombre_apc);
        $nombre_apc = str_replace('í', 'i', $nombre_apc);
        $nombre_apc = str_replace('ó', 'o', $nombre_apc);
        $nombre_apc = str_replace('ú', 'u', $nombre_apc);
        ########
        $carpetaPrincipal = "F" . $folio . "_" . $nombre_apc;
        $zip->addEmptyDir($carpetaPrincipal);

        if ($elementos === 'documentacion') {
            if ($row['ruta_doc_ingresados'] != '') {
                $zip->addFile($row['ruta_doc_ingresados'], $carpetaPrincipal . "/Documentos_ingresados/" . $carpetaPrincipal . "_doc.pdf");
            }

        }
        if ($elementos === 'fotos') {
            if ($row['ruta_fotografias'] != '') {
                $zip->addFile($row['ruta_fotografias'], $carpetaPrincipal . "/Fotos/" . $carpetaPrincipal . "_fotos.docx");
            }

        }
        if ($elementos === 'certificados') {
            if ($row['ruta_certificado'] != '') {
                $zip->addFile($row['ruta_certificado'], $carpetaPrincipal . "/Certificado/" . $carpetaPrincipal . "_cer.pdf");
            }

        }
        if ($elementos === 'dfc') {
            if ($row['ruta_doc_ingresados'] != '') {
                $zip->addFile($row['ruta_doc_ingresados'], $carpetaPrincipal . "/Documentos_ingresados/" . $carpetaPrincipal . "_doc.pdf");
            }

            if ($row['ruta_fotografias'] != '') {
                $zip->addFile($row['ruta_fotografias'], $carpetaPrincipal . "/Fotos/" . $carpetaPrincipal . "_fotos.docx");
            }

            if ($row['ruta_certificado'] != '') {
                $zip->addFile($row['ruta_certificado'], $carpetaPrincipal . "/Certificado/" . $carpetaPrincipal . "_cer.pdf");
            }

        }
    }
    $zip->close();

    if ($zip) {
        if (!$archivoZip) {
            echo "error";
        } else {
            $nombreArchivo = str_replace($ruta_actual, "", $archivoZip);
            echo "ok," . $archivoZip . "," . $nombreArchivo;
        }
    }
}
