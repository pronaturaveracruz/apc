<?php
//error_reporting(0);
date_default_timezone_set("Mexico/General");
$fecha_actual = date("Ymd_His");
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();
##FUNCIÓN PARA CONVERTIR UN ARRAY TIPO OBJETO A UN ARRAY CONVENCIONAL
function objectToArray($d)
{
    if (is_object($d)) {
        $d = get_object_vars($d); #Gets the properties of the given object with get_object_vars function
    }
    if (is_array($d)) {
        return array_map(__FUNCTION__, $d); #Return array converted to object, Using __FUNCTION__ (Magic constant), for recursive call
    } else {
        return $d; #Return array
    }
}
##FUNCIÓN PARA CREAR UN ARCHIVO ZIP
function create_zip($files = array(), $destination = '', $overwrite = false)
{
    if (file_exists($destination) && !$overwrite) {return false;}
    $valid_files = array();
    if (is_array($files)) {
        foreach ($files as $file) {
            if (file_exists($file)) {
                $valid_files[] = $file;
            }

        }
    }
    if (count($valid_files)) {
        $zip = new ZipArchive();
        if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }

        foreach ($valid_files as $file) {
            $file_wo_path = str_replace("exportToShapefile/", "", $file);
            $zip->addFile($file, $file_wo_path);
        }
        $zip->close();
        return file_exists($destination);
    } else {
        return false;
    }
}

#VARIABLES POST
$folio = $_POST['folio'];
$nombre_apc = $_POST['nombre_apc'];
$nombre_completo = $_POST['nombre_completo'];
$hectareas = $_POST['hectareas'];
$estado = $_POST['estado'];
$municipio = $_POST['municipio'];
$localidad = $_POST['localidad'];
$fecha_ingreso = $_POST['fecha_ingreso'];
$estatus = $_POST['estatus'];
$fecha_certificacion = $_POST['fecha_certificacion'];
$numero_sedema = $_POST['numero_sedema'];
$vegetacion = $_POST['vegetacion'];
$especies = $_POST['especies'];
$tecnico = $_POST['tecnico'];
$descripcion_zona = $_POST['descripcion_zona'];
$nombre_proyecto = $_POST['nombre_proyecto'];
$fase = $_POST['fase'];
$cuenta_plan_manejo = $_POST['cuenta_plan_manejo'];
$tenencia = $_POST['tenencia'];
$proyeccion = $_POST['proyeccion'];
$sello = $_POST['sello'];
$marca = $_POST['marca'];
$figura_legal = $_POST['figura_legal'];

##CONSULTA GENERAL
$select = "SELECT ok3.figura_legal, ok3.id_principal, ok3.nombre_completo, ok3.folio, ok3.nombre_apc, ok3.hectareas, ok3.hectareas_certificadas, ok3.estado, ok3.municipio, ok3.localidad, ok3.fecha_ingreso, ok3.estatus, ok3.fecha_certificacion, ok3.numero_sedema, ok3.motivo_no_procede, ok3.vegetacion, ok3.observaciones_vegetacion, ok3.especies, ok3.tecnico, ok3.descripcion_zona, ok3.nombre_proyecto, ok3.fase, ok3.vigencia, ok3.proteccion_legal, ok3.cuenta_plan_manejo, ok3.vigencia_plan_manejo, ok3.tenencia, /*ok3.organizacion,*/ ok3.org_marca ";
$selectCount = "SELECT COUNT(*) ";
$consultaBase = "FROM( SELECT ok2.id_principal, ok2.figura_legal, trim(replace(ok2.nombre_completo,'  ', ' ')) AS nombre_completo, ok2.folio, ok2.nombre_apc, ok2.hectareas, ok2.hectareas_certificadas, ok2.estado, ok2.municipio, ok2.localidad, ok2.fecha_ingreso, ok2.estatus, ok2.fecha_certificacion, ok2.numero_sedema, ok2.motivo_no_procede, ok2.vegetacion, ok2.observaciones_vegetacion, ok2.especies, ok2.tecnico, ok2.descripcion_zona, ok2.nombre_proyecto, ok2.fase, ok2.vigencia, ok2.proteccion_legal, ok2.cuenta_plan_manejo, ok2.vigencia_plan_manejo, ok2.tenencia, ok2.ruta_doc_ingresados, ok2.ruta_fotografias, ok2.ruta_certificado, ok2.ruta_plan_de_manejo, ok2.organizacion, ok2.org_marca FROM ( SELECT ok.id_principal, ok.figura_legal, array_to_string(array_agg(ok.nombre_completo), ',') AS nombre_completo, ok.folio, ok.nombre_apc, ok.hectareas, ok.hectareas_certificadas, ok.estado, ok.municipio, ok.localidad, ok.fecha_ingreso, ok.estatus, ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion, ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.tenencia, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca FROM ( SELECT apc_principal.id_principal, figura_legal.figura_legal, trim((propietario.nombre || ' ' || propietario.apat || ' ' || propietario.amat)) AS nombre_completo, apc_principal.folio, apc_principal.nombre_apc, apc_principal.hectareas, apc_principal.hectareas_certificadas, apc_principal.estado, apc_principal.municipio, apc_principal.localidad, apc_principal.fecha_ingreso, apc_principal.estatus, apc_principal.fecha_certificacion, apc_principal.numero_sedema, apc_principal.motivo_no_procede, resultado.vegetacion, apc_principal.observaciones_vegetacion, especies, (tecnico.nombre || ' ' || tecnico.apat || ' ' || tecnico.amat) AS tecnico, descripcion_zona, nombre_proyecto, fase, vigencia, proteccion_legal, cuenta_plan_manejo, vigencia_plan_manejo, tenencia, apc_principal.ruta_doc_ingresados, apc_principal.ruta_fotografias, apc_principal.ruta_certificado, apc_principal.ruta_plan_de_manejo, marca_sello.organizacion, (marca_sello.organizacion || ' - ' || marca_sello.marca) AS org_marca FROM ( SELECT resultante.id_apc, resultante.vegetacion, array_to_string(array_agg(categoria || ':' || nombre_cientifico), ',') AS especies FROM ( SELECT tv_apcs.id_apc, array_to_string(array_agg(categoria || ':' || tipo_vegetacion), ',') AS vegetacion FROM tv_apcs JOIN tipo_vegetacion ON tv_apcs.id_tipo_vegetacion = tipo_vegetacion.id_tipo_vegetacion GROUP BY tv_apcs.id_apc ORDER BY tv_apcs.id_apc) AS resultante LEFT JOIN objetivo_certificacion ON resultante.id_apc = objetivo_certificacion.id_apc GROUP BY resultante.id_apc, resultante.vegetacion ORDER BY resultante.id_apc ) AS resultado JOIN apc_principal ON resultado.id_apc = apc_principal.id_principal JOIN prop_apcs ON apc_principal.id_principal = prop_apcs.id_apc JOIN public.propietario ON prop_apcs.id_propietario = propietario.id_propietario LEFT JOIN public.tecnico ON apc_principal.tecnico = tecnico.id_tecnico LEFT JOIN concentrado_zona_proyecto ON apc_principal.id_czp = concentrado_zona_proyecto.id_czp LEFT JOIN zonas ON concentrado_zona_proyecto.id_zona = zonas.id_zona LEFT JOIN proyecto ON concentrado_zona_proyecto.id_proyecto = proyecto.id_proyecto LEFT JOIN marca_sello ON apc_principal.id_marca = marca_sello.id_marca LEFT JOIN figura_legal ON apc_principal.id_figura_legal = figura_legal.id_figura_legal ORDER BY propietario.nombre ASC ) AS ok GROUP BY ok.folio, ok.figura_legal, ok.id_principal, ok.nombre_apc, ok.hectareas, ok.hectareas_certificadas, ok.estado, ok.municipio, ok.localidad, ok.fecha_ingreso, ok.estatus, ok.fecha_certificacion, ok.numero_sedema, ok.motivo_no_procede, ok.vegetacion, ok.observaciones_vegetacion, ok.especies, ok.tecnico, ok.descripcion_zona, ok.nombre_proyecto, ok.fase, ok.vigencia, ok.proteccion_legal, ok.cuenta_plan_manejo, ok.vigencia_plan_manejo, ok.tenencia, ok.ruta_doc_ingresados, ok.ruta_fotografias, ok.ruta_certificado, ok.ruta_plan_de_manejo, ok.organizacion, ok.org_marca ) AS ok2 )AS ok3 ";
$innerJoinPoints = "INNER JOIN shape_puntos_ccl ON ok3.nombre_apc = shape_puntos_ccl.nombre_apc AND ok3.folio = shape_puntos_ccl.folio AND ok3.nombre_completo = shape_puntos_ccl.propietari WHERE ok3.id_principal > 0 ";
$innerJoinPolygons = "INNER JOIN shape_poligonos_ccl_merge_final ON ok3.nombre_apc=shape_poligonos_ccl_merge_final.nombre_apc AND ok3.folio=shape_poligonos_ccl_merge_final.folio AND ok3.nombre_completo=shape_poligonos_ccl_merge_final.propietari WHERE ok3.id_principal > 0 ";
#########################

#INICIALIZAMOS VARIABLES LOCALES
$filterArray = array();
$filtroActivo = 0;
$where = "";

#PARAMETROS DEL SERVIDOR
$ogr2ogr = "ogr2ogr";
$host = HOST_PG;
$user = USER_PG;
$frase = PASS_PG;
$encoding = "export PGCLIENTENCODING=LATIN1;";
##1) DEFINIMOS LOS NOMBRES DE LOS ARCHIVOS
$nombre_archivo_puntos = "shape_puntos_" . $fecha_actual . "_" . $proyeccion;
$nombre_archivo_poligonos = "shape_poligonos_" . $fecha_actual . "_" . $proyeccion;
##2) DESTINO DE TODOS LOS ARCHIVOS SHAPEFILE (.shp, .shx, .prj, etc.)
$destino_shapefile_puntos = PATH_FOR_SHAPEFILES . $nombre_archivo_puntos;
$destino_shapefile_poligonos = PATH_FOR_SHAPEFILES . $nombre_archivo_poligonos;

#VALIDAR SI LAS VARIABLES POST TIENEN ALGO PARA FILTRAR
if ($folio !== '') {
    $folio = explode("|", $folio);
    array_push($filterArray, $folio);
    $filtroActivo++;
}
if ($nombre_apc !== '') {
    $nombre_apc = explode("|", $nombre_apc);
    array_push($filterArray, $nombre_apc);
    $filtroActivo++;
}
if ($nombre_completo !== '') {
    $nombre_completo = explode("|", $nombre_completo);
    array_push($filterArray, $nombre_completo);
    $filtroActivo++;
}
if ($hectareas !== '') {
    $hectareas = explode("|", $hectareas);
    array_push($filterArray, $hectareas);
    $filtroActivo++;
}
if ($estado !== '') {
    $estado = explode("|", $estado);
    array_push($filterArray, $estado);
    $filtroActivo++;
}
if ($municipio !== '') {
    $municipio = explode("|", $municipio);
    array_push($filterArray, $municipio);
    $filtroActivo++;
}
if ($localidad !== '') {
    $localidad = explode("|", $localidad);
    array_push($filterArray, $localidad);
    $filtroActivo++;
}
if ($fecha_ingreso !== '') {
    $fecha_ingreso = explode("|", $fecha_ingreso);
    array_push($filterArray, $fecha_ingreso);
    $filtroActivo++;
}
if ($estatus !== '') {
    $estatus = explode("|", $estatus);
    array_push($filterArray, $estatus);
    $filtroActivo++;
}
if ($fecha_certificacion !== '') {
    $fecha_certificacion = explode("|", $fecha_certificacion);
    array_push($filterArray, $fecha_certificacion);
    $filtroActivo++;
}
if ($numero_sedema !== '') {
    $numero_sedema = explode("|", $numero_sedema);
    array_push($filterArray, $numero_sedema);
    $filtroActivo++;
}
if ($vegetacion !== '') {
    $vegetacion = explode("|", $vegetacion);
    array_push($filterArray, $vegetacion);
    $filtroActivo++;
}
if ($especies !== '') {
    $especies = explode("|", $especies);
    array_push($filterArray, $especies);
    $filtroActivo++;
}
if ($tecnico !== '') {
    $tecnico = explode("|", $tecnico);
    array_push($filterArray, $tecnico);
    $filtroActivo++;
}
if ($descripcion_zona !== '') {
    $descripcion_zona = explode("|", $descripcion_zona);
    array_push($filterArray, $descripcion_zona);
    $filtroActivo++;
}
if ($nombre_proyecto !== '') {
    $nombre_proyecto = explode("|", $nombre_proyecto);
    array_push($filterArray, $nombre_proyecto);
    $filtroActivo++;
}
if ($fase !== '') {
    $fase = explode("|", $fase);
    array_push($filterArray, $fase);
    $filtroActivo++;
}
if ($cuenta_plan_manejo !== '') {
    $cuenta_plan_manejo = explode("|", $cuenta_plan_manejo);
    array_push($filterArray, $cuenta_plan_manejo);
    $filtroActivo++;
}
if ($tenencia !== '') {
    $tenencia = explode("|", $tenencia);
    array_push($filterArray, $tenencia);
    $filtroActivo++;
}
////
if ($sello !== '') {
    $sello = explode("|", $sello);
    array_push($filterArray, $sello);
    $filtroActivo++;
}
if ($marca !== '') {
    $marca = explode("|", $marca);
    array_push($filterArray, $marca);
    $filtroActivo++;
}
if ($figura_legal !== '') {
    $figura_legal = explode("|", $figura_legal);
    array_push($filterArray, $figura_legal);
    $filtroActivo++;
}

if ($filtroActivo > 0) {
    #CAMBIAMOS LOS VALORES DE LAS LLAVES DE NUMERICOS A TEXTO (PARA QUE QUEDEN IGUAL QUE EN 'get_apc.php')
    $filterArray = array_map(function ($tag) {
        return array(
            'field' => $tag[0],
            'op' => $tag[1],
            'value' => $tag[2],
        );
    }, $filterArray);

    $num_filter = count($filterArray);
    for ($i = 0; $i < $num_filter; $i++) {
        $filterField = $filterArray[$i]['field'];
        $filterOperator = $filterArray[$i]['op'];
        $filterValue = $filterArray[$i]['value'];

        #EXCEPCION PARA EL SELLO DE BIODIVERSIDAD
        if ($filterField == 'organizacion') {
            if ($filterValue == 'Si') {
                $filterValue = '';
                $where .= " AND ok3." . $filterField . " <> '" . $filterValue . "'";
            } else if ($filterValue == 'No') {
                $filterValue = '';
                $where .= " AND ok3." . $filterField . " IS NULL";
            }
        } else {
            $mValues = explode(",", $filterValue);
            $countmValues = count($mValues);

            for ($j = 0; $j < $countmValues; $j++) {
                $filterValue = $mValues[$j];
                if ($countmValues > 1) {
                    if ($j === 0) {
                        $where .= " AND(  (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))";
                    } else if ($j !== 0 && $j !== ($countmValues - 1)) {
                        $where .= " OR (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))";
                    } else if ($j === ($countmValues - 1)) {
                        $where .= " OR (lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*'))  )";
                    }

                } else {
                    switch ($filterOperator) {
                        case 'contains':
                            $where .= " AND ( lower(unaccent(ok3." . $filterField . ")) LIKE lower('%$filterValue%') OR (ok3." . $filterField . " ~* '.*$filterValue.*') )";
                            break;
                        case 'equal';
                            $where .= " AND ok3." . $filterField . " = '" . $filterValue . "'";
                            break;
                        case 'notequal';
                            $where .= " AND ok3." . $filterField . " <> '" . $filterValue . "'";
                            break;
                        case 'beginwith';
                            $where .= " AND ok3." . $filterField . " LIKE '" . $filterValue . "%'";
                            break;
                        case 'endwith';
                            $where .= " AND ok3." . $filterField . " LIKE '%" . $filterValue . "'";
                            break;
                        case 'less';
                            $where .= " AND ok3." . $filterField . " < '" . $filterValue . "'";
                            break;
                        case 'lessorequal';
                            $where .= " AND ok3." . $filterField . " <= '" . $filterValue . "'";
                            break;
                        case 'greater';
                            $where .= " AND ok3." . $filterField . " > '" . $filterValue . "'";
                            break;
                        case 'greaterorequal';
                            $where .= " AND ok3." . $filterField . " >= '" . $filterValue . "'";
                            break;
                    }
                }
            }

        }
    }

    ##INSERTAR AQUI LAS CONSULTAS
    #1) EJECUTAR CONSULTA COMUN CON COUNT(*) PARA SABER SI EL RESULTADO ES 0 Y NO EJECUTAR PARA EVITAR GENERAR UN SHAPE DAÑADO
    $countElementsPoints = pg_query($link, $selectCount . $consultaBase . $innerJoinPoints . $where);
    $countElementsPolygons = pg_query($link, $selectCount . $consultaBase . $innerJoinPolygons . $where);
    $auxContinuar = 0;
    $cuantos_puntos = 0;
    $cuantos_poligonos = 0;
    while ($row = pg_fetch_row($countElementsPoints)) {
        $cuantos_puntos = $row[0];
        if ($cuantos_puntos !== '0') {
            $auxContinuar += 1;
        }

    }

    while ($row = pg_fetch_row($countElementsPolygons)) {
        $cuantos_poligonos = $row[0];
        if ($cuantos_poligonos !== '0') {
            $auxContinuar += 2;
        }

    }

    if ($cuantos_puntos > 0 && $cuantos_poligonos > 0) {
        $tipogeom = 'ambos';
    } else if ($cuantos_puntos > 0) {
        $tipogeom = 'puntos';
    } else if ($cuantos_poligonos > 0) {
        $tipogeom = 'poligonos';
    } else if ($cuantos_puntos == 0 && $cuantos_poligonos == 0) {
        $tipogeom = 'ceros';
    }

    #CCL
    if ($proyeccion === 'ccl') {
        $consulta_puntos = $select . ", shape_puntos_ccl.geom AS geom_point " . $consultaBase . $innerJoinPoints . $where;
        $consulta_poligonos = $select . ", shape_poligonos_ccl_merge_final.geom AS geom_poly " . $consultaBase . $innerJoinPolygons . $where;
    }
    #UTM14
    if ($proyeccion === 'utm14') {
        $consulta_puntos = $select . ", ST_Transform(shape_puntos_ccl.geom, 32614) AS geom_point " . $consultaBase . $innerJoinPoints . $where;
        $consulta_poligonos = $select . ", ST_Transform(shape_poligonos_ccl_merge_final.geom, 32614) AS geom_poly " . $consultaBase . $innerJoinPolygons . $where;
    }
    #UTM15
    if ($proyeccion === 'utm15') {
        $consulta_puntos = $select . ", ST_Transform(shape_puntos_ccl.geom, 32615) AS geom_point " . $consultaBase . $innerJoinPoints . $where;
        $consulta_poligonos = $select . ", ST_Transform(shape_poligonos_ccl_merge_final.geom, 32615) AS geom_poly " . $consultaBase . $innerJoinPolygons . $where;
    }

    #ELIMINAR TODOS LOS ARCHIVOS DEL DIRECTORIO 'exportToShapefile/'
    $files = glob('exportToShapefile/*');
    foreach ($files as $file) {
        if (is_file($file)) {
            unlink($file);
        }

    }

    ###DEBUG##############
    //POLIGONOS
    //print_r($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_poligonos . '.shp PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');
    //PUNTOS
    //print_r($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_puntos . '.shp PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
    ######################

    #GENERAMOS EL SHAPEFILE CON LAS HERRAMIENTAS ESPACIALES DE POSTGIS Y OGR
    #SE CAMBIO 'pgsql2shp' por 'ogr2ogr' para soportar acentos [ Si en un futuro da problemas de acentos usar al final del comando: -sql "SELECT * FROM..." -overwrite -lco ENCODING=ISO8859-1 ]
    if ($tipogeom == 'puntos') {
        shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_puntos . '.shp PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
        shell_exec($ogr2ogr . ' -f KML ' . $destino_shapefile_puntos . '.kml PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
    } else if ($tipogeom == 'poligonos') {
        shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_poligonos . '.shp PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');
        shell_exec($ogr2ogr . ' -f KML ' . $destino_shapefile_poligonos . '.kml PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');
    } else if ($tipogeom == 'ambos') {
        shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_puntos . '.shp PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
        shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_poligonos . '.shp PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');
        shell_exec($ogr2ogr . ' -f KML ' . $destino_shapefile_puntos . '.kml PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
        shell_exec($ogr2ogr . ' -f KML ' . $destino_shapefile_poligonos . '.kml PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');
    } else if ($tipogeom == 'ceros') {
        echo "error, Los registros seleccionados no tienen shapefile cargado aún.";
    }

    #CREAR EL ARCHIVO ZIP
    if ($tipogeom != 'ceros') {
        #1) AGREGAMOS A UN ARREGLO LA LISTA DE ARCHIVOS
        $arreglo_archivos = array();
        foreach (glob('exportToShapefile/' . $nombre_archivo_puntos . ".*") as $nombre_fichero) {
            array_push($arreglo_archivos, $nombre_fichero);
        }
        foreach (glob('exportToShapefile/' . $nombre_archivo_poligonos . ".*") as $nombre_fichero) {
            array_push($arreglo_archivos, $nombre_fichero);
        }

        #2) DEFINIMOS NOMBRE DEL ARCHIVO Y CREAMOS EL ZIP CON LA FUNCIÓN 'create_zip'
        $nombre_archivo_zip = "exportToShapefile/shapeAPC_" . $fecha_actual . "_" . $proyeccion . ".zip";
        $result = create_zip($arreglo_archivos, $nombre_archivo_zip);

        if ($result) {
            if (!$nombre_archivo_zip) {
                echo "error, No se pudo crear el archivo Zip.";
            } else {
                if ($tipogeom == 'ambos') {
                    echo "ok," . $tipogeom . "," . $cuantos_puntos . "," . $cuantos_poligonos . "," . $nombre_archivo_zip;
                } else if ($tipogeom == 'puntos') {
                    echo "ok," . $tipogeom . "," . $cuantos_puntos . "," . $nombre_archivo_zip;
                } else if ($tipogeom == 'poligonos') {
                    echo "ok," . $tipogeom . "," . $cuantos_poligonos . "," . $nombre_archivo_zip;
                }

            }
        }
    }
    #FIN
} else #SIN FILTROS
{
    #1) EJECUTAR CONSULTA COMUN CON COUNT(*) PARA SABER EL NUMERO DE PUNTOS Y EL NUMERO DE POLIGONOS
    $countElementsPoints = pg_query($link, $selectCount . $consultaBase . $innerJoinPoints);
    $countElementsPolygons = pg_query($link, $selectCount . $consultaBase . $innerJoinPolygons);

    $cuantos_puntos = 0;
    $cuantos_poligonos = 0;
    while ($row = pg_fetch_row($countElementsPoints)) {
        $cuantos_puntos = $row[0];
    }

    while ($row = pg_fetch_row($countElementsPolygons)) {
        $cuantos_poligonos = $row[0];
    }

    #CCL
    if ($proyeccion === 'ccl') {
        $consulta_puntos = $select . ", shape_puntos_ccl.geom AS geom_point " . $consultaBase . $innerJoinPoints;
        $consulta_poligonos = $select . ", shape_poligonos_ccl_merge_final.geom AS geom_poly " . $consultaBase . $innerJoinPolygons;
    }
    #UTM14
    if ($proyeccion === 'utm14') {
        $consulta_puntos = $select . ", ST_Transform(shape_puntos_ccl.geom, 32614) AS geom_point " . $consultaBase . $innerJoinPoints;
        $consulta_poligonos = $select . ", ST_Transform(shape_poligonos_ccl_merge_final.geom, 32614) AS geom_poly " . $consultaBase . $innerJoinPolygons;
    }
    #UTM15
    if ($proyeccion === 'utm15') {
        $consulta_puntos = $select . ", ST_Transform(shape_puntos_ccl.geom, 32615) AS geom_point " . $consultaBase . $innerJoinPoints;
        $consulta_poligonos = $select . ", ST_Transform(shape_poligonos_ccl_merge_final.geom, 32615) AS geom_poly " . $consultaBase . $innerJoinPolygons;
    }

    #ELIMINAR TODOS LOS ARCHIVOS DEL DIRECTORIO 'exportToShapefile/'
    $files = glob('exportToShapefile/*');
    foreach ($files as $file) {
        if (is_file($file)) {
            unlink($file);
        }

    }

    ###DEBUG##############
    //print_r($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_poligonos . '.shp PG:"host=' . $host . ' dbname=' . DBNAME_PG . ' user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');
    ######################

    #GENERAMOS EL SHAPEFILE CON LAS HERRAMIENTAS ESPACIALES DE POSTGIS Y OGR
    shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_puntos . '.shp PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
    shell_exec($ogr2ogr . ' -f "ESRI Shapefile" ' . $destino_shapefile_poligonos . '.shp PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');
    shell_exec($ogr2ogr . ' -f KML ' . $destino_shapefile_puntos . '.kml PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_puntos . '"');
    shell_exec($ogr2ogr . ' -f KML ' . $destino_shapefile_poligonos . '.kml PG:"host=' . $host . ' dbname=ecoforestal_geo user=' . $user . ' password=' . $frase . '" -sql "' . $consulta_poligonos . '"');

    #CREAR EL ARCHIVO ZIP
    #1) AGREGAMOS A UN ARREGLO LA LISTA DE ARCHIVOS
    $arreglo_archivos = array();
    foreach (glob('exportToShapefile/' . $nombre_archivo_puntos . ".*") as $nombre_fichero) {
        array_push($arreglo_archivos, $nombre_fichero);
    }
    foreach (glob('exportToShapefile/' . $nombre_archivo_poligonos . ".*") as $nombre_fichero) {
        array_push($arreglo_archivos, $nombre_fichero);
    }

    #2) DEFINIMOS NOMBRE DEL ARCHIVO Y CREAMOS EL ZIP CON LA FUNCIÓN 'create_zip'
    $nombre_archivo_zip = "exportToShapefile/shapeAPC_" . $fecha_actual . "_" . $proyeccion . ".zip";
    $result = create_zip($arreglo_archivos, $nombre_archivo_zip);

    if ($result) {
        if (!$nombre_archivo_zip) {
            echo "error, No se pudo crear el archivo Zip.";
        } else {
            echo "ok,ambos," . $cuantos_puntos . "," . $cuantos_poligonos . "," . $nombre_archivo_zip;
        }
    }

}
