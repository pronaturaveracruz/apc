<?php
include "../includes/conexion.php";
$link = ConectarsePostgreSQL();

$sql = pg_query($link, "SELECT id_tecnico,(nombre || ' ' || apat || ' ' || amat) AS nombre_completo FROM public.tecnico ORDER BY nombre ASC");
$items = array();
while ($row = pg_fetch_object($sql)) {
    array_push($items, $row);
}
echo json_encode($items);
